@extends('layouts.app')
@section('contenido')
<style>
    .dropify-wrapper{
        height: 100px;
    }
</style>
<div class="page-wrapper">
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">CIRCULARES</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Circulares</li>
                        <li class="breadcrumb-item active">Lista de circulares</li>
                    </ol>
                    <button type="button" data-toggle="modal" data-toggle="modal" id="create_circular" class="btn btn-primary d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Nueva circular</button>
                </div>
            </div>
        </div>
        {{-- End BreadCrumb --}}

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Circulares</h4>
                        <h6 class="card-subtitle">Circulares de la institución.</h6>
                        <div class="table-responsive m-t-40">
                            <table class="table table-bordered dataTable display" cellspacing="0" width="100%" id="table_circulares">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Nombre</th>
                                        <th scope="col">Descripción</th>
                                        <th scope="col">Archivo</th>
                                        <th scope="col">Fecha de Subida</th>
                                        <th scope="col">Acciones</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Modal para Nueva Circular --}}
<div class="modal" id="modal_create_circular" tabindex="-1" role="dialog" aria-labelledby="ModalCircular" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" id="new_circular" autocomplete="off">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="ModalCircular">Nueva circular</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="nombre">Nombre:</label>
                                    <input type="text" class="form-control" name="nombre" id="nombre">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="descripcion">Descripción:</label>
                                    <textarea name="descripcion" class="form-control" name="descripcion" id="descripcion"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="archivos">Archivo PDF de la circular:</label>
                                    <input type="file" class="dropify archivo" accept=".pdf"  id="archivo" name="archivo" data-height="50" data-show-remove="true" data-allowed-file-extensions="pdf"  data-errors-position="outside" data-max-file-size="5M"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" id="btn_guardar" class="btn btn-success">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('script-js')
<script>
    $(document).ready( function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        /*
        |--------------------------------------------------------------------------
        |   Variables globales para las funciones
        |   Aqui lleno todos los valores para evitar estar cambiando en cada funcion
        |   los valores y asi evitar posibles fallos por olvidar cambiar un valor
        |--------------------------------------------------------------------------
        */

        var view_create = '#create_circular';
        var view_delete = '#delete_circular';
        var datatable_id = '#table_circulares';
        var modal_id = '#modal_lista_circulares';
        

        /*
        |--------------------------------------------------------------------------
        | Tabla donde se muestran todas las circulares registradas en el sistema
        |--------------------------------------------------------------------------
        */
        $(datatable_id).DataTable({
            processing: false,
            serverSide: true,
            ajax: {
                url: "{{route('circulares.index')}}",
                type: 'GET',
            },
            language: idiomaDataTable,
            columns: [
                {data: 'id'},
                {data: 'nombre'},
                {data: 'descripcion'},
                {data: 'archivo'},
                {data: 'fecha_subida'},
                {data: 'action', orderable: false},
            ],
            order: [[0, 'asc']]
        });


        /*
        |--------------------------------------------------------------------------
        | Cuando se da click en agregar circular se ejecuta lo siguiente
        |--------------------------------------------------------------------------
        */
        $('body').on('click', '#create_circular', function () {
            $('#nombre').val('');
            $('#descripcion').val('');

            // Reiniciar dropify
            var drEvent = $('#archivo').dropify();
            drEvent = drEvent.data('dropify');
            drEvent.resetPreview();
            drEvent.clearElement();
            drEvent.destroy();
            drEvent.init();

            $('#modal_create_circular').modal('show');  
        });


        /*
        |--------------------------------------------------------------------------
        | Cuando se da click en guardar modal circulares
        |--------------------------------------------------------------------------
        */
        $('body').on('click', '#btn_guardar', function (e) {
            e.preventDefault();

            let nombre = $('#nombre').val();
            let descripcion = $('#descripcion').val();
            let archivo = $('#archivo').val();

            if($.trim(nombre) != '' && $.trim(descripcion) != '' && $.trim(archivo) != ''){
                var form = $('#new_circular')[0];
                var formData = new FormData(form);
                $.ajax({
                    url: "{{ route('circulares.store') }}",
                    method: "POST",
                    data: formData,
                    dataType:'JSON',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success:function(response){
                        if(response.code == 200){
                            $('#nombre').val('');
                            $('#descripcion').val('');

                            // Reiniciar dropify
                            var drEvent = $('#archivo').dropify();
                            drEvent = drEvent.data('dropify');
                            drEvent.resetPreview();
                            drEvent.clearElement();
                            drEvent.destroy();
                            drEvent.init();

                            var oTable = $(datatable_id).dataTable(); 
                            oTable.fnDraw(false);

                            Swal.fire('Circular guardada', '', 'success');

                            $('#modal_create_circular').modal('hide');  
                        }else if(response.code == 500){
                            Swal.fire('¡Ocurrio un error!', 'Ha ocurrido un error.', 'error');
                        }
                    }
                });
            }else{
                Swal.fire('¡Ocurrio un error!', 'Por favor ingresa todos los datos o selecciona un archivo.', 'error');
            }         
        });


        /*
        |--------------------------------------------------------------------------
        | Cuando se da click en descargar circular se ejecuta lo siguiente
        |--------------------------------------------------------------------------
        */
        $('body').on('click', '.descargar-circular', function () {
            let id = $(this).attr('data-id');
            let route = "{{ route('circulares.download', [':id']) }}";
            let url = route.replace(":id", id);

            window.location = url;
        });


        /*
        |--------------------------------------------------------------------------
        | Cuando se da click en eliminar se ejecuta lo siguiente
        |--------------------------------------------------------------------------
        */
        $('body').on('click', view_delete, function (e) { 
            e.preventDefault();
            var id = $(this).data('id');
            
            var route = '{{ route("circulares.destroy", ":id") }}';
            var url = route.replace(":id", id);

            Swal.fire({
                title: '¿Estás seguro?',
                text: "Un registro eliminado no se puede recuperar.",
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                confirmButtonColor: '#dc3f4e',
                confirmButtonText: 'Si, eliminar',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    axios.post(url, {'_method':'delete'}).then(respuesta => {
                        // console.log(respuesta);
                        if (respuesta.status == 200) {
                            Swal.fire('Registro eliminado', '', 'success');
                            var oTable = $(datatable_id).dataTable(); 
                            oTable.fnDraw(false);
                        }else{
                            Swal.fire('¡Ocurrio un error!', 'No se pudo eliminar el registro', 'error');
                        }
                    }).catch(error => {
                        Swal.fire('¡Ocurrio un error!', 'No se pudo eliminar el registro', 'error');

                    });
                }else if(result.dismiss = 'cancel'){

                }
		    });              
        }); 

    });
</script>
@endsection