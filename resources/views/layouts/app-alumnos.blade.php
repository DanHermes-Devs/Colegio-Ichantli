<!DOCTYPE html>
<html lang="es">
    <head>
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        {{-- <meta charset="utf-8"> --}}
        {{-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> --}}
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="gereyes">
        <!-- Favicon icon -->
        {{-- <link rel="icon" type="image/ico" href="{{ asset('images/favicon.ico') }}"> --}}

        <link rel="shortcut icon" href="{{ asset('images/favicons.ico') }}" type="image/x-icon">
        <link rel="icon" href="{{ asset('images/favicons.ico') }}" type="image/x-icon">
        <title>Sistema de boletas | Colegio Ichantli</title>
        
        <link rel="stylesheet" href="{{ asset('css/all.css') }}">

        <style>
            .page-wrapper{
                margin-left: 0px;
            }

            .footer{
                margin-left: 0px;
            }

            .profile-tab li a.nav-link.active, .customtab li a.nav-link.active{
                border-bottom: 2px solid #A80735;
                color: #A80735;
            }

            .profile-tab li a.nav-link:hover, .customtab li a.nav-link:hover{
                color: #A80735;
            }
            
        </style>
    </head>

<body class="skin-ichantli fixed-layout">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Sistema de boletas | Colegio Ichantli</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">

                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item" style="display: flex; align-items: center; justify-content: center;">
                            <span>Sistema de boletas del colegio Ichantli</span>
                        </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- User Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown u-pro">
                            {{-- <a class="nav-link dropdown-toggle waves-effect waves-dark profile-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Genesis Reyes&nbsp;<i class="fa fa-angle-down"></i> </a> --}}
                            <a class="nav-link dropdown-toggle waves-effect waves-dark profile-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Str::title(Auth::user()->nombre) }} {{ Str::ucfirst(Auth::user()->apellido_paterno) }}&nbsp;<i class="fa fa-angle-down"></i> </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <!-- text-->
                                <a href="{{ route('logout') }}" class="dropdown-item"><i class="fa fa-power-off"></i> Cerrar Sesión</a>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- End User Profile -->
                        <!-- ============================================================== -->
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- CONTENIDO PRINCIPAL  -->
        <!-- ============================================================== -->
        {{-- @include('partials.sidebar') --}}
        @yield('contenido')
        <!-- ============================================================== -->
        <!-- FIN CONTENIDO PRINCIPAL  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
            <div class="row">
                <div class="col">
                    @if ($data->url_activo == 1)
                        <strong>© {{ now()->year }} <span style="color: #1C3B8B;">Colegio</span> <span style="color: #A80735;">Ichantli</span> <a href="{{ $data->url_empresa }}" style="color: #000;">{{ $data->nombre_empresa }}</a></strong>
                    @else
                        <strong>© {{ now()->year }} <span style="color: #1C3B8B;">Colegio</span> <span style="color: #A80735;">Ichantli</span> {{ $data->nombre_empresa }}</strong>
                    @endif
                </div>
            </div>
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Scripts-->
    <!-- ============================================================== -->
    <script src="{{ asset('js/all.js') }}"></script>

    @yield('script-js')
</body>

</html>