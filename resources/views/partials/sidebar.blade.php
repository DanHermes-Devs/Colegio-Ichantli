<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar" onmouseover="ps()">
        <div class="user-profile">
            <div class="user-pro-body">
                <div>
                    <img src="{{ asset('images/logo.png') }}" alt="homepage" class="dark-logo"/>
                </div>
                <div class="text-center text-escuela">
                    <b class="text-center">Colegio Ichantli</b>

                </div>
            </div>
        </div>
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav pt-2">
            <ul id="sidebarnav">
                @can('inicio')
                    <li><a class="waves-effect waves-dark" href="{{ route('home') }}" aria-expanded="false"><i class="fas fa-tachometer-alt"></i><span class="hide-menu">Inicio</span></a></li>
                @endcan

                @can('boletas')
                    <li><a class="waves-effect waves-dark" href="{{ route('boleta.index') }}" aria-expanded="false"><i class="fas fa-book-reader"></i><span class="hide-menu">Boletas</span></a></li>
                @endcan

                @can('circulares')
                    <li><a class="waves-effect waves-dark" href="{{ route('circulares.index') }}" aria-expanded="false"><i class="fas fa-newspaper"></i><span class="hide-menu">Circulares</span></a></li>
                @endcan

                @can('catalogos')
                    <li><a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fas fa-archive"></i><span class="hide-menu">Catálogos</span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li>                                  
                                <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">Alumnos</a>
                                <ul aria-expanded="false" class="collapse">
                                    <li>
                                        <a href="{{ route('alumno.index') }}">Listado</a>                                      
                                    </li>
                                    <li>
                                        <a href="{{ route('alumno.create') }}">Nuevo alumno</a>                                      
                                    </li>
                                </ul>
                            </li>                        
                        </ul>
                    </li>
                @endcan

                @can('gestion_de_usuarios')
                    <li><a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fas fa-users"></i><span class="hide-menu">Gestión de usuarios</span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="{{route('usuario.index')}}">Usuarios</a></li>
                            <li><a href="{{route('rol.index')}}">Roles</a></li>
                        </ul>
                    </li>
                @endcan

                @can('configuracion')
                    <li><a class="waves-effect waves-dark" href="{{ route('config.index') }}" aria-expanded="false"><i class="fas fa-wrench"></i><span class="hide-menu">Configuración</span></a></li>
                @endcan
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->