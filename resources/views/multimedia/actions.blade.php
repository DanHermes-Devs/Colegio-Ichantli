<button type="button" data-toggle="tooltip" title="Botón de acción para descargar archivo del alumno"
        data-id="{{ $id }}" class="btn btn-success descargar_archivo"><i class="fas fa-file-download"></i></button>

<button type="button" data-toggle="tooltip" title="Botón de acción para eliminar archivo del alumno"
        data-id="{{ $id }}" class="btn btn-danger eliminar_archivo"><i class="fas fa-trash"></i></button>
