@extends('layouts.app')

@section('style-css')
    <link rel="stylesheet" href="{{ asset("js/bootstrap-fileinput/css/fileinput.min.css") }}">
@endsection

@section('contenido')
    <div class="page-wrapper">
        <div class="container-fluid">

            <form action="{{ route('multimedia.store') }}" method="POST" enctype="multipart/form-data" id="formulario_archivo">
                @csrf

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="archivo">Selecciona el tipo de documento</label>
                            <select name="id_tipoArchivo" id="id_tipoArchivo" class="form-control">
                                <option value="">-- Seleccionar --</option>
                                <option value="1">Acta de Nacimiento</option>
                                <option value="2">Comrpobante de Domiclio</option>
                                <option value="3">Certificado Médico</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="archivo">Selecciona documento (.pdf, .png, .jpeg, .docx, .xlsx)</label>
                            <input type="file" class="form-control nombreArchivo" name="nombreArchivo" id="nombreArchivo" accept=".jpg, .jpeg, .png, .doc, .docx, .pdf, .xlsx">
                        </div>
                    </div>

                    <input type="hidden" name="id_alumno" id="id_alumno" class="id_alumno" value="1">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" id="btn-guardarArchivo" class="btn btn-primary">Cargar Archivo</button>
                </div>
            </form>

        </div>
    </div>
@endsection

@section('script-js')
   <script>

    </script>
@endsection

