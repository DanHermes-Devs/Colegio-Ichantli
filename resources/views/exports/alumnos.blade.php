<table>
    <thead>
    <tr>
        <th style="width: 25px;">Matricula</th>
        <th style="width: 25px;">Nombre</th>
        <th style="width: 25px;">Apellido Paterno</th>
        <th style="width: 25px;">Apellido Materno</th>
        <th style="width: 25px;">Plantel</th>
        <th style="width: 25px;">Nivel Educativo</th>
        <th style="width: 25px;">Grado - Grupo</th>
        <th style="width: 25px;">Fecha de Nacimiento</th>
        <th style="width: 25px;">CURP</th>
        <th style="width: 25px;">Email</th>
        <th style="width: 25px;">Teléfono</th>
        <th style="width: 25px;">Domicilio</th>
        <th style="width: 25px;">Nombre del Padre o Tutor</th>
        <th style="width: 25px;">Email del Padre o Tutor</th>
        <th style="width: 25px;">Teléfono del Padre o Tutor</th>
        <th style="width: 25px;">Observaciones</th>
    </tr>
    </thead>
    <tbody>

    @foreach($cursos as $curso)
        <tr>
            <td>{{ $curso->matricula }}</td>
            <td>{{ $curso->nombre }}</td>
            <td>{{ $curso->apellido_paterno }}</td>
            <td>{{ $curso->apellido_materno }}</td>
            <td>{{ $curso->plantel->nombre }}</td>
            <td>{{ $curso->curso->nombre }}</td>
            <td>{{ $curso->grupo->grado }} - {{ $curso->grupo->letra }}</td>
            <td>{{ $curso->fecha_nacimiento }}</td>
            <td>{{ $curso->curp }}</td>
            <td>{{ $curso->email }}</td>
            <td>{{ $curso->telefono }}</td>
            <td>{{ $curso->domicilio }}</td>
            <td>{{ $curso->nombre_completo_tutor }}</td>
            <td>{{ $curso->email_tutor }}</td>
            <td>{{ $curso->telefono_tutor }}</td>
            <td>{{ $curso->observaciones }}</td>
        </tr>
    @endforeach
    </tbody>
</table>