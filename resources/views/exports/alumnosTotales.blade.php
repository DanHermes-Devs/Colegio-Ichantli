<table>
    <thead>
    <tr>
        <th style="width: 25px;">Matricula</th>
        <th style="width: 25px;">Nombre</th>
        <th style="width: 25px;">Apellido Paterno</th>
        <th style="width: 25px;">Apellido Materno</th>
        <th style="width: 25px;">Plantel</th>
        <th style="width: 25px;">Nivel Educativo</th>
        <th style="width: 25px;">Grado - Grupo</th>
        <th style="width: 25px;">Fecha de Nacimiento</th>
        <th style="width: 25px;">CURP</th>
        <th style="width: 25px;">Email</th>
        <th style="width: 25px;">Teléfono</th>
        <th style="width: 25px;">Domicilio</th>
        <th style="width: 25px;">Nombre del Padre o Tutor</th>
        <th style="width: 25px;">Email del Padre o Tutor</th>
        <th style="width: 25px;">Teléfono del Padre o Tutor</th>
        <th style="width: 25px;">Observaciones</th>
    </tr>
    </thead>
    <tbody>

    @foreach($alumnos as $alumno)
        <tr>
            <td>{{ $alumno->matricula }}</td>
            <td>{{ $alumno->nombre }}</td>
            <td>{{ $alumno->apellido_paterno }}</td>
            <td>{{ $alumno->apellido_materno }}</td>
            <td>{{ $alumno->plantel->nombre }}</td>
            <td>{{ $alumno->curso->nombre }}</td>
            <td>{{ $alumno->grupo->grado }} - {{ $alumno->grupo->letra }}</td>
            <td>{{ $alumno->fecha_nacimiento }}</td>
            <td>{{ $alumno->curp }}</td>
            <td>{{ $alumno->email }}</td>
            <td>{{ $alumno->telefono }}</td>
            <td>{{ $alumno->domicilio }}</td>
            <td>{{ $alumno->nombre_completo_tutor }}</td>
            <td>{{ $alumno->email_tutor }}</td>
            <td>{{ $alumno->telefono_tutor }}</td>
            <td>{{ $alumno->observaciones }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
