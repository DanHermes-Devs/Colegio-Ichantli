@extends('auth.layout-login')

@section('login')
<div class="login-register" style="background-image:url(images/background/login-register8.jpg);">
    <div class="login-box card">
        <div class="card-body">
            <form class="form-horizontal" method="POST" id="loginForm" action="{{ route('login') }}" novalidate>
                @csrf
                <div class="text-center image-box mb-4">
                    <img src="{{ asset('images/logo.png') }}" alt="Home"/>
                </div>
                <h3 class="text-center mb-3">Iniciar sesión</h3>
                <div class="row float-right mb-2">
                    <div class="col-md-12 float-right">
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Padre de familia para acceder al sistema debe colocar la matrícula en mayusculas en el campo de usuario, y colocar el CURP en mayusculas para la contraseña."><strong>Ayuda</strong> <i class="far fa-question-circle"></i></span> </a>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" value="{{ old('usuario') }}"type="text" required="" placeholder="Usuario" id="usuario" name="usuario">
                        @if ($errors->has('usuario'))
                            <span class="text-danger m-2">{{ $errors->first('usuario') }}</span>    
                        @endif                        
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        {{-- Se utilizo la siguient elibreria para mostrar el password
                        http://bootstrap-show-password.wenzhixin.net.cn/getting-started/ --}}
                        <input class="form-control" type="password" required="" placeholder="Contraseña" data-toggle="password" id="password" name="password"> 
                        @if ($errors->has('password'))
                            <span class="text-danger m-2">{{ $errors->first('password') }}</span>    
                        @endif
                    </div>
                </div>
                {{-- <div class="form-group row">
                    <div class="col-md-12">
                        <div class="d-flex no-block align-items-center">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="customCheck1">
                                <label class="custom-control-label" for="customCheck1">Remember me</label>
                            </div> 
                            <div class="ml-auto">
                                <a href="javascript:void(0)" id="to-recover" class="text-muted"><i class="fas fa-lock m-r-5"></i> Forgot pwd?</a> 
                            </div>
                        </div>
                    </div>
                </div> --}}
                <div class="form-group text-center">
                    <div class="col-xs-12 p-b-20">
                        <button class="btn btn-block btn-lg btn-primary" type="submit">Acceder</button>
                    </div>
                </div>
                <div class="form-group m-b-0">
                    <div class="col-sm-12 text-center">
                        Colegio Ichantli {{ now()->year }}.
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
