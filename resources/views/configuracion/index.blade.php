@extends('layouts.app')
@section('contenido')
<div class="page-wrapper">
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">CONFIGURACIÓN</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">Configuración</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form action="POST" id="form_configuracion" autocomplete="off" enctype="multipart/form-data">
                            @csrf
                            <div class="form-body">
                                <h3 class="card-title bg-info text-white p-2 mt-2">Datos de configuración</h3>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="nombre_empresa" class="control-label">Nombre empresa: <span class="field-required-span">*</span></label>
                                            <input type="text" class="form-control" id="nombre_empresa" name="nombre_empresa" value="{{ $data->nombre_empresa }}">
                                            <div id="error-nombre-empresa"></div>
                                        </div>
                                    </div>
                                </div> 

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="descripcion_empresa">Descripción empresa: <span class="field-required-span">&nbsp;</span></label>
                                            <input type="text" class="form-control" id="descripcion_empresa" name="descripcion_empresa" value="{{ $data->descripcion }}">
                                            <div id="error-descripcion-empresa"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="form-check">
                                                <input class="form-check-input" type="hidden" id="url_activo_1" value="0" name="url_activo">
                                                <input class="form-check-input" type="checkbox" id="url_activo_2" value="1" name="url_activo" {{ ($data->url_activo == 1)? 'checked': ''}}>
                                                <label class="form-check-label" for="url_activo">¿Url activo?</label>
                                              </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="url_empresa">url empresa: <span class="field-required-span">&nbsp;</span></label>
                                            <input type="text" class="form-control" id="url_empresa" name="url_empresa" value="{{ $data->url_empresa }}">
                                            <div id="error-descripcion-empresa"></div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-actions float-right">
                                    <button type="button" class="btn btn-secondary btn_cancelar">Cancelar</button>
                                    <button type="submit" id="btn_guardar" class="btn btn-success"> <i class="fa fa-check"></i> Guardar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script-js')
<script>
    $(document).ready( function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        
        /*
        |--------------------------------------------------------------------------
        |   Variables globales para las funciones
        |   Aqui lleno todos los valores para evitar estar cambiando en cada funcion
        |   los valores y asi evitar posibles fallos por olvidar cambiar un valor
        |--------------------------------------------------------------------------
        */
        var form_id = '#form_configuracion';
        var btn_save = '#btn_guardar';

        
        /*
        |--------------------------------------------------------------------------
        |   Cuando se da click en el boton de cancelar
        |--------------------------------------------------------------------------
        */
        $('body').on('click', '.btn_cancelar', function () {            
            window.location = "{{ route('home') }}";
        });


        /*
        |--------------------------------------------------------------------------
        |   Inicializa la validacion del formulario y envia el formulario
        |   solo si fueron aceptados los campos requeridos
        |--------------------------------------------------------------------------
        */
        $(form_id).validate({
            focusInvalid: true,
            rules: {
                nombre_empresa: "required" 
            },
            // Specify validation error messages
            messages: {
                nombre_empresa: "Ingrese el nombre de su empresa.",     
            },            
            highlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            errorPlacement: function(error, element) {
                if(element.is(nombre_empresa)){
                    error.appendTo($('#error-nombre-empresa'));
                }
            },
            submitHandler: function(form) {
                $.ajax({
                    url:"{{ route('config.store') }}",
                    method: "post",
                    data: new FormData(form),
                    dataType:'JSON',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success:function(response){
                        if(response.code == 200){   
                            Swal.fire({
                                title: 'Éxito',
                                text: "Configuración guardada con éxito.",
                                icon: 'success',
                                confirmButtonText: 'Ok'
                            }).then((result) => {
                                if (result.value) {
                                    location.reload();
                                }else if(result.dismiss = 'cancel'){

                                }
                            });  
                        }else if(response.code == 500){
                            Swal.fire('¡Ocurrio un error!', 'Ha ocurrido un error', 'error');
                        }                        
                    }
                });
            }
        });


    });
</script>
@endsection

