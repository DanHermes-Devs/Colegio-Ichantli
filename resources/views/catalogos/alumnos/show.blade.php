@extends('layouts.app')
@section('contenido')
<div class="page-wrapper">
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">CATÁLOGO DE ALUMNOS | VER ALUMNO</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Catálogos</li>
                        <li class="breadcrumb-item">Alumnos</li>
                        <li class="breadcrumb-item active">Ver alumno</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="form-body">
                            <h3 class="card-title bg-info text-white p-2 mt-2">Datos del alumno</h3>
                            {{-- matricula, curso y grupo --}}
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="matricula" class="control-label">Matrícula <span class="field-required-span">&nbsp;</span></label>
                                        <input type="text" class="form-control" id="matricula" name="matricula" disabled value="{{ $data->matricula }}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="curso_id">Nivel educativo <span class="field-required-span">&nbsp;</span></label>
                                        <input type="text" class="form-control" id="curso_id" name="curso_id" disabled value="{{ $data->curso->nombre }}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="grupo_id">Grupo <span class="field-required-span">&nbsp;</span></label>
                                        <input type="text" class="form-control" id="grupo_id" name="grupo_id" disabled value="{{ $data->grupo->grado.'-'.$data->grupo->letra }}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label" for="grupo_id">Plantel <span class="field-required-span">&nbsp;</span></label>
                                        <input type="text" class="form-control" id="plantel_id" name="plantel_id" disabled value="{{ $data->plantel->nombre }}">
                                    </div>
                                </div>
                            </div> 
                            <hr>

                            <div class="row">
                                {{-- fotografia --}}
                                <div class="col-md-2 mt-2">
                                    <div class="form-group">
                                        @if($data->foto)
                                            <img src="{{ asset('store/alumnos/fotografias').'/'.$data->foto }}" alt="image" class="img-thumbnail" width="130" height="150">
                                        @else
                                            <p class="font-weight-bold">No se ha agregado una fotografía.</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    {{-- nombre, apellido paterno, apellido materno--}}
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="nombre" class="control-label">Nombre(s) <span class="field-required-span">&nbsp;</span></label>
                                                <input type="text" class="form-control" id="nombre" name="nombre" disabled value="{{ $data->nombre }}">
                                            </div>                                        
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label" for="apellido_paterno">Apellido paterno <span class="field-required-span">&nbsp;</span></label>
                                                <input type="text" class="form-control" id="apellido_paterno" name="apellido_paterno" disabled value="{{ $data->apellido_paterno }}">
                                            </div>                                        
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label" for="apellido_materno">Apellido materno <span class="field-required-span">&nbsp;</span></label>
                                                <input type="text" class="form-control" id="apellido_materno" name="apellido_materno" disabled value="{{ $data->nombre }}">
                                            </div>                                       
                                        </div>
                                    </div>
                                    {{-- sexo, fecha de nacimiento, curp, telefono --}}
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label" for="sexo_id">Sexo <span class="field-required-span">&nbsp;</span></label>
                                                <input type="text" class="form-control" id="sexo_id" name="sexo_id" disabled value="{{ $data->genero->nombre }}">
                                            </div>                                        
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label" for="fecha_nacimiento">Fecha de nacimiento <span class="field-required-span">&nbsp;</span></label>
                                                <input type="text" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" disabled value="{{ $data->fecha_nacimiento->format('d/m/Y') }}">
                                            </div>                                        
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label" for="curp">CURP <span class="field-required-span">&nbsp;</span></label>
                                                <input type="text" class="form-control" id="curp" name="curp" disabled value="{{ $data->curp}}">
                                            </div>                                       
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label" for="domicilio">Domicilio <span class="field-required-span">&nbsp;</span></label>
                                                <input type="text" class="form-control" id="domicilio" name="domicilio" disabled value="{{ $data->domicilio }}">
                                            </div>                                        
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h3 class="card-title bg-info text-white p-2 mt-2">Datos del padre o tutor</h3>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="nombre_tutor">Nombre del padre o tutor <span class="field-required-span">&nbsp;</span></label>
                                        <input type="text" class="form-control" id="nombre_completo_tutor" name="nombre_completo_tutor" disabled value="{{ $data->nombre_completo_tutor }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="correo_electronico_tutor">Correo electrónico <span class="field-required-span">&nbsp;</span></label>
                                        <input type="text" class="form-control" id="correo_electronico_tutor" name="correo_electronico_tutor" disabled value="{{ $data->email_tutor }}">
                                    </div>                                        
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label" for="telefono_celular_tutor">Teléfono celular <span class="field-required-span">&nbsp;</span></label>
                                        <input type="text" class="form-control" id="telefono_celular_tutor" name="telefono_celular_tutor" disabled value="{{ $data->telefono_tutor }}">
                                    </div>                                        
                                </div>
                            </div>

                            <h3 class="card-title bg-info text-white p-2 mt-2">Observaciones del alumno</h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="observaciones">Observaciones <span class="field-required-span">&nbsp;</span></label>
                                        <textarea name="observaciones" id="observaciones" class="form-control" disabled>{{ $data->observaciones }}</textarea>
                                    </div>
                                </div>                                        
                            </div>

                            <div class="form-actions float-right">
                                <button type="button" class="btn btn-secondary btn_cancelar">Cancelar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script-js')
<script>
    $(document).ready( function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        /*
        |--------------------------------------------------------------------------
        |   Cuando se da click en el boton de cancelar
        |--------------------------------------------------------------------------
        */
        $('body').on('click', '.btn_cancelar', function () {            
            window.location = "{{route('alumno.index')}}";
        });
    });
</script>
@endsection

