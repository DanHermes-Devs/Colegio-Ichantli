@extends('layouts.app')
@section('contenido')
    @inject('generos', 'App\Services\Generos')
    <div class="page-wrapper">
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">CATÁLOGO DE ALUMNOS</h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">Catálogos</li>
                            <li class="breadcrumb-item active">Alumnos</li>
                        </ol>
                        <button type="button" data-toggle="modal" id="create_alumno"
                            class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Nuevo
                            alumno</button>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Alumnos</h4>
                            <h6 class="card-subtitle">Alumnos de la institución.</h6>
                            <div class="d-flex align-items-flex-end">
                                <div class="col-md-3 border-right border-dark">
                                    <div class="form-group m-0 d-flex flex-column">
                                        <label class="control-label" for="exportTotal">Exportar Todo</label>
                                        <a href="{{ route('alumno.export') }}" id="exportTotal" class="text-white form-control btn btn-info">Exportar XLS</a>
                                    </div>
                                </div>

                                <div class="col-md-9 d-flex align-items-flex-end gap-1 flex-wrap">
                                        <div class="form-group m-0">
                                            <label class="control-label" for="curso_id">Nivel educativo <span class="field-required-span">*</span></label>
                                            <select class="form-control" id="curso_id" name="curso_id">
                                                <option hidden selected value="">-- Selecciona --</option>
                                                @foreach ($niveleducativo as $nivel)
                                                    <option value="{{ $nivel->id }}">{{ $nivel->nombre }}</option>
                                                 @endforeach
                                            </select>
                                            <div id="error-curso"></div>
                                        </div>

                                        <div class="form-group m-0">
                                            <label class="control-label" for="grupo_id">Grupo <span class="field-required-span">*</span></label>
                                            <select class="form-control" id="grupo_id" name="grupo_id" disabled>
                                                <option hidden value="">-- Seleccionar --</option>
                                            </select>
                                            <div id="error-grupo"></div>
                                        </div>

                                        <div class="form-group m-0">
                                            <label class="control-label" for="grupo_id"> </label>
                                            <button class="btn btn-success form-control text-white" id="exportar-filtro">Exportar Filtro</button>
                                        </div>
                                    </div>
                            </div>

                            <div class="table-responsive m-t-40">
                                <table class="table table-bordered dataTable display" cellspacing="0" width="100%"
                                    id="table_alumnos">
                                    <thead>
                                        <tr>
                                            <th scope="col" style="width: 5%">#</th>
                                            <th scope="col" style="width: 10%">Matrícula</th>
                                            <th scope="col" style="width: 10%">Plantel</th>
                                            <th scope="col" style="width: 20%">Nivel educativo</th>
                                            <th scope="col" style="width: 5%">Grupo</th>
                                            <th scope="col" style="width: 30%">Nombre</th>
                                            <th scope="col" style="width: 20%">Acciones</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script-js')
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            /*
            |--------------------------------------------------------------------------
            |   Variables globales para las funciones
            |   Aqui lleno todos los valores para evitar estar cambiando en cada funcion
            |   los valores y asi evitar posibles fallos por olvidar cambiar un valor
            |--------------------------------------------------------------------------
            */

            var view_create = '#create_alumno';
            var view_read = '#view_alumno';
            var view_update = '#edit_alumno';
            var view_delete = '#delete_alumno';
            var view_archivos = '#cargar_archivo';
            var datatable_id = '#table_alumnos';

            /*
            |--------------------------------------------------------------------------
            |   Funcion que carga el dropdown de grupos
            |--------------------------------------------------------------------------
            */
            $("#curso_id").change(function () {
                let curso_id = $(this).val();
                $('#grupo_id').prop('disabled', true);

                $.ajax({
                    url: '{{ route("obtener.grupos") }}',
                    type: 'get',
                    dataType: 'json',
                    data: {
                        curso_id: curso_id
                    }
                }).always(function(response){
                    if(Object.keys(response).length > 0){
                        $('#grupo_id').empty();
                        $('#grupo_id').append('<option hidden selected value="">-- Seleccionar --</option>');

                        $.each(response, function(index, value){
                            $('#grupo_id').append("<option value='" + value.id +"'>"+ value.grado + '-'+ value.letra +"</option>");
                        });

                        $('#grupo_id').prop('disabled', false);
                    }else{
                        Swal.fire('¡Ocurrio un error!', 'Ha ocurrido un error', 'error');;
                    }
                });
            });


            $("#exportar-filtro").click(function(){
                var curso_id = $("#curso_id").val();
                var grupo_id = $("#grupo_id").val();

                var route = '{{ route('alumno.filter', [':id1', ':id2']) }}';
                var url = route.replace(":id1", curso_id).replace(":id2", grupo_id);
                console.log(url)

                $.ajax({
                    type: "GET",
                    url: url,
                    success: function(respuesta){
                        window.location.href = url;
                    }
                })
            });

            /*
            |--------------------------------------------------------------------------
            | Tabla donde se muestran todos los alumnos registrados en el sistema
            |--------------------------------------------------------------------------
            */
            $(datatable_id).DataTable({
                processing: false,
                serverSide: true,
                ajax: {
                    url: "{{ route('alumno.index') }}",
                    type: 'GET',
                },
                language: idiomaDataTable,
                columns: [{
                        data: 'id'
                    },
                    {
                        data: 'matricula'
                    },
                    {
                        data: 'plantel.nombre',
                        render: function(data, type, row) {
                            return `<span class="badge bg-info font-weight-bold p-1 text-white">${data}</span>`;
                        }
                    },
                    {
                        data: 'curso',
                        render: function(data, type, row) {
                            return data.nombre;
                        }
                    },
                    {
                        data: 'grupo',
                        render: function(data, type, row) {
                            return data.grado + '-' + data.letra;
                        }
                    },
                    {
                        data: 'nombre_completo'
                    },
                    {
                        data: 'action',
                        orderable: false
                    },
                ],
                order: [
                    [0, 'asc']
                ]
            });


            /*
            |--------------------------------------------------------------------------
            | Cuando se da click en nuevo se ejecuta lo siguiente
            |--------------------------------------------------------------------------
            */
            $('body').on('click', view_create, function() {
                window.location = "{{ route('alumno.create') }}";
            });


            /*
            |--------------------------------------------------------------------------
            | Cuando se da click en ver se ejecuta lo siguiente
            |--------------------------------------------------------------------------
            */
            $('body').on('click', view_read, function() {
                let url = $(this).attr('data-route');
                window.location = url;
            });

            /*
            |--------------------------------------------------------------------------
            | Cuando se da click en subir archivos se ejecuta lo siguiente
            |--------------------------------------------------------------------------
            */
            $('body').on('click', view_archivos, function() {
                let url = $(this).attr('data-route');
                window.location = url;
            });


            /*
            |--------------------------------------------------------------------------
            | Cuando se da click en editar se ejecuta lo siguiente
            |--------------------------------------------------------------------------
            */
            $('body').on('click', view_update, function() {
                let url = $(this).attr('data-route');
                window.location = url;
            });


            /*
            |--------------------------------------------------------------------------
            | Cuando se da click en eliminar se ejecuta lo siguiente
            |--------------------------------------------------------------------------
            */
            $('body').on('click', view_delete, function(e) {
                e.preventDefault();
                var id = $(this).data('id');

                var route = '{{ route('alumno.destroy', ':id') }}';
                var url = route.replace(":id", id);

                Swal.fire({
                    title: '¿Estás seguro?',
                    text: "Un registro eliminado no se puede recuperar.",
                    icon: 'warning',
                    showCancelButton: true,
                    cancelButtonText: 'Cancelar',
                    confirmButtonColor: '#dc3f4e',
                    confirmButtonText: 'Si, eliminar',
                    reverseButtons: true
                }).then((result) => {
                    if (result.value) {
                        axios.post(url, {
                            '_method': 'delete'
                        }).then(respuesta => {
                            // console.log(respuesta);
                            if (respuesta.status == 200) {
                                Swal.fire('Registro eliminado', '', 'success');
                                var oTable = $(datatable_id).dataTable();
                                oTable.fnDraw(false);
                            } else {
                                Swal.fire('¡Ocurrio un error!',
                                    'No se pudo eliminar el registro', 'error');
                            }
                        }).catch(error => {
                            Swal.fire('¡Ocurrio un error!',
                                'No se pudo eliminar el registro', 'error');

                        });
                    } else if (result.dismiss = 'cancel') {

                    }
                });
            });

        });
    </script>
@endsection
