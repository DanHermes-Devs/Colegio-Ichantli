<button type="button" data-toggle="tooltip" title="Botón de acción para ver" id="view_alumno" data-id="{{ $id }}" data-route="{{ route("alumno.show", $id) }}" class="btn btn-info"><i class="far fa-eye"></i></button>

<button type="button" data-toggle="tooltip" title="Botón de acción para cargar archivos" id="cargar_archivo" data-id="{{ $id }}" data-route="{{ route("alumno.archivos", $id) }}" class="btn btn-dark"><i class="fas fa-file-upload"></i></button>

<button type="button" data-toggle="tooltip" title="Botón de acción para editar" id="edit_alumno" data-id="{{ $id }}" data-route="{{ route("alumno.edit", $id) }}" class="btn btn-warning"><i class="fa fa-pencil-alt"></i></button>

<button type="button" data-toggle="tooltip" title="Botón de acción para eliminar" id="delete_alumno" data-id="{{ $id }}" class="btn btn-danger"><i class="fas fa-trash"></i></button>

