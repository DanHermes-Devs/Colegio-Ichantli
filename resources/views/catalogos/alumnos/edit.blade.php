@extends('layouts.app')

@section('contenido')
<div class="page-wrapper">
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">CATÁLOGO DE ALUMNOS | EDITAR ALUMNO</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Catálogos</li>
                        <li class="breadcrumb-item">Alumnos</li>
                        <li class="breadcrumb-item active">Editar alumno</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form action="POST" id="form_nuevo_alumno" autocomplete="off" enctype="multipart/form-data">
                            <input type="hidden" id="alumno_id" name="alumno_id" value="{{ $data->id }}">
                            @csrf
                            <div class="form-body">
                                <h3 class="card-title bg-info text-white p-2 mt-2">Datos del alumno</h3>
                                {{-- matricula, curso y grupo --}}
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="matricula" class="control-label">Matrícula <span class="field-required-span">*</span></label>
                                            <input type="text" class="form-control" id="matricula" name="matricula" disabled value="{{ $data->matricula }}">
                                            <input type="hidden" class="form-control" id="matricula2" name="matricula2" value="{{ $data->matricula }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label" for="curso_id">Nivel educativo <span class="field-required-span">*</span></label>
                                            <select class="form-control" id="curso_id" name="curso_id">
                                                <option hidden selected value="">-- Seleccionar --</option>
                                                @foreach ($cursos as $curso)
                                                    <option value="{{ $curso->id }}" {{ ($curso->id == $data->nivel_educativo_id)? 'selected' : '' }}>{{ $curso->nombre }}</option>
                                                @endforeach
                                            </select>
                                            <div id="error-curso"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label" for="grupo_id">Grupo <span class="field-required-span">*</span></label>
                                            <select class="form-control" id="grupo_id" name="grupo_id">
                                                <option hidden value="">-- Seleccionar --</option>
                                                @foreach ($grupos as $grupo)
                                                    @if ($grupo->nivel_educativo_id == $data->nivel_educativo_id)
                                                        <option value="{{ $grupo->id }}" {{ ($grupo->id == $data->grupo_id)? 'selected' : '' }}>{{ $grupo->grado.'-'.$grupo->letra }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            <div id="error-grupo"></div>
                                        </div>
                                    </div>
                                    {{-- Select Planteles --}}
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label" for="plantel_id">Plantel <span class="field-required-span">*</span></label>
                                            <select class="form-control" id="plantel_id" name="plantel_id">
                                                <option hidden value="">-- Seleccionar --</option>
                                                @foreach ($planteles as $plantel)
                                                    <option value="{{ $plantel->id }}" {{ ($data->plantel_id == $plantel->id)? 'selected': '' }}>{{ $plantel->nombre }}</option>
                                                @endforeach
                                            </select>
                                            <div id="error-plantel"></div>
                                        </div>
                                    </div>
                                </div>
                                <hr>

                                <div class="row">
                                    {{-- fotografia --}}
                                    <div class="col-md-2 mt-2">
                                        <div class="form-group">
                                            <label class="control-label" for="fotografia">Fotografía </label>
                                            @if ($data->foto)
                                                <input type="file" class="dropify fotografia" id="fotografia" name="fotografia" data-height="150" data-show-remove="true" data-allowed-file-extensions="png jpg"  data-errors-position="outside" data-max-file-size="3M" data-default-file="{{ asset('store/alumnos/fotografias').'/'.$data->foto }}"/>
                                            @else
                                                <input type="file" class="dropify fotografia" id="fotografia" name="fotografia" data-height="150" data-show-remove="true" data-allowed-file-extensions="png jpg"  data-errors-position="outside" data-max-file-size="3M"/>
                                            @endif
                                            <small id="fotografia" class="form-text text-muted">Solo fotografias en formato PNG o JPG.</small>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        {{-- nombre, apellido paterno, apellido materno--}}
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="nombre" class="control-label">Nombre(s) <span class="field-required-span">*</span></label>
                                                    <input type="text" class="form-control" id="nombre" name="nombre" value="{{ $data->nombre }}">
                                                    <div id="error-nombre"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label" for="apellido_paterno">Apellido paterno <span class="field-required-span">*</span></label>
                                                    <input type="text" class="form-control" id="apellido_paterno" name="apellido_paterno" value="{{ $data->apellido_paterno }}">
                                                    <div id="error-apellido-paterno"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label" for="apellido_materno">Apellido materno <span class="field-required-span">*</span></label>
                                                    <input type="text" class="form-control" id="apellido_materno" name="apellido_materno" value="{{ $data->apellido_materno }}">
                                                    <div id="error-apellido-materno"></div>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- sexo, fecha de nacimiento, curp, telefono --}}
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label" for="sexo_id">Sexo <span class="field-required-span">*</span></label>
                                                    <select class="form-control" id="sexo_id" name="sexo_id">
                                                        <option hidden selected value="">-- Seleccionar --</option>
                                                        @foreach ($generos as $genero)
                                                            <option value="{{ $genero->id }}" {{ ($genero->id == $data->sexo_id)? 'selected': ''}}>{{ $genero->nombre }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div id="error-sexo"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label" for="fecha_nacimiento">Fecha de nacimiento <span class="field-required-span">*</span></label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control date-picker fecha-mask" style="padding-left: 0.50rem; padding-right: 0.50rem;" placeholder="dd/mm/aaaa" data-inputmask="'alias': 'datetime', 'inputFormat': 'dd/mm/yyyy', 'placeholder': 'dd/mm/aaaa'" id="fecha_nacimiento" name="fecha_nacimiento" value="{{ $data->fecha_nacimiento->format('d/m/Y')}}">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text" id="basic-addon2"><i class="far fa-calendar-alt"></i></span>
                                                        </div>
                                                    </div>
                                                    <div id="error-fecha-nacimiento"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label" for="curp">CURP <span class="field-required-span">*</span></label>
                                                    <input type="text" class="form-control sinEspaciosMayusculas" id="curp" name="curp" maxlength="18" value="{{ $data->curp }}">
                                                    <div id="error-curp"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label" for="domicilio">Domicilio <span class="field-required-span">*</span></label>
                                                    <input type="text" class="form-control" id="domicilio" name="domicilio" value="{{ $data->domicilio }}">
                                                    <div id="error-domicilio"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <h3 class="card-title bg-info text-white p-2 mt-2">Datos del padre o tutor</h3>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label" for="nombre_tutor">Nombre del padre o tutor <span class="field-required-span">&nbsp;</span></label>
                                            <input type="text" class="form-control" id="nombre_tutor" name="nombre_tutor" value="{{ $data->nombre_completo_tutor }}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label" for="correo_electronico_tutor">Correo electrónico <span class="field-required-span">&nbsp;</span></label>
                                            <input type="text" class="form-control email-inputmask" id="correo_electronico_tutor" name="correo_electronico_tutor" value="{{ $data->email_tutor }}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label" for="telefono_celular_tutor">Teléfono celular <span class="field-required-span">&nbsp;</span></label>
                                            <input type="text" class="form-control phone-inputmask" id="telefono_celular_tutor" name="telefono_celular_tutor" value="{{ $data->telefono_tutor }}">
                                        </div>
                                    </div>
                                </div>

                                <h3 class="card-title bg-info text-white p-2 mt-2">Observaciones del alumno</h3>
                                <div class="row mb-3">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="observaciones">Observaciones <span class="field-required-span">&nbsp;</span></label>
                                            <textarea name="observaciones" id="observaciones" class="form-control">{{ $data->observaciones }}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-actions float-right">
                                    <button type="button" class="btn btn-secondary btn_cancelar">Cancelar</button>
                                    <button type="submit" id="btn_guardar" class="btn btn-success"> <i class="fa fa-check"></i> Guardar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script-js')
<script>
    $(document).ready( function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        /*
        |--------------------------------------------------------------------------
        |   Variables globales para las funciones
        |   Aqui lleno todos los valores para evitar estar cambiando en cada funcion
        |   los valores y asi evitar posibles fallos por olvidar cambiar un valor
        |--------------------------------------------------------------------------
        */
        var form_id = '#form_nuevo_alumno';
        var btn_save = '#btn_guardar';


        /*
        |--------------------------------------------------------------------------
        |   Funcion para bloquear o desbloquear el boton de buscar
        |--------------------------------------------------------------------------
        */
        $("#matricula_alumno").keyup(function(){
            if($(this).val().length == 11){
                $('#buscar_matricula').prop('disabled', false);
            }else{
                $('#buscar_matricula').prop('disabled', true);
            }
        });


        /*
        |--------------------------------------------------------------------------
        |   Cuando se da click en buscar
        |--------------------------------------------------------------------------
        */
        $('body').on('click', '#buscar_matricula', function () {
            let matricula = $('#matricula_alumno').val();

            if($.trim(matricula) != ''){
                $.ajax({
                    url: '{{ route("alumno.find") }}',
                    type: 'get',
                    dataType: 'json',
                    data: {
                        matricula: matricula
                    }
                }).always(function(response){
                    // Cuando la consulta ha sido completada
                    if(response.status == true){
                        Swal.fire('¡Ocurrio un error!', 'Ya existe un alumno con la matrícula ingresada.', 'error');

                    }else if(response.status == false){
                        $('#matricula_alumno').val('');
                        $('#matricula').val(matricula);
                        $('#matricula2').val(matricula);

                        $('.busqueda-matricula').hide();
                        $('.alumno-no-encontrado').show();

                    }
                });
            }else{
                Swal.fire('¡Ocurrio un error!', 'No se ha ingresado una matrícula en el campo de búsqueda.', 'error');
                $('#matricula_alumno').val('');
            }
        });


        /*
        |--------------------------------------------------------------------------
        |   Cuando se da click en el boton de cancelar
        |--------------------------------------------------------------------------
        */
        $('body').on('click', '.btn_cancelar', function () {
            window.location = "{{route('alumno.index')}}";
        });


        /*
        |--------------------------------------------------------------------------
        |   Funcion que carga el dropdown de grupos
        |--------------------------------------------------------------------------
        */
        $("#curso_id").change(function () {
            let curso_id = $(this).val();
            $('#grupo_id').prop('disabled', true);

            $.ajax({
                url: '{{ route("obtener.grupos") }}',
                type: 'get',
                dataType: 'json',
                data: {
                    curso_id: curso_id
                }
            }).always(function(response){
                if(Object.keys(response).length > 0){
                    $('#grupo_id').empty();
                    $('#grupo_id').append('<option hidden selected value="">-- Seleccionar --</option>');

                    $.each(response, function(index, value){
                        $('#grupo_id').append("<option value='" + value.id +"'>"+ value.grado + '-'+ value.letra +"</option>");
                    });

                    $('#grupo_id').prop('disabled', false);
                }else{
                    Swal.fire('¡Ocurrio un error!', 'Ha ocurrido un error', 'error');;
                }
            });
		});



        /*
        |--------------------------------------------------------------------------
        |  Funcion custom del curp para jquery validate
        |--------------------------------------------------------------------------
        */
        $.validator.addMethod("validateCurp", function (value, element) {
            if(validarInput(value)){
                return true;
            }else{
                return false;
            }

        }, "Formato del CURP incorrecto");


        /*
        |--------------------------------------------------------------------------
        |   Inicializa la validacion del formulario y envia el formulario
        |   solo si fueron aceptados los campos requeridos
        |--------------------------------------------------------------------------
        */
        $(form_id).validate({
            focusInvalid: true,
            rules: {
                curso_id: "required",
                grupo_id: "required",
                plantel_id: "required",
                nombre: "required",
                apellido_paterno: "required",
                apellido_materno: "required",
                sexo_id: "required",
                fecha_nacimiento: "required",
                curp: { required: true, validateCurp: true},
                domicilio: "required"
            },
            // Specify validation error messages
            messages: {
                curso_id: "Seleccione un nivel educativo.",
                grupo_id: "Seleccione un grupo.",
                plantel_id: "Seleccione un plantel.",
                nombre: "Ingrese el nombre del alumno.",
                apellido_paterno: "Ingrese el apellido paterno del alumno.",
                apellido_materno: "Ingrese el apellido materno del alumno.",
                sexo_id: "Seleccione el sexo del alumno.",
                fecha_nacimiento: "Ingrese la fecha de nacimiento del alumno.",
                curp: { required: "Ingrese el CURP.", validateCurp: 'CURP invalido'},
                domicilio: "Ingrese un domicilio del alumno.",
            },
            highlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            errorPlacement: function(error, element) {
                if(element.is(curso_id)){
                    error.appendTo($('#error-curso'));
                }
                else if(element.is(grupo_id)){
                    error.appendTo($('#error-grupo'));
                }
                else if(element.is(plantel_id)){
                    error.appendTo($('#error-plantel'));
                }
                else if(element.is(nombre)){
                    error.appendTo($('#error-nombre'));
                }
                else if(element.is(apellido_paterno)){
                    error.appendTo($('#error-apellido-paterno'));
                }
                else if(element.is(apellido_materno)){
                    error.appendTo($('#error-apellido-materno'));
                }
                else if(element.is(sexo_id)){
                    error.appendTo($('#error-sexo'));
                }
                else if(element.is(fecha_nacimiento)){
                    error.appendTo($('#error-fecha-nacimiento'));
                }
                else if(element.is(curp)){
                    error.appendTo($('#error-curp'));
                }
                else if(element.is(domicilio)){
                    error.appendTo($('#error-domicilio'));
                }
            },
            submitHandler: function(form) {
                $.ajax({
                    url:"{{ route('alumno.update') }}",
                    method: "post",
                    data: new FormData(form),
                    dataType:'JSON',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success:function(response){
                        if(response.code == 200){
                            Swal.fire({
                                title: 'Registro guardado',
                                text: "El alumno ahora se ecuentra actualizado.",
                                icon: 'success',
                                confirmButtonText: 'Ok'
                            }).then((result) => {
                                if (result.value) {
                                    window.location = '{{route("alumno.index")}}';
                                }else if(result.dismiss = 'cancel'){

                                }
                            });
                        }else if(response.code == 500){
                            Swal.fire('¡Ocurrio un error!', 'Ha ocurrido un error', 'error');
                        }
                    }
                });
            }
        });


    });
</script>
@endsection

