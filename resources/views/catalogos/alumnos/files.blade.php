@extends('layouts.app')

@section('style-css')
    <link rel="stylesheet" href="{{ asset("js/bootstrap-fileinput/css/fileinput.min.css") }}">
@endsection

@section('contenido')
    <div class="page-wrapper">
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h4 class="text-themecolor">CATÁLOGO DE ALUMNOS | ARCHIVOS ALUMNO</h4>
                </div>
                <div class="col-md-7 align-self-center text-right">
                    <div class="d-flex justify-content-end align-items-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">Catálogos</li>
                            <li class="breadcrumb-item">Alumnos</li>
                            <li class="breadcrumb-item active">Archivos</li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="POST" id="form_nuevo_alumno" autocomplete="off" enctype="multipart/form-data">
                                <input type="hidden" id="alumno_id" name="alumno_id" value="{{ $data->id }}">
                                @csrf
                                <div class="form-body">
                                    <h3 class="card-title bg-info text-white p-2 mb-3">Archivos</h3>

                                    <div class="d-flex mb-3 justify-content-end">
                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#cargarArchivos">
                                            Cargar archivo
                                        </button>
                                    </div>
                                    <div class="row mb-4">
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table class="table table-bordered dataTable display" cellspacing="0" width="100%"
                                                       id="table_archivos">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col" style="width: 5%">#</th>
                                                        <th scope="col" style="width: 10%">Nombre del archivo</th>
                                                        <th scope="col" style="width: 10%">Tipo de archivo</th>
                                                        <th scope="col" style="width: 20%">Acciones</th>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions float-right">
                                        <button type="button" class="btn btn-secondary btn_cancelar">Cancelar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="cargarArchivos" tabindex="-1" aria-labelledby="cargarArchivosLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="cargarArchivosLabel">Cargar archivo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="#" enctype="multipart/form-data" id="formulario_archivo">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="id_tipoArchivo" class="control-label">Selecciona el tipo de documento</label>
                                    <select name="id_tipoArchivo" id="id_tipoArchivo" class="form-control">
                                        <option value="">-- Seleccionar --</option>
                                        @foreach($tipoArchivos as $id => $tipoArchivo)
                                            <option value="{{ $id }}">{{ $tipoArchivo }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="nombreArchivo" class="control-label">Selecciona documento (.pdf)</label>
                                    <input type="file" class="form-control nombreArchivo" name="nombreArchivo" id="nombreArchivo" accept=".pdf">
                                    <small>Tamaño max. archivo: 8MB</small>
                                </div>
                            </div>

                            <input type="hidden" name="id_alumno" id="id_alumno" class="id_alumno" value="{{ $data->id }}">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="button" id="btn-guardarArchivo" class="btn btn-success">Cargar archivo</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script-js')
    <script src="{{ asset('js/bootstrap-fileinput/js/fileinput.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-fileinput/js/locales/es.js') }}"></script>
    <script src="{{ asset('js/bootstrap-fileinput/themes/fas/theme.min.js') }}"></script>
    <script>
        $(document).ready( function () {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $("#nombreArchivo").fileinput({
                language: 'es',
                allowedFileExtensions: ['pdf'],
                maxFileSize: 8000,
                showUpload: false,
                showClose: false,
                initialPreviewAsData: true,
                dropZoneEnabled: false,
                theme: "fas"
            });

            $(document).on('click', '#btn-guardarArchivo', function(e){
                e.preventDefault();
                let archivo = $('#nombreArchivo').val();
                
                if($.trim(archivo) != ''){
                    let data = new FormData($("#formulario_archivo").get(0));
                    $.ajax({
                        type: "POST",
                        url: "{{ route("multimedia.store") }}",
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: "JSON",
                        success: function(response) {
                            if(response.status === 200){
                                Swal.fire('Archivo cargado satisfactoriamente', '', 'success');
                                $('#cargarArchivos').modal('hide');
                                var oTable = $('#table_archivos').dataTable();
                                oTable.fnDraw(false);
                                $('#cargarArchivos').find('select').val("");
                                $('#nombreArchivo').fileinput('clear');
                            }
                        }
                    });
                }else {
                    Swal.fire('¡Ocurrio un error!', 'Por favor selecciona un archivo.', 'error');
                }
            });

            /*
            |--------------------------------------------------------------------------
            | Tabla donde se muestran todos los alumnos registrados en el sistema
            |--------------------------------------------------------------------------
            */
            let id = $('#id_alumno').val();
            $("#table_archivos").DataTable({
                processing: true,
                responsive: true,
                autoWidth: false,
                serverSide: true,
                destroy: true,
                ajax: {
                    url: "/catalogos/alumno/edit/"+id,
                    type: 'GET',
                },
                language: idiomaDataTable,
                columns: [{
                    data: 'id'
                },
                    {
                        data: 'nombreArchivo'
                    },
                    {
                        data: 'archivo.nombre',
                        render: function(data, type, row) {
                            return `<span class="badge bg-info font-weight-bold p-1 text-white">${data}</span>`;
                        }
                    },
                    {
                        data: 'action',
                        orderable: false
                    },
                ],
                order: [
                    [0, 'asc']
                ]
            });


            /*
            |--------------------------------------------------------------------------
            | Cuando se da click en descargar boleta se ejecuta lo siguiente
            |--------------------------------------------------------------------------
            */
            $(document).on('click', '.descargar_archivo', function(){
                let id = $(this).attr('data-id');
                let route = '/multimedia/download/'+id;
                let url = route.replace(":id", id);
                window.location = url;
            });


            /*
            |--------------------------------------------------------------------------
            | Cuando se da click en eliminar boleta se ejecuta lo siguiente
            |--------------------------------------------------------------------------
            */
            $('body').on('click', '.eliminar_archivo', function (e) {
                e.preventDefault();

                let id = $(this).attr('data-id');
                let route = '{{ route("multimedia.destroy", ":id") }}';
                let url = route.replace(":id", id);

                Swal.fire({
                    title: '¿Estás seguro?',
                    text: "Un registro eliminado no se puede recuperar.",
                    icon: 'warning',
                    showCancelButton: true,
                    cancelButtonText: 'Cancelar',
                    confirmButtonColor: '#dc3f4e',
                    confirmButtonText: 'Si, eliminar',
                    reverseButtons: true
                }).then((result) => {
                    if (result.value) {
                        axios.post(url, {'_method':'delete'}).then(respuesta => {
                            if (respuesta.data.code == 200) {
                                Swal.fire('Archivo eliminado', '', 'success');
                                var oTable = $('#table_archivos').dataTable();
                                oTable.fnDraw(false);
                            }else if(respuesta.data.code == 500){
                                Swal.fire('¡Ocurrio un error!', 'No se pudo eliminar el archivo', 'error');
                            }
                        }).catch(error => {
                            Swal.fire('¡Ocurrio un error!', 'No se pudo eliminar el archivo', 'error');

                        });
                    }else if(result.dismiss = 'cancel'){

                    }
                });
            });
        });
    </script>
@endsection

