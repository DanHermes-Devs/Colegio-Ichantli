@extends('layouts.app')
@section('contenido')
@inject('permisos', 'App\Services\Permisos')
<div class="page-wrapper">
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">ROLES</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Gestión de usuarios</li>
                        <li class="breadcrumb-item active">Roles</li>
                    </ol>
                    <button type="button" data-toggle="modal" id="create_rol" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Nuevo rol</button>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Roles</h4>
                        <h6 class="card-subtitle">Roles de acceso al sistema.</h6>
                        <div class="table-responsive m-t-40">
                            <table class="table table-bordered dataTable display" cellspacing="0" width="100%" id="rolesTable">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Nombre</th>
                                        <th scope="col">Acciones</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal" tabindex="-1" id="modal_rol" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">                  
                    <form method="post" id="form_rol" autocomplete="off">
                        @csrf
                        <input type="hidden" name="rol_id" id="rol_id">
                        <div class="modal-header">
                            <h4 class="modal-title" id="modalTitle"></h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group mb-2 col-md-12">
                                <label for="nombre_rol" class="control-label">Nombre <span class="field-required-span">*</span></label>
                                <input type="text" class="form-control" id="nombre_rol" name="nombre_rol">
                                <label id="error-nombre"></label>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="control-label">Selecciona los permisos que tendrá el rol <span class="field-required-span">*</span></label>
                                <ul class="icheck-list">
                                    {{-- https://github.com/bantikyan/icheck-material --}}
                                    {{-- https://github.com/bantikyan/icheck-bootstrap --}}

                                </ul>
                                @foreach ($permisos->get() as $permiso)
                                    <div class="icheck-material-blue">                                                
                                        <input type="checkbox" id="permiso_{{ $permiso->id }}" name="permisos[]" value="{{ $permiso->id }}">
                                        <label for="permiso_{{ $permiso->id }}">{{ $permiso->long_name }}</label>
                                    </div>
                                @endforeach
                                <label id="error-permisos"></label>
                            </div>                           
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Cerrar</button>
                            <button type="submit" id="btn_guardar" class="btn btn-success waves-effect waves-light">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script-js')
<script>
    $(document).ready( function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        
        /*
        |--------------------------------------------------------------------------
        |   Variables globales para las funciones
        |   Aqui lleno todos los valores para evitar estar cambiando en cada funcion
        |   los valores y asi evitar posibles fallos por olvidar cambiar un valor
        |--------------------------------------------------------------------------
        |    
        |    // Aqui va el id del llamado a la vista de nuevo
        |    var view_create = '#create_rol';
        |
        |    // Aqui va el id del llamado a la vista de ver
        |    var view_read = '#view_rol';
        |
        |    // Aqui va el id del llamado a la vista de editar
        |    var view_update = '#edit_rol';
        |
        |    // Aqui va el id del llamado a la vista de eliminar
        |    var view_delete = '#delete_rol';
        |
        |    // Aqui va el id del datatable
        |    var datatable_id = '#rolesTable';
        |
        |    // Aqui va el id del modal
        |    var modal_id = '#modal_rol';
        |
        |    // Aqui va el id del formulario
        |    var form_id = '#form_rol'; 
        |    
        |   // Aqui va el id del campo oculto
        |   var hidden_id = '#rol_id'; 
        |    
        |    // Aqui va el id del formulario y de lo que se quiere activar o desactivar
        |    var property_disabled = '#form_rol input';
        |
        |    // Aqui va el id del boton de guardar que siempre sera el mismo
        |    var btn_save = '#btn_guardar';
        */

        var view_create = '#create_rol';
        var view_read = '#view_rol';
        var view_update = '#edit_rol';
        var view_delete = '#delete_rol';
        var datatable_id = '#rolesTable';
        var modal_id = '#modal_rol';
        var form_id = '#form_rol'; 
        var hidden_id = '#rol_id';    
        var property_disabled = '#form_rol input, select';
        var btn_save = '#btn_guardar';
        

        /*
        |--------------------------------------------------------------------------
        |   Funcion que permite encapsular acciones que se repiten para asi
        |   tener un mejor control de las acciones repetitivas
        |--------------------------------------------------------------------------
        */
        function resetModal(opcion){
            if(opcion === 1){
                // opcion 1 para el boton de crear
                $(form_id).validate().resetForm(); 
                $(form_id)[0].reset();
                $(hidden_id).val('');
                $(modal_id).modal('show');
                $(property_disabled).prop("disabled", false);
                $(btn_save).show();

            }else if(opcion === 2){
                // opcion 2 para el boton de ver
                $(form_id).validate().resetForm();
                $(form_id)[0].reset(); 
                $(hidden_id).val('');
                $(modal_id).modal('show');
                $(property_disabled).prop("disabled", true);              
                $(btn_save).hide();

            }else if(opcion === 3){
                // opcion 3 para el boton de editar
                $(form_id).validate().resetForm();
                $(form_id)[0].reset(); 
                $(hidden_id).val('');
                $(modal_id).modal('show');
                $(property_disabled).prop("disabled", false);
                $(btn_save).show();

            }else if(opcion === 4){
                // opcion 4 para el boton de guardar del formulario crear y editar
                $(form_id).validate().resetForm(); 
                $(form_id)[0].reset();
                $(hidden_id).val('');  
                $(modal_id).modal('hide');
                $(property_disabled).prop("disabled", true);
                $(btn_save).show();
            }
        }


        /*
        |--------------------------------------------------------------------------
        |   Tabla donde se muestran los registros
        |--------------------------------------------------------------------------
        */
        $(datatable_id).DataTable({
            processing: false,
            serverSide: true,
            ajax: {
                url: "{{route('rol.index')}}",
                type: 'GET',
            },
            language: idiomaDataTable,
            columns: [
                {data: 'id', 'visible': false},
                {data: 'name'},
                {data: 'action', orderable: false},
            ],
            order: [[0, 'asc']]
        });


        /*
        |--------------------------------------------------------------------------
        |   Inicializa la validacion del formulario y envia el formulario
        |   solo si fueron aceptados los campos requeridos
        |--------------------------------------------------------------------------
        */
        $(form_id).validate({
            // ignore: ":hidden",
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                nombre_rol: "required", 
                'permisos[]': {required: true}        
            },
            // Specify validation error messages
            messages: {
                nombre_rol: "Ingresa el nombre del rol.",
                'permisos[]': "Selecciona al menos un permiso."        
            },
            highlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            errorPlacement: function(error, element) {
                // console.log(element.attr('id'));
                if (element.is(nombre_rol)) {
                    error.appendTo($('#error-nombre'));
                }
                else if(element.is(permiso_2)){
                    error.appendTo($('#error-permisos'));
                }
            },

            // errorLabelContainer: $('#consolaerror'),

            
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                var datos = new FormData(form);
                // console.log(datos.getAll('permisos[]'))
                axios.post("{{route('rol.store')}}",{
                    'rol_id': datos.get('rol_id'),
                    'nombre_rol': datos.get('nombre_rol'),
                    'permisos': datos.getAll('permisos[]')
                }).then((response) => {
                    //console.log(response) 
                    if(response.data.status === true){
                        resetModal(4);
                        Swal.fire('Registro Guardado', '', 'success');
                        var oTable = $(datatable_id).dataTable();
                        oTable.fnDraw(false);                        
                    }else{
                        Swal.fire('¡Ocurrio un error!', 'Ha ocurrido un error', 'error');
                    } 
                }).catch((error) => {
                    Swal.fire('¡Ocurrio un error!', error, 'error');
                });
            // form.submit();
            }
        });

        /*
        |--------------------------------------------------------------------------
        |   Cuando se da click en un nuevo se ejecuta lo siguiente
        |--------------------------------------------------------------------------
        */
        $(view_create).click(function () {
            $('#modalTitle').html("Nuevo rol");
            resetModal(1);
        });


        /*
        |--------------------------------------------------------------------------
        |   Cuando se da click en ver se ejecuta lo siguiente
        |--------------------------------------------------------------------------
        */
        $('body').on('click', view_read, function () {
            let url = $(this).attr('data-route');

            axios.get(url).then(response => {
                if (response.status == 200) {
                    $('#modalTitle').html("Ver rol: " + response.data.rol.name);
                    resetModal(2);

                    // ======================================
                    // Aqui van los campos (SE MODIFICA)
                    // ======================================
                    $('#rol_id').val(response.data.rol.id);
                    $('#nombre_rol').val(response.data.rol.name);

                    var arreglo_permisos = [];

                    $.each(response.data.permisos, function(index, val){
                        arreglo_permisos.push(val.id);
                    });

                    for(var permiso in arreglo_permisos){
                        $("input:checkbox[value="+arreglo_permisos[permiso]+"]").prop("checked", true).prop("disabled", true);
                    }
                }else{
                    Swal.fire('¡Ocurrio un error!', 'No se puede ver el registro', 'error');
                }
            }).catch(error => {
                Swal.fire('¡Ocurrio un error!', 'No se puede ver el registro', 'error');
            });
        });


        /*
        |--------------------------------------------------------------------------
        |   Cuando se da click en editar se ejecuta lo siguiente
        |--------------------------------------------------------------------------
        */
        $('body').on('click', view_update, function () {
            let url = $(this).attr('data-route');

            axios.get(url).then(response => {
                if (response.status == 200) {
                    $('#modalTitle').html("Editar rol: " + response.data.rol.name);
                    resetModal(3);

                    // ======================================
                    // Aqui van los campos
                    // ======================================
                    $('#rol_id').val(response.data.rol.id);
                    $('#nombre_rol').val(response.data.rol.name);
                    var arreglo_permisos = [];

                    $.each(response.data.permisos, function(index, val){
                        arreglo_permisos.push(val.id);
                    });

                    for(var permiso in arreglo_permisos){
                        $("input:checkbox[value="+arreglo_permisos[permiso]+"]").prop("checked", true);
                    }
                }else{
                    Swal.fire('¡Ocurrio un error!', 'No se puede ver el registro', 'error');
                }
            }).catch(error => {
                Swal.fire('¡Ocurrio un error!', 'No se puede ver el registro', 'error');
            });
        });


        /*
        |--------------------------------------------------------------------------
        |   Cuando se da click en eliminar se ejecuta lo siguiente
        |--------------------------------------------------------------------------
        */
        $('body').on('click', view_delete, function (e) { 
            e.preventDefault();
            var id = $(this).data('id');

            var route = '{{ route("rol.destroy", ":id") }}';
            var url = route.replace(":id", id);

            Swal.fire({
                title: '¿Estás seguro?',
                text: "Un registro eliminado no se puede recuperar.",
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                confirmButtonColor: '#dc3f4e',
                confirmButtonText: 'Si, eliminar',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    axios.post(url, {'_method':'delete'}).then(respuesta => {
                        // console.log(respuesta);
                        if (respuesta.status == 200) {
                            Swal.fire('Registro Eliminado', '', 'success');
                            var oTable = $(datatable_id).dataTable(); 
                            oTable.fnDraw(false);
                        }else{
                            Swal.fire('¡Ocurrio un error!', 'No se pudo eliminar el registro', 'error');
                        }
                    }).catch(error => {
                        Swal.fire('¡Ocurrio un error!', 'No se pudo eliminar el registro', 'error');

                    });
                }else if(result.dismiss = 'cancel'){

                }
		    });           
        }); 
    });
</script>
@endsection

