@if ($id != 1)
    <button type="button" data-toggle="tooltip" title="Botón de acción para ver" id="view_usuario" data-id="{{ $id }}" data-route="{{ route("usuario.show", $id) }}" class="btn btn-info"><i class="far fa-eye"></i></button>
    <button type="button" data-toggle="tooltip" title="Botón de acción para editar" id="edit_usuario" data-id="{{ $id }}" data-route="{{ route("usuario.edit", $id) }}" class="btn btn-warning"><i class="fa fa-pencil-alt"></i></button>
    <button type="button" data-toggle="tooltip" title="Botón de acción para cambiar contraseña" id="change_password" data-id="{{ $id }}" class="btn btn-success"><i class="fal fa-key"></i></button>
    <button type="button" data-toggle="tooltip" title="Botón de acción para eliminar" id="delete_usuario" data-id="{{ $id }}" class="btn btn-danger"><i class="fa fa-trash-alt"></i></button>
@else
    <button type="button" data-toggle="tooltip" title="Botón de acción para cambiar contraseña" id="change_password" data-id="{{ $id }}" class="btn btn-success"><i class="fal fa-key"></i></button>  
@endif
