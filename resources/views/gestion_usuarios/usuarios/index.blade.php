@extends('layouts.app')
@section('contenido')
@inject('generos', 'App\Services\Generos')
@inject('roles', 'App\Services\Roles')
<div class="page-wrapper">
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">USUARIOS</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Gestión de usuarios</li>
                        <li class="breadcrumb-item active">Usuarios</li>
                    </ol>
                    <button type="button" data-toggle="modal" id="create_usuario" class="btn btn-primary d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Nuevo usuario</button>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Usuarios</h4>
                        <h6 class="card-subtitle">Pueden acceder a distintos modulos del sistema dependiendo del rol que se les asigne.</h6>
                        <div class="table-responsive m-t-40">
                            <table class="table table-bordered dataTable display" id="usuariosTable" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Rol</th>
                                        <th scope="col">Nombre</th>
                                        <th scope="col">Usuario</th>
                                        <th scope="col">Acciones</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal" id="modal_usuario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">                  
                    <form method="post" id="form_usuario" autocomplete="off">
                        @csrf
                        <input type="hidden" name="usuario_id" id="usuario_id">
                        <div class="modal-header">
                            <h4 class="modal-title" id="modalTitle"></h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <div class="container">
                                {{-- Nombre, apellidos y sexo --}}
                                <div class="form-row mb-3" id="div-datos-basicos">
                                    <div class="col-md-3">
                                        <label for="nombre_usuario" class="control-label">Nombre(s) <span class="field-required-span">*</span></label>
                                        <input type="text" id='nombre_usuario' name="nombre_usuario" class="form-control">
                                        <div id="error-nombre"></div>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="apellido_paterno" class="control-label">Apellido paterno <span class="field-required-span">*</span></label>
                                        <input type="text" class="form-control" id="apellido_paterno" name="apellido_paterno">
                                        <div id="error-apellido-paterno"></div>
                                    </div> 
                                    <div class="col-md-3">
                                        <label for="apellido_materno" class="control-label">Apellido materno <span class="field-required-span">*</span></label>
                                        <input type="text" class="form-control" id="apellido_materno" name="apellido_materno">
                                        <div id="error-apellido-materno"></div>
                                    </div> 
                                    <div class="col-md-3">
                                        <label class="control-label" for="sexo_usuario">Sexo <span class="field-required-span">*</span></label>
                                        <select class="form-control" id="sexo_usuario" name="sexo_usuario">
                                            <option hidden selected value="">-- Seleccionar --</option>
                                            @foreach ($generos->get() as $genero)
                                                <option value="{{ $genero->id }}">{{ $genero->nombre }}</option>
                                            @endforeach
                                        </select>
                                        <div id="error-sexo"></div>
                                    </div>
                                </div>

                                {{-- correo electronico, rol y usuario --}}
                                <div class="form-row mb-3"> 
                                    <div class="col-md-3">
                                        <label class="control-label" for="email_usuario">Correo electrónico <span class="field-required-span">*</span></label>
                                        <input type="email" class="form-control" id="email_usuario" name="email_usuario">
                                        <div id="error-correo"></div>
                                    </div> 
                                    <div class="col-md-3">
                                        <label for="rol_id" class="control-label">Rol <span class="field-required-span">*</span></label>
                                        <select class="form-control" id="rol_id" name="rol_id">
                                            <option hidden selected value="">-- Seleccionar --</option>
                                                @foreach ($roles->get() as $rol)
                                                    <option value="{{ $rol->id }}">{{ $rol->name }}</option>
                                                @endforeach
                                        </select>
                                        <div id="error-rol"></div>
                                    </div> 
                                    <div class="col-md-3 usuario-div">
                                        <label for="nombre_de_usuario" class="control-label">Usuario <span class="field-required-span">*</span></label>
                                        <input type="text" class="form-control" id="nombre_de_usuario" name="nombre_de_usuario">
                                        <div id="error-usuario"></div>
                                    </div>                                
                                </div>

                                {{-- password y confirmar password --}}
                                <div class="form-row mb-3">
                                    <div class="col-md-3 password-div">
                                        <label for="password_usuario" class="control-label">Contraseña <span class="field-required-span">*</span></label>
                                        <input type="password" class="form-control" id="password_usuario" name="password_usuario">
                                        <div id="error-password"></div>
                                    </div>
                                    <div class="col-md-3 password-div">
                                        <label for="confirmar_password_usuario" class="control-label">Confirmar contraseña <span class="field-required-span">*</span></label>
                                        <input type="password" class="form-control" id="confirmar_password_usuario" name="confirmar_password_usuario">
                                        <div id="error-password-confirmar"></div>
                                    </div>
                                </div>
                            </div>
                      
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Cerrar</button>
                            <button type="submit" id="btn_guardar" class="btn btn-success waves-effect waves-light">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal" id="modal_password" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-md">
                <div class="modal-content">                  
                    <form method="post" id="form_password">
                        @csrf
                        <input type="hidden" name="usuario_id_password" id="usuario_id_password">
                        <div class="modal-header">
                            <h4 class="modal-title" id="modalTitlePassword"></h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <div class="container">
                                {{-- Nombre, apellidos y sexo --}}
                                <div class="form-row mb-3">
                                    <div class="col-md-12">
                                        <label for="new_password_usuario" class="control-label">Nueva contraseña <span class="field-required-span">*</span></label>
                                        <input type="password" class="form-control" id="new_password_usuario" name="new_password_usuario">
                                        <div id="error-new-password"></div>
                                    </div>
                                </div>
                                <div class="form-row mb-3">
                                    <div class="col-md-12">
                                        <label for="new_password_usuario_confirm" class="control-label">Confirmar nueva contraseña <span class="field-required-span">*</span></label>
                                        <input type="password" class="form-control" id="new_password_usuario_confirm" name="new_password_usuario_confirm">
                                        <div id="error-new-password-confirm"></div>
                                    </div>
                                </div>
                            </div>
                      
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-success waves-effect waves-light">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script-js')
<script>
    $(document).ready( function () {      
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        /*
        |--------------------------------------------------------------------------
        |   Variables globales para las funciones
        |   Aqui lleno todos los valores para evitar estar cambiando en cada funcion
        |   los valores y asi evitar posibles fallos por olvidar cambiar un valor
        |--------------------------------------------------------------------------
        |    
        |    // Aqui va el id del llamado a la vista de nuevo
        |    var view_create = '#create_rol';
        |
        |    // Aqui va el id del llamado a la vista de ver
        |    var view_read = '#view_rol';
        |
        |    // Aqui va el id del llamado a la vista de editar
        |    var view_update = '#edit_rol';
        |
        |    // Aqui va el id del llamado a la vista de eliminar
        |    var view_delete = '#delete_rol';
        |
        |    // Aqui va el id del datatable
        |    var datatable_id = '#rolesTable';
        |
        |    // Aqui va el id del modal
        |    var modal_id = '#modal_rol';
        |
        |    // Aqui va el id del formulario
        |    var form_id = '#form_rol'; 
        |    
        |   // Aqui va el id del campo oculto
        |   var hidden_id = '#rol_id'; 
        |    
        |    // Aqui va el id del formulario y de lo que se quiere activar o desactivar
        |    var property_disabled = '#form_rol input';
        |
        |    // Aqui va el id del boton de guardar que siempre sera el mismo
        |    var btn_save = '#btn_guardar';
        */

        var view_create = '#create_usuario';
        var view_read = '#view_usuario';
        var view_update = '#edit_usuario';
        var view_delete = '#delete_usuario';
        var view_password = '#change_password';
        var datatable_id = '#usuariosTable';
        var modal_id = '#modal_usuario';
        var form_id = '#form_usuario'; 
        var hidden_id = '#usuario_id';    
        var property_disabled = '#form_usuario input, select';
        var btn_save = '#btn_guardar';

        // Colocar esto ya que en los modales no funciona el search de select2
        jQuery.fn.modal.Constructor.prototype._enforceFocus = function() {};

        /*
        |--------------------------------------------------------------------------
        | Tabla donde se muestran todos los usuarios registrados en el sistema
        |--------------------------------------------------------------------------
        */
        $(datatable_id).DataTable({
            processing: false,
            serverSide: true,
            ajax: {
                url: "{{route('usuario.index')}}",
                type: 'GET',
            },
            language: idiomaDataTable,
            columns: [
                {data: 'id', 'visible': false},
                {data: 'roles[, ].name', name:'roles'},
                {data: 'full_name'},
                {data: 'usuario'},
                {data: 'action', orderable: false},
            ],
            order: [[0, 'asc']]
        });


        /*
        |--------------------------------------------------------------------------
        | Metodo que detecta el cambio del radio input es docente
        |--------------------------------------------------------------------------
        */
        $("input[name=is_docente]").change(function () {            
            if($(this).val() == 1){
                // Reseteo la validacion
                $(form_id).validate().resetForm();

                // Establezco los valores de los inputs en vacio
                $("#div-datos-basicos :input").val('');

                // Asi se limpia un select2
                $('#docente_id').val('').trigger('change');

                // Activa los campos cuando se selecciona que si es un docentes
                $("#div-datos-basicos :input").prop("readonly", true);

                $('#sexo_usuario').attr("style", "pointer-events: none; background-color: #e9ecef; opacity: 0.7;");
                // $("#sexo_usuario").hide();

                // Ocultar el div de datos cuando se selecciona que si es un docente
                $('#div-datos-basicos').hide();

                // Mostrar el select para seleccionar el docente cuando se selecciona que si es un docente
                $(".div-select-docente").show();

            }else if($(this).val() == 0){
                // Reseteo la validacion
                $(form_id).validate().resetForm();

                // Establezco los valores de los inputs en vacio
                $("#div-datos-basicos :input").val('');

                // Asi se limpia un select2
                $('#docente_id').val('').trigger('change');

                // Desactiva los campos cuando se selecciona que no es un docentes
                $("#div-datos-basicos :input").prop("readonly", false);

                $('#sexo_usuario').removeAttr("style");

                // Mostrar el div de datos cuando se selecciona que no es un docente
                $('#div-datos-basicos').show();
                
                // Ocultar el select para seleccionar el docente cuando se selecciona que no es un docente
                $(".div-select-docente").hide();
            }
		});


        /*
        |--------------------------------------------------------------------------
        |   Funcion que permite encapsular acciones que se repiten para asi
        |   tener un mejor control de las acciones repetitivas
        |--------------------------------------------------------------------------
        */
        function resetModal(opcion){
            if(opcion === 1){
                // opcion 1 para el boton de crear
                $(form_id).validate().resetForm();            
                $(form_id)[0].reset();
                $(hidden_id).val('');
                $(modal_id).modal('show');
                $(property_disabled).prop("disabled", false);
                $(property_disabled).prop("readonly", false);
                $(btn_save).show();

                $('#sexo_usuario').removeAttr("style");
                $('#div-datos-basicos').show();
                $('.password-div').show();
                $('.usuario-div').show();
                

            }else if(opcion === 2){
                // opcion 2 para el boton ver
                $(form_id).validate().resetForm();            
                $(form_id)[0].reset();
                $(hidden_id).val('');
                $(modal_id).modal('show');
                $(property_disabled).prop("disabled", true);
                $(property_disabled).prop("readonly", false);
                $(btn_save).hide();

                $('#sexo_usuario').removeAttr("style");
                $('#div-datos-basicos').show();
                $('.password-div').hide();
                $('.usuario-div').hide();

            }else if(opcion === 3){
                // opcion 3 para el boton de editar
                $(form_id).validate().resetForm();            
                $(form_id)[0].reset();
                $(hidden_id).val('');
                $(modal_id).modal('show');
                $(property_disabled).prop("disabled", false);
                $(property_disabled).prop("readonly", false);
                $(btn_save).show();

                $('#sexo_usuario').removeAttr("style");
                $('#div-datos-basicos').show();
                $('.password-div').hide();
                $('.usuario-div').hide();

            }else if(opcion === 4){
                // opcion 4 para el boton de guardar
                $(form_id).validate().resetForm(); 
                $(form_id)[0].reset();
                $(hidden_id).val('');
                $(modal_id).modal('hide');
                $(property_disabled).prop("disabled", false);
                $(property_disabled).prop("readonly", false);
                $(btn_save).show();
                
                $('#sexo_usuario').removeAttr("style");
            }
        }


        jQuery.validator.addMethod('passwordMatch', function(value, element) {
            // The two password inputs
            var password = $("#password_usuario").val();
            var confirmPassword = $("#confirmar_password_usuario").val();

            // Check for equality with the password inputs
            if (password != confirmPassword ) {
                return false;
            } else {
                return true;
            }

        }, "Las contraseñas no coinciden.");


        /*
        |--------------------------------------------------------------------------
        |   Inicializa la validacion del formulario y envia el formulario
        |   solo si fueron aceptados los campos requeridos
        |--------------------------------------------------------------------------
        */
        $(form_id).validate({
            // ignore: ":hidden",
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                nombre_usuario: "required",
                apellido_paterno: "required",
                apellido_materno: "required",
                sexo_usuario: "required",
                email_usuario: "required",
                nombre_de_usuario: {
                    required: true,
                    remote: {
                        url: '{{route('usuario.validateUsuario')}}',
                        type: "post"
                    }
                },
                password_usuario: {
                    required: true,
                    minlength: 6
                },
                confirmar_password_usuario: {
                    required: true,
                    minlength: 6,
                    passwordMatch: true // set this on the field you're trying to match
                },
                rol_id: "required"             
            },
            // Specify validation error messages
            messages: {
                nombre_usuario: "Ingresa el nombre.",
                apellido_paterno: "Ingresa el apellido paterno.",
                apellido_materno: "Ingresa el apellido materno.",
                sexo_usuario: "Selecciona el sexo.",
                email_usuario: "Ingresa un correo electrónico.",
                nombre_de_usuario: {
                    required: "Ingresa un nombre de usuario.",
                    remote: "Ya existe un usuario con el mismo nombre."
                },
                password_usuario: {
                    required: "Ingresa una contraseña.",
                    minlength: "La contraseña debe contener como minimo 6 caracteres."
                },
                confirmar_password_usuario: {
                    required: "Confirma la contraseña.",
                    minlength: "La contraseña debe contener como minimo 6 caracteres.",
                    equalTo : "Las contraseñas no coinciden."
                },
                rol_id: "Selecciona un rol."          
            },
            highlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            errorPlacement: function(error, element) {
                if(element.is(nombre_usuario)) {
                    error.appendTo($('#error-nombre'));
                }
                else if(element.is(apellido_paterno)){
                    error.appendTo($('#error-apellido-paterno'));
                }
                else if(element.is(apellido_materno)){
                    error.appendTo($('#error-apellido-materno'));
                }
                else if(element.is(sexo_usuario)){
                    error.appendTo($('#error-sexo'));
                }
                else if(element.is(email_usuario)){
                    error.appendTo($('#error-correo'));
                }
                else if(element.is(nombre_de_usuario)){
                    error.appendTo($('#error-usuario'));
                }
                else if(element.is(password_usuario)){
                    error.appendTo($('#error-password'));
                }
                else if(element.is(confirmar_password_usuario)){
                    error.appendTo($('#error-password-confirmar'));
                }
                else if(element.is(rol_id)){
                    error.appendTo($('#error-rol'));
                }
            },

            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                var datos = new FormData(form);
                // for (var pair of datos.entries()) {
                //     console.log(pair[0]+ ' - ' + pair[1]); 
                // }
                axios.post("{{route('usuario.store')}}",{
                    'id': datos.get('usuario_id'),
                    'nombre': datos.get('nombre_usuario'),
                    'apellido_paterno':datos.get('apellido_paterno'),
                    'apellido_materno':datos.get('apellido_materno'),
                    'sexo_id': parseInt(datos.get('sexo_usuario')),
                    'email':datos.get('email_usuario'),
                    'usuario':datos.get('nombre_de_usuario'),
                    'password':datos.get('password_usuario'),
                    'role_id': datos.get('rol_id'),
                }).then((response) => {
                    if(response.data.status === true){                        
                        Swal.fire('Registro Guardado', '', 'success');
                        resetModal(4);
                        var oTable = $(datatable_id).dataTable();
                        oTable.fnDraw(false); 
                    }else if(response.data.status == 'user_exist'){
                        Swal.fire('¡Ocurrio un error!', 'El nombre de usuario ya existe intenta con otro.', 'error');
                        
                    }else{
                        Swal.fire('¡Ocurrio un error!', 'Ha ocurrido un error', 'error');
                    }
                }).catch((error) => {
                    Swal.fire('¡Ocurrio un error!', error, 'error');
                });
            }
        });


        /*
        |--------------------------------------------------------------------------
        | Cuando se da click en nuevo se ejecuta lo siguiente
        |--------------------------------------------------------------------------
        */
        $(view_create).click(function () {
            $('#modalTitle').html("Nuevo usuario");
            resetModal(1);
        });


        /*
        |--------------------------------------------------------------------------
        | Cuando se da click en ver se ejecuta lo siguiente
        |--------------------------------------------------------------------------
        */
        $('body').on('click', view_read, function () {
            let url = $(this).attr('data-route');

            axios.get(url).then(response => {
                if (response.status == 200) {
                    $('#modalTitle').html("Ver usuario: " + response.data.full_name);
                    resetModal(2);

                    // ======================================
                    // Aqui van los campos
                    // ======================================
                    $('#usuario_id').val(response.data.id);
                    $('#nombre_usuario').val(response.data.nombre);
                    $('#apellido_paterno').val(response.data.apellido_paterno);
                    $('#apellido_materno').val(response.data.apellido_materno);
                    $('#sexo_usuario').val(response.data.sexo_id);
                    $('#email_usuario').val(response.data.email);
                    $('#nombre_de_usuario').val(response.data.usuario);       
                    $('#rol_id').val(response.data.roles[0].id);
                }else{
                    Swal.fire('¡Ocurrio un error!', 'No se puede ver el registro', 'error');
                }
            }).catch(error => {
                Swal.fire('¡Ocurrio un error!', 'No se puede ver el registro', 'error');
            });

        });


        /*
        |--------------------------------------------------------------------------
        | Cuando se da click en editar se ejecuta lo siguiente
        |--------------------------------------------------------------------------
        */
        $('body').on('click', view_update, function () {
            let url = $(this).attr('data-route');

            axios.get(url).then(response => {
                if (response.status == 200) {
                    $('#modalTitle').html("Editar usuario: " + response.data.full_name);
                    resetModal(3);

                    // ======================================
                    // Aqui van los campos
                    // ======================================
                    $('#usuario_id').val(response.data.id);
                    $('#nombre_usuario').val(response.data.nombre);
                    $('#apellido_paterno').val(response.data.apellido_paterno);
                    $('#apellido_materno').val(response.data.apellido_materno);
                    $('#sexo_usuario').val(response.data.sexo_id);
                    $('#email_usuario').val(response.data.email);
                    $('#nombre_de_usuario').val(response.data.usuario);       
                    $('#rol_id').val(response.data.roles[0].id);
                }else{
                    Swal.fire('¡Ocurrio un error!', 'No se puede ver el registro', 'error');
                }
            }).catch(error => {
                Swal.fire('¡Ocurrio un error!', 'No se puede ver el registro', 'error');
            });
        });


        /*
        |--------------------------------------------------------------------------
        | Cuando se da click en eliminar se ejecuta lo siguiente
        |--------------------------------------------------------------------------
        */
        $('body').on('click', view_delete, function (e) { 
            e.preventDefault();
            var id = $(this).data('id'); 

            var route = '{{ route("usuario.destroy", ":id") }}';
            var url = route.replace(":id", id);

            Swal.fire({
                title: '¿Estás seguro?',
                text: "Un registro eliminado no se puede recuperar.",
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                confirmButtonColor: '#dc3f4e',
                confirmButtonText: 'Si, eliminar',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    axios.post(url, {'_method':'delete'}).then(respuesta => {
                        // console.log(respuesta);
                        if (respuesta.status == 200) {
                            Swal.fire('Registro Eliminado', '', 'success');
                            var oTable = $(datatable_id).dataTable(); 
                            oTable.fnDraw(false);
                        }else{
                            Swal.fire('¡Ocurrio un error!', 'No se pudo eliminar el registro', 'error');
                        }
                    }).catch(error => {
                        Swal.fire('¡Ocurrio un error!', 'No se pudo eliminar el registro', 'error');

                    });
                }else if(result.dismiss = 'cancel'){

                }
		    });         
        }); 


        /*
        |--------------------------------------------------------------------------
        | Cuando se da click en cambiar password se ejecuta lo siguiente
        |--------------------------------------------------------------------------
        */
        $('body').on('click', view_password, function () { 
            $('#form_password').validate().resetForm();            
            $('#usuario_id_password').val('');
            $('#form_password')[0].reset()
            $('#modalTitlePassword').html("Cambiar la contraseña");
            $('#modal_password').modal('show');
            
            var usuario_id_password = $(this).data('id');
            $('#usuario_id_password').val(usuario_id_password);
     
        });


        /*
        |--------------------------------------------------------------------------
        |   Inicializa la validacion del formulario de cambiar password 
        |    y envia el formulario solo si fueron aceptados los campos requeridos
        |--------------------------------------------------------------------------
        */
        $('#form_password').validate({
            // ignore: ":hidden",
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                new_password_usuario: {
                    required: true,
                    minlength: 6
                },
                new_password_usuario_confirm: {
                    required: true,
                    minlength: 6,
                    equalTo : "#new_password_usuario"
                },           
            },
            // Specify validation error messages
            messages: {
                new_password_usuario: {
                    required: "Ingresa una nueva contraseña.",
                    minlength: "La contraseña debe contener como minimo 6 caracteres."
                },  
                new_password_usuario_confirm: {
                    required: "Confirma la nueva contraseña.",
                    minlength: "La contraseña debe contener como minimo 6 caracteres.",
                    equalTo : "Las contraseñas no coinciden."
                },       
            },

            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                var datos = new FormData(form);
                // for (var pair of datos.entries()) {
                //     console.log(pair[0]+ ' - ' + pair[1]); 
                // }
                axios.post("{{route('usuario.updatePassword')}}",{
                    'id': datos.get('usuario_id_password'),
                    'password':datos.get('new_password_usuario_confirm'),
                }).then((response) => {
                    if(response.data.status === true){                        
                        Swal.fire('Contraseña Cambiada', '', 'success');
                        $('#form_password').validate().resetForm();            
                        $('#usuario_id_password').val('');
                        $('#form_password')[0].reset()
                        $('#modal_password').modal('hide');

                        var oTable = $(datatable_id).dataTable();
                        oTable.fnDraw(false); 
                    }else{
                        Swal.fire('¡Ocurrio un error!', 'Ha ocurrido un error', 'error');
                    }
                }).catch((error) => {
                    Swal.fire('¡Ocurrio un error!', error, 'error');
                });
            }
        });


    });
</script>
@endsection

