@extends('layouts.app')
@section('contenido')
<div class="page-wrapper">
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">INICIO</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
                        {{-- <li class="breadcrumb-item active">Empleados</li> --}}
                    </ol>
                    {{-- <button type="button" data-toggle="modal" id="nuevo-empleado-button" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Nuevo Empleado</button> --}}
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">{{ (Auth::user()->sexo_id == 1)? 'Bienvenido': 'Bienvenida' }} al sistema de boletas del Colegio Ichantli.</h4>
                        <h6 class="card-subtitle">Del lado izquierdo se encuentra el menú, donde podrás navegar entre las distintas opciones que el sistema te proporciona.</h6>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-12">
                <div class="card grid-cards">
                    <div class="img-user flex-cards bg-success">
                        <i class="text-white fas fa-user"></i>
                    </div>
                    <div class="card-title m-0 d-flex align-items-center justify-content-between">
                        Alumnos registrados: 
                        <span class="ml-4 fz-1-5 font-weight-bold count-alumnos">
                            {{ $alumnos }}
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-12">
                <div class="card grid-cards">
                    <div class="img-user-slash flex-cards bg-danger">
                        <i class="text-white fas fa-user-slash"></i>
                    </div>
                    <div class="card-title m-0 d-flex align-items-center justify-content-between">
                        Alumnos con adeudo:
                        <span class="ml-4 fz-1-5 font-weight-bold count-alumnos">
                            {{ $alumnos_adeudo }}
                        </span>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-12">
                <div class="card grid-cards">
                    <div class="img-circular flex-cards bg-info">
                        <i class="text-white fas fa-newspaper"></i>
                    </div>
                    <div class="card-title m-0 d-flex align-items-center justify-content-between">
                        Total circulares:
                        <span class="ml-4 fz-1-5 font-weight-bold count-alumnos">
                            {{ $circulares }}
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

