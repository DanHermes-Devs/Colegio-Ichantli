@extends('layouts.app-alumnos')
@section('contenido')
<div class="page-wrapper">
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Mi Panel</h4>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row">
            <!-- Column -->
            <div class="col-lg-3 col-xlg-3 col-md-3">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <center>
                                    <img src="{{ asset('images/logo.png') }}" alt="homepage" class="dark-logo" style="width: 35%;"/>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-img mt-3">
                                @if (!$alumno->foto)
                                <img class="card-img-top rounded-lg d-flex w-35 m-auto" src="http://gravatar.com/avatar/404?d=mp" alt="Sin Imagen">
                                @endif
                                <img class="card-img-top rounded-lg d-flex w-35 m-auto" src="{{ asset('store/alumnos/fotografias/'.$alumno->foto) }}" alt="{{ $alumno->foto }}">
                                
                            </div>

                            <div class="card-body">
                                <center>{{-- m-t-30 --}}
                                    <h4 class="card-title mt-1">{{$alumno->nombre_completo}}</h4> <!-- m-t-10 -->
                                    <h6 class="card-subtitle font-weight-bold" style="color: #A80735;">{{ $alumno->curso->nombre }}</h6>
                                </center>
                            </div>
                            <hr>
                             
                            <div class="card-body"> 
                                <small class="text-muted">Matrícula </small>
                                <h6>{{ $alumno->matricula }}</h6> 
                                
                                <small class="text-muted" style="font-size: 12px;">Grupo </small>
                                <h6>{{ $alumno->grupo->grado }}-{{ $alumno->grupo->letra }}</h6>  
                                
                                <small class="text-muted">Correo electrónico </small>
                                @if ($alumno->email_tutor)
                                    <h6>{{$alumno->email_tutor}}</h6> 
                                @else
                                    <h6 class="text-danger">No se ha registrado.</h6> 
                                @endif                
                                
                                <small class="text-muted p-t-30 db">Teléfono</small>
                                @if ($alumno->telefono_tutor)
                                    <h6>{{$alumno->telefono_tutor}}</h6> 
                                @else
                                    <h6 class="text-danger">No se ha registrado.</h6> 
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-9 col-xlg-9 col-md-9">
                <div class="card">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs profile-tab" role="tablist">
                        <li class="nav-item"> <a class="nav-link font-weight-bold active" data-toggle="tab" href="#home" role="tab">Mis boletas</a> </li>
                        <li class="nav-item"> <a class="nav-link font-weight-bold" data-toggle="tab" href="#circulares" role="tab">Circulares</a> </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="home" role="tabpanel">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        @if ($alumno->activado == 1)
                                        <input type="hidden" id="alumno_id" value="{{ $alumno->id }}">
                                            <div class="table-responsive">
                                                <table class="table table-bordered dataTable display" cellspacing="0" width="100%" id="table_boletas">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">#</th>
                                                            <th scope="col">Materia</th>
                                                            <th scope="col">Boleta</th>
                                                            <th scope="col">Fecha</th>
                                                            <th scope="col">Acciones</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        @else
                                        <div class="alert alert-danger" role="alert">
                                            <h4 class="alert-heading">¡Oops! parece que tienes pagos pendientes.</h4>
                                            <hr>
                                            <p class="mb-0">Por favor acude a realizar tu pago por el monto de: ${{ $alumno->monto }}</p>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="circulares" role="tabpanel">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-bordered dataTable display" cellspacing="0" width="100%" id="table_circulares">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">#</th>
                                                        <th scope="col">Nombre</th>
                                                        <th scope="col">Descripción</th>
                                                        <th scope="col">Fecha</th>
                                                        <th scope="col">Acciones</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>
    </div>
</div>
@endsection


@section('script-js')
@routes
<script>
    $(document).ready( function () {

        let alumno_id = $('#alumno_id').val();  
        let route = '{{ route('alumno.home', [':id']) }}';
        let url = route.replace(":id", alumno_id);


        /*
        |--------------------------------------------------------------------------
        | Tabla para mostrar los datos de las boletas
        |--------------------------------------------------------------------------
        */
        $('#table_boletas').DataTable({
            processing: false,
            serverSide: true,
            ajax: {
                url: url,
                type: 'GET',
            },
            language: idiomaDataTable,
            columns: [
                {data: 'id'},
                { 
                    data: 'materia',
                    render: function ( data, type, row ) {
                        if(data == 1){
                            return 'Español';
                        }else if(data == 2){
                            return 'Inglés';
                        }else if(data == 3){
                            return 'Francés';
                        }
                    }
                },
                {data: 'nombre'},
                {data: 'fecha_subida'},
                { 
                    data: 'id',
                    render: function ( data, type, row ) {
                        return `<button type="button" data-toggle="tooltip" title="Botón de acción para descargar boleta del alumno" data-id="${data}" class="btn btn-success descargar_boleta"><i class="fas fa-file-download"></i></button>`;
                    }
                }
            ],
            order: [[0, 'desc']]
        });


        /*
        |--------------------------------------------------------------------------
        | Tabla para mostrar los datos de las circulares
        |--------------------------------------------------------------------------
        */
        $('#table_circulares').DataTable({
            processing: false,
            serverSide: true,
            ajax: {
                url: "{{ route('alumno_panel.circulares')}}",
                type: 'GET',
            },
            language: idiomaDataTable,
            columns: [
                {data: 'id'},
                {data: 'nombre'},
                {data: 'descripcion'},
                {data: 'fecha_subida'},
                { 
                    data: 'id',
                    render: function ( data, type, row ) {
                        return `<button type="button" data-toggle="tooltip" title="Botón de acción para descargar circular" data-id="${data}" class="btn btn-success descargar_circular"><i class="fas fa-file-download"></i></button>`;
                    }
                }
            ],
            order: [[0, 'desc']]
        });


        /*
        |--------------------------------------------------------------------------
        | Cuando se da click en descargar boleta se ejecuta lo siguiente
        |--------------------------------------------------------------------------
        */
        $('body').on('click', '.descargar_boleta', function () {
            let id = $(this).attr('data-id');
            let route = '{{ route('alumno_panel.download_boleta', [':id']) }}';
            let url = route.replace(":id", id);

            window.location = url;
        });


        /*
        |--------------------------------------------------------------------------
        | Cuando se da click en descargar circular se ejecuta lo siguiente
        |--------------------------------------------------------------------------
        */
        $('body').on('click', '.descargar_circular', function () {
            let id = $(this).attr('data-id');
            let route = '{{ route('alumno_panel.download_circular', [':id']) }}';
            let url = route.replace(":id", id);

            window.location = url;
        });
    });
</script>
@endsection
