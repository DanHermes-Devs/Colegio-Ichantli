@extends('layouts.app')
@section('contenido')
<div class="page-wrapper">
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">BOLETAS | SUBIR MASIVAMENTE</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Boletas</li>
                        <li class="breadcrumb-item active">Subir masivamente</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row primer-formulario mb-3">
                            <div class="col-md-12">
                                <form action="POST" id="form_archivo_excel" autocomplete="off" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="dropzone" class="control-label">Seleccionar el archivo de excel (solo 100 registros por archivo): <span class="field-required-span">&nbsp;</span></label>
                                                    <input type="file" class="form-control" id="archivo" name="archivo" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-actions float-right">
                                            <button type="submit" class="btn btn-success btn_guardar" id="btn_guardar">Importar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="row segundo-formulario" style="display: none;">
                            <div class="col-md-12">
                                <label for="dropzone" class="control-label">Elegir unicamente las boletas que corresponden a la importación previa: <span class="field-required-span">&nbsp;</span></label>
                                <form action="POST" class="dropzone" id="dropzone" autocomplete="off" enctype="multipart/form-data">
                                    @csrf
                                </form>
                                <div class="form-actions float-right mt-3">
                                    <button type="button" class="btn btn-primary btn_finalizar" style="display: none;">Finalizar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script-js')
<script>
    Dropzone.options.dropzone = {
        url: '{{ route('boleta.store_masivamente') }}',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        uploadMultiple: true,
        parallelUploads: 10,
        maxFilesize: 100,
        dictRemoveFile: 'Remover archivo',
        dictDefaultMessage: 'Da clic para subir un máximo de 100 archivos, o arrastra y suelta.',
        dictCancelUpload: 'Cancelar',
        acceptedFiles: ".pdf",
        addRemoveLinks: true,
        timeout: 60000,
        removedfile: function(file) {
            var name = file.upload.filename;
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                url: '{{ route("boleta.destroy_masivamente") }}',
                data: {filename: name},
                success: function (data){
                },
                error: function(e) {
                }
            });
            var fileRef = file.previewElement;
            return (fileRef != null) ? fileRef.parentNode.removeChild(file.previewElement) : void 0;
        },
        success: function (file, response) {
            // console.log(response);
        },
        successmultiple(files, res) {
            let contador = 0;
            res.data.map(function(item) {
                if (item.success) {
                    contador++;
                }else {
                    // something went wrong
                }
            })

            if(contador == files.length){
                Swal.fire('Carga de boletas exitosa', '', 'success');
                $('.btn_finalizar').show();
            }

            // we have finished processing this group of files
            stillSending = false
        },
        error: function (file, response) {
            return false;
        }
    };

    $(document).ready( function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        /*
        |--------------------------------------------------------------------------
        | Cuando se da click en importar
        |--------------------------------------------------------------------------
        */
        $('body').on('click', '.btn_guardar', function (e) {
            e.preventDefault();

            let archivo = $('#archivo').val();

            if($.trim(archivo) != ''){
                var form = $('#form_archivo_excel')[0];
                var formData = new FormData(form);
                $.ajax({
                    url: "{{ route('boleta.import') }}",
                    method: "POST",
                    data: formData,
                    dataType:'JSON',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success:function(response){
                        if(response.code == 200){
                            Swal.fire('Importación exitosa', '', 'success');

                            $('.primer-formulario').hide();
                            $('.segundo-formulario').show();
                        }else if(response.code == 500){
                            Swal.fire('¡Ocurrio un error!', 'Ha ocurrido un error.', 'error');
                        }
                    }
                });
            }else{
                Swal.fire('¡Ocurrio un error!', 'Por favor selecciona un archivo de excel.', 'error');
            }
        });


        /*
        |--------------------------------------------------------------------------
        | Cuando se da click en el boton de finalizar
        |--------------------------------------------------------------------------
        */
        $('body').on('click', '.btn_finalizar', function (e) {
            window.location = "{{route('boleta.index')}}";
        });
    });
</script>
@endsection

