<button type="button" data-toggle="tooltip" title="Botón de acción para ver boletas del alumno" id="ver_boleta" data-id="{{ $id }}" class="btn btn-info"><i class="far fa-eye"></i></button>
<button type="button" data-toggle="tooltip" title="Botón de acción para agregar boleta" id="agregar_boleta" data-id="{{ $id }}" class="btn btn-warning"><i class="fas fa-upload"></i></button>


@if($activado == 1)
    <button type="button" data-toggle="tooltip" title="Botón de acción para desactivar alumno" id="desactivar_alumno" data-id="{{ $id }}" data-route="{{ route("alumno.desactivar", $id) }}" class="btn btn-danger"><i class="fas fa-user-times"></i></button>
@else
    <button type="button" data-toggle="tooltip" title="Botón de acción para activar alumno" id="activar_alumno" data-id="{{ $id }}" data-route="{{ route("alumno.activar", $id) }}" class="btn btn-success"><i class="fas fa-user-check"></i></button>
@endif


