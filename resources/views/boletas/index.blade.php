@extends('layouts.app')
@section('contenido')
@inject('generos', 'App\Services\Generos')
<div class="page-wrapper">
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">BOLETAS | LISTA DE ALUMNOS</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Boletas</li>
                        <li class="breadcrumb-item active">Lista de alumnos</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <h4 class="card-title">Boletas</h4>
                                <h6 class="card-subtitle">Lista de alumnos.</h6>
                            </div>
                            <div class="col-md-6">
                                <button class="btn btn-sunflower float-right subir-masivamente"><i class="fas fa-file-pdf"></i> Subir masivamente</button>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label class="control-label" for="ciclo_id">Selecciona un nivel educativo: </label>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-12">
                                <div class="icheck-material-blue icheck-inline">
                                    <input type="radio" id="tipo_curso1" name="tipo_curso" value="1"/>
                                    <label for="tipo_curso1">Kinder</label>
                                </div>
                                <div class="icheck-material-blue icheck-inline">
                                    <input type="radio" id="tipo_curso2" name="tipo_curso" value="2"/>
                                    <label for="tipo_curso2">Preescolar</label>
                                </div>
                                <div class="icheck-material-blue icheck-inline">
                                    <input type="radio" id="tipo_curso3" name="tipo_curso" value="3"/>
                                    <label for="tipo_curso3">Primaria</label>
                                </div>
                                <div class="icheck-material-blue icheck-inline">
                                    <input type="radio" id="tipo_curso4" name="tipo_curso" value="4"/>
                                    <label for="tipo_curso4">Secundaria</label>
                                </div>
                            </div>
                        </div>
                        <hr>

                        <style>
                            table.dataTable tbody tr.dtrg-group:hover{
                                cursor: default;
                            }
                        </style>

                        <div class="row curso-1" style="display: none;">
                            <div class="col-md-12">
                                <div class="table-responsive mt-3">
                                    <table class="table table-bordered dataTable display" cellspacing="0" width="100%" id="table_1">
                                        <thead>
                                            <tr>
                                                {{-- <th scope="col" style="width: 5%">#</th> --}}
                                                <th scope="col" style="width: 10%">Matrícula</th>
                                                <th scope="col" style="width: 10%">Plantel</th>
                                                <th scope="col" style="width: 5%">Grupo</th>
                                                <th scope="col" style="width: 30%">Nombre</th>
                                                <th scope="col" style="width: 20%">Acciones</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row curso-2" style="display: none;">
                            <div class="col-md-12">
                                <div class="table-responsive mt-3">
                                    <table class="table table-bordered dataTable display" cellspacing="0" width="100%" id="table_2">
                                        <thead>
                                            <tr>
                                                {{-- <th scope="col" style="width: 5%">#</th> --}}
                                                <th scope="col" style="width: 10%">Matrícula</th>
                                                <th scope="col" style="width: 10%">Plantel</th>
                                                <th scope="col" style="width: 5%">Grupo</th>
                                                <th scope="col" style="width: 30%">Nombre</th>
                                                <th scope="col" style="width: 20%">Acciones</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row curso-3" style="display: none;">
                            <div class="col-md-12">
                                <div class="table-responsive mt-3">
                                    <table class="table table-bordered dataTable display" cellspacing="0" width="100%" id="table_3">
                                        <thead>
                                            <tr>
                                                {{-- <th scope="col" style="width: 5%">#</th> --}}
                                                <th scope="col" style="width: 10%">Matrícula</th>
                                                <th scope="col" style="width: 10%">Plantel</th>
                                                <th scope="col" style="width: 5%">Grupo</th>
                                                <th scope="col" style="width: 30%">Nombre</th>
                                                <th scope="col" style="width: 20%">Acciones</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row curso-4" style="display: none;">
                            <div class="col-md-12">
                                <div class="table-responsive mt-3">
                                    <table class="table table-bordered dataTable display" cellspacing="0" width="100%" id="table_4">
                                        <thead>
                                            <tr>
                                                {{-- <th scope="col" style="width: 5%">#</th> --}}
                                                <th scope="col" style="width: 10%">Matrícula</th>
                                                <th scope="col" style="width: 10%">Plantel</th>
                                                <th scope="col" style="width: 5%">Grupo</th>
                                                <th scope="col" style="width: 30%">Nombre</th>
                                                <th scope="col" style="width: 20%">Acciones</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal" tabindex="-1" id="modal_upload_boleta" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="post" id="form_upload_boleta" autocomplete="off">
                        @csrf
                        <input type="hidden" name="alumno_id_2" id="alumno_id_2">
                        <div class="modal-header">
                            <h4 class="modal-title" id="modalTitle">Subir boleta</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="monto" class="control-label">Materia: <span class="field-required-span">*</span></label>
                                    <select class="form-control" id="materia_id" name="materia_id">
                                        <option hidden selected value="">-- Seleccionar --</option>
                                        <option value="1">Español</option>
                                        <option value="2">Inglés</option>
                                        <option value="3">Francés</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group mb-0">
                                    <label for="archivo" class="control-label">Elegir el archivo de la boleta: <span class="field-required-span">*</span></label>
                                    <input type="file" class="dropify archivo" accept=".pdf"  id="archivo" name="archivo" data-height="150" data-show-remove="true" data-allowed-file-extensions="pdf"  data-errors-position="outside" data-max-file-size="5M"/>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Cerrar</button>
                            <button type="submit" id="btn_guardar_boleta" class="btn btn-success waves-effect waves-light">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal" tabindex="-1" id="modal_monto" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="post" id="form_monto" autocomplete="off">
                        @csrf
                        <input type="hidden" name="alumno_id" id="alumno_id">
                        <div class="modal-header">
                            <h4 class="modal-title" id="modalTitle">Monto de adeudo</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <div class="col-md-12">
                                <div class="form-group mb-0">
                                    <label for="monto" class="control-label">Monto <span class="field-required-span">*</span></label>
                                    <input type="text" class="form-control" id="monto" name="monto" onpaste="return false">
                                    <label id="error-monto"></label>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Cerrar</button>
                            <button type="submit" id="btn_guardar_monto" class="btn btn-success waves-effect waves-light">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal" tabindex="-1" id="modal_lista_boletas" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form method="post" id="form_lista_boletas" autocomplete="off">
                        @csrf
                        <input type="hidden" name="alumno_id_3" id="alumno_id_3">
                        <div class="modal-header">
                            <h4 class="modal-title" id="modalTitle">Boletas por alumno</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <div class="col-md-12">
                                <div class="form-group mb-0">
                                    <div class="table-responsive m-t-40">
                                        <table class="table table-bordered dataTable display" cellspacing="0" width="100%" id="table_boletas_alumno">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Matrícula</th>
                                                    <th scope="col">Materia</th>
                                                    <th scope="col">Nombre</th>
                                                    <th scope="col">Fecha</th>
                                                    <th scope="col">Acciones</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Cerrar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script-js')
@routes
<script>
    $(document).ready( function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        /*
        |--------------------------------------------------------------------------
        |   Variables globales para las funciones
        |   Aqui lleno todos los valores para evitar estar cambiando en cada funcion
        |   los valores y asi evitar posibles fallos por olvidar cambiar un valor
        |--------------------------------------------------------------------------
        */

        var view_create = '#create_alumno';
        var view_read = '#view_alumno';
        var view_update = '#edit_alumno';
        var view_delete = '#delete_alumno';
        var datatable_1 = '#table_1';
        var datatable_2 = '#table_2';
        var datatable_3 = '#table_3';
        var datatable_4 = '#table_4';
        var eleccion_datatable = null;


        /*
        |--------------------------------------------------------------------------
        |   Funcion para detectar cambios en el radio input
        |--------------------------------------------------------------------------
        */
        $("input[name=tipo_curso]").change(function () {

            // Destruir el datatable inicial
            $(datatable_1).dataTable().fnDestroy();
            $(datatable_2).dataTable().fnDestroy();
            $(datatable_3).dataTable().fnDestroy();
            $(datatable_4).dataTable().fnDestroy();


            if($(this).val() == 1){
                $('.curso-1').show();
                $('.curso-2').hide();
                $('.curso-3').hide();
                $('.curso-4').hide();

                eleccion_datatable = datatable_1;
                loadDatatable(datatable_1, 1);

            }else if($(this).val() == 2){
                $('.curso-1').hide();
                $('.curso-2').show();
                $('.curso-3').hide();
                $('.curso-4').hide();

                eleccion_datatable = datatable_2;
                loadDatatable(datatable_2, 2);

            }else if($(this).val() == 3){
                $('.curso-1').hide()
                $('.curso-2').hide();
                $('.curso-3').show();
                $('.curso-4').hide();

                eleccion_datatable = datatable_3;
                loadDatatable(datatable_3, 3);

            }else if($(this).val() == 4){
                $('.curso-1').hide()
                $('.curso-2').hide();
                $('.curso-3').hide();
                $('.curso-4').show();

                eleccion_datatable = datatable_4;
                loadDatatable(datatable_4, 4);
            }
		});


        /*
        |--------------------------------------------------------------------------
        | Tabla donde se muestran alumnos dependiendo de su nivel educativo
        |--------------------------------------------------------------------------
        */
        var collapsedGroups = {};
        function loadDatatable(table_id, curso_id){
            var table = $(table_id).DataTable({
                processing: false,
                serverSide: true,
                pageLength: 50,
                ajax: {
                    url: "{{route('boleta.index')}}",
                    data: {
                        curso_id: curso_id,
                    },
                    type: 'GET',
                    dataSrc: 'data',
                    cache: true
                },
                language: idiomaDataTable,
                columns: [
                    // {data: 'id'},
                    {data: 'matricula'},
                    {
                        data: 'plantel.nombre',
                        render: function ( data, type, row ) {
                            return `<span class="badge bg-info font-weight-bold p-1 text-white">${data}</span>`;
                        }
                    },
                    {
                        data: 'grupo',
                        orderable: false,
                        render: function ( data, type, row ) {
                            return data.grado + '-' + data.letra;
                        }
                    },
                    {data: 'nombre_completo'},
                    {data: 'action', orderable: false},
                ],
                order: [[0, 'asc']],
                rowGroup: {
                    // Uses the 'row group' plugin
                    dataSrc: 'grupo',
                    startRender: function (rows, group) {
                        // Asi se hace si queremos que este expandido
                        var collapsed = !!collapsedGroups[group];

                        // Se hace lo siguiente ya que queremos que este collapsado
                        // var expanded = !!collapsedGroups[group];

                        rows.nodes().each(function (r) {
                            // Asi se hace si queremos que este expandido
                            r.style.display = collapsed ? 'none' : '';

                            // Se hace lo siguiente ya que queremos que este collapsado
                            // r.style.display = 'none';
                            // if (expanded) {
                            //     r.style.display = '';
                            // }
                        });

                        // Asi se hace si queremos que este expandido
                        return $('<tr/>')
                            .append('<td colspan="5">' + group.grado + '-' + group.letra + ' | Total: ' + rows.count() + '</td>')
                            .attr('data-name', group)
                            .toggleClass('collapsed', collapsed);

                        // Se hace lo siguiente ya que queremos que este collapsado
                        // return $('<tr/>')
                        //     .append('<td colspan="6">' + group.nombre + ' (' + rows.count() + ')</td>')
                        //     .attr('data-name', group)
                        //     .toggleClass('collapsed', expanded);
                    }
                }
            });
        }


        /*
        |--------------------------------------------------------------------------
        | Cuando se da click en ver boleta se ejecuta lo siguiente
        |--------------------------------------------------------------------------
        */
        $('body').on('click', '#ver_boleta', function () {
            $('#table_boletas_alumno').dataTable().fnDestroy();
            let alumno_id = $(this).attr('data-id');
            let route = '{{ route('boleta.show', [':id']) }}';
            var url = route.replace(":id", alumno_id);

            cargarDatatable(url);

            $('#modal_lista_boletas').modal('show');
        });


        /*
        |--------------------------------------------------------------------------
        | Funcion para cargar un datatable
        |--------------------------------------------------------------------------
        */
        function cargarDatatable(url){
            $('#table_boletas_alumno').DataTable({
                processing: false,
                serverSide: true,
                ajax: {
                    url: url,
                    type: 'GET',
                },
                language: idiomaDataTable,
                columns: [
                    {data: 'alumno.matricula'},
                    {
                        data: 'materia',
                        render: function ( data, type, row ) {
                            if(data == 1){
                                return 'Español';
                            }else if(data == 2){
                                return 'Inglés';
                            }else if(data == 3){
                                return 'Francés';
                            }
                        }
                    },
                    {data: 'nombre'},
                    {data: 'fecha_subida'},
                    {
                        data: 'id',
                        render: function ( data, type, row ) {
                            return `<button type="button" data-toggle="tooltip" title="Botón de acción para descargar boleta del alumno" data-id="${data}" class="btn btn-success descargar_boleta"><i class="fas fa-file-download"></i></button> <button type="button" data-toggle="tooltip" title="Botón de acción para eliminar boleta del alumno" data-id="${data}" class="btn btn-danger eliminar_boleta"><i class="fas fa-trash"></i></button>`;
                        }
                    }
                ],
                order: [[3, 'asc']]
            });
        }


        /*
        |--------------------------------------------------------------------------
        | Cuando se da click en agregar boleta se ejecuta lo siguiente
        |--------------------------------------------------------------------------
        */
        $('body').on('click', '#agregar_boleta', function () {
            let alumno_id = $(this).attr('data-id');
            $('#alumno_id_2').val(alumno_id);

            $('#modal_upload_boleta').modal('show');
        });


        /*
        |--------------------------------------------------------------------------
        | Cuando se da click en guardar modal boletas
        |--------------------------------------------------------------------------
        */
        $('body').on('click', '#btn_guardar_boleta', function (e) {
            e.preventDefault();

            let alumno_id = $('#alumno_id_2').val();
            let materia_id = $('#materia_id').val();
            let archivo = $('#archivo').val();


            if($.trim(alumno_id) != '' && $.trim(materia_id) != '' && $.trim(archivo) != ''){
                var form = $('#form_upload_boleta')[0];
                var formData = new FormData(form);
                $.ajax({
                    url: "{{ route('boleta.store') }}",
                    method: "POST",
                    data: formData,
                    dataType:'JSON',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success:function(response){
                        if(response.code == 200){
                            $('#alumno_id_2').val('');
                            $('#materia_id').val('');

                            // Reiniciar dropify y cargar la nueva url
                            var drEvent = $('#archivo').dropify();
                            drEvent = drEvent.data('dropify');
                            drEvent.resetPreview();
                            drEvent.clearElement();
                            drEvent.destroy();
                            drEvent.init();

                            Swal.fire('Boleta guardada', '', 'success');

                            $('#modal_upload_boleta').modal('hide');
                        }else if(response.code == 500){
                            Swal.fire('¡Ocurrio un error!', 'Ha ocurrido un error.', 'error');
                        }
                    }
                });
            }else{
                Swal.fire('¡Ocurrio un error!', 'Por favor selecciona una materia o selecciona un archivo.', 'error');
            }
        });


        /*
        |--------------------------------------------------------------------------
        | Cuando se da click en activar se ejecuta lo siguiente
        |--------------------------------------------------------------------------
        */
        $('body').on('click', '#activar_alumno', function (e) {
            e.preventDefault();

            let alumno_id = $(this).attr('data-id');
            let data = { alumno_id: alumno_id};
            let url = route("alumno.activar", data);

            $.ajax({
                url: url,
                method:"POST",
                dataType:'JSON',
                contentType: false,
                cache: false,
                processData: false,
                success:function(response){
                    if(response.status == true){
                        var oTable = $(eleccion_datatable).dataTable();
                        oTable.fnDraw(false);

                        Swal.fire('Éxito', 'El alumno ha sido activado nuevamente.', 'success');
                    }else if(response.status == false){
                        Swal.fire('¡Ocurrio un error!', 'Ha ocurrido un error.', 'error');
                    }
                }
            });
        });


        /*
        |--------------------------------------------------------------------------
        | Cuando se da click en desactivar se ejecuta lo siguiente
        |--------------------------------------------------------------------------
        */
        $('body').on('click', '#desactivar_alumno', function () {
            let alumno_id = $(this).attr('data-id');
            $('#alumno_id').val(alumno_id);

            $('#modal_monto').modal('show');
        });


        /*
        |--------------------------------------------------------------------------
        |  Establece la mascara de formato de moneda en el
        |  input de monto
        |--------------------------------------------------------------------------
        */
        $("#monto").maskMoney({thousands:',', decimal:'.', allowZero:true, prefix: '$ ', affixesStay: true});


        /*
        |--------------------------------------------------------------------------
        | Cuando se da click en editar se ejecuta lo siguiente
        |--------------------------------------------------------------------------
        */
        $('body').on('click', '#btn_guardar_monto', function (e) {
            e.preventDefault();

            let alumno_id = $('#alumno_id').val();
            let monto = $('#monto').maskMoney('unmasked')[0];


            if($.trim(alumno_id) != '' && $.trim(monto) != ''){
                let data = { alumno_id: alumno_id, monto: monto };
                let url = route("alumno.desactivar", data);

                $.ajax({
                    url: url,
                    method:"POST",
                    dataType:'JSON',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success:function(response){
                        if(response.status == true){
                            $('#alumno_id').val('');
                            $('#monto').val('');
                            $('#modal_monto').modal('hide');
                            var oTable = $(eleccion_datatable).dataTable();
                            oTable.fnDraw(false);

                            Swal.fire('Éxito', 'El alumno ha sido desactivado por falta de pago.', 'success');
                        }else if(response.status == false){
                            Swal.fire('¡Ocurrio un error!', 'Ha ocurrido un error.', 'error');
                        }
                    }
                });
            }else{
                Swal.fire('¡Ocurrio un error!', 'Por favor ingresa un monto.', 'error');
            }
        });


        /*
        |--------------------------------------------------------------------------
        | Cuando se da click en descargar boleta se ejecuta lo siguiente
        |--------------------------------------------------------------------------
        */
        $('body').on('click', '.descargar_boleta', function () {
            let id = $(this).attr('data-id');
            let data = {id: id};
            let url = route('boleta.download', data);

            window.location = url;
        });


        /*
        |--------------------------------------------------------------------------
        | Cuando se da click en eliminar boleta se ejecuta lo siguiente
        |--------------------------------------------------------------------------
        */
        $('body').on('click', '.eliminar_boleta', function (e) {
            e.preventDefault();

            let id = $(this).attr('data-id');
            let route = '{{ route("boleta.destroy", ":id") }}';
            let url = route.replace(":id", id);

            Swal.fire({
                title: '¿Estás seguro?',
                text: "Un registro eliminado no se puede recuperar.",
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                confirmButtonColor: '#dc3f4e',
                confirmButtonText: 'Si, eliminar',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    axios.post(url, {'_method':'delete'}).then(respuesta => {
                        if (respuesta.data.code == 200) {
                            Swal.fire('Registro eliminado', '', 'success');
                            var oTable = $('#table_boletas_alumno').dataTable();
                            oTable.fnDraw(false);
                        }else if(respuesta.data.code == 500){
                            Swal.fire('¡Ocurrio un error!', 'No se pudo eliminar el registro', 'error');
                        }
                    }).catch(error => {
                        Swal.fire('¡Ocurrio un error!', 'No se pudo eliminar el registro', 'error');

                    });
                }else if(result.dismiss = 'cancel'){

                }
		    });
        });


        /*
        |--------------------------------------------------------------------------
        | Cuando se da click en subir masivamente
        |--------------------------------------------------------------------------
        */
        $('body').on('click', '.subir-masivamente', function (e) {
            window.location = "{{route('boleta.index_masivamente')}}";
        });
    });
</script>
@endsection

