$(document).ready(function () {
    "use strict";
    $(function () {
        $(".preloader").fadeOut();
    });
    jQuery(document).on('click', '.mega-dropdown', function (e) {
        e.stopPropagation()
    });
    // ==============================================================
    // This is for the top header part and sidebar part
    // ==============================================================
    var set = function () {
        var width = (window.innerWidth > 0) ? window.innerWidth : this.screen.width;
        var topOffset = 55;
        if (width < 1170) {
            $("body").addClass("mini-sidebar");
            $('.navbar-brand span').hide();
            $(".sidebartoggler i").addClass("ti-menu");
        }
        else {
            $("body").removeClass("mini-sidebar");
            $('.navbar-brand span').show();
        }
         var height = ((window.innerHeight > 0) ? window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $(".page-wrapper").css("min-height", (height) + "px");
        }
    };
    $(window).ready(set);
    $(window).on("resize", set);
    // ==============================================================
    // Theme options
    // ==============================================================
    $(".sidebartoggler").on('click', function () {
        if ($("body").hasClass("mini-sidebar")) {
            $("body").trigger("resize");
            $("body").removeClass("mini-sidebar");
            $('.navbar-brand span').show();
        }
        else {
            $("body").trigger("resize");
            $("body").addClass("mini-sidebar");
            $('.navbar-brand span').hide();
        }
    });
    // this is for close icon when navigation open in mobile view
    $(".nav-toggler").click(function () {
        $("body").toggleClass("show-sidebar");
        $(".nav-toggler i").toggleClass("ti-menu");
        $(".nav-toggler i").addClass("ti-close");
    });
    $(".search-box a, .search-box .app-search .srh-btn").on('click', function () {
        $(".app-search").toggle(200);
    });
    // ==============================================================
    // Right sidebar options
    // ==============================================================
    $(".right-side-toggle").click(function () {
        $(".right-sidebar").slideDown(50);
        $(".right-sidebar").toggleClass("shw-rside");
    });
    // ==============================================================
    // This is for the floating labels
    // ==============================================================
    $('.floating-labels .form-control').on('focus blur', function (e) {
        $(this).parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
    }).trigger('blur');
 
    // ==============================================================
    //tooltip
    // ==============================================================
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

    // ==============================================================
    //Popover
    // ==============================================================
    $(function () {
        $('[data-toggle="popover"]').popover()
    })

    // ==============================================================
    // Perfact scrollbar
    // ==============================================================
    // $('.scroll-sidebar, .right-side-panel, .message-center, .right-sidebar').perfectScrollbar();
	// $('#chat, #msg, #comment, #todo').perfectScrollbar(); 
    // ==============================================================
    // Resize all elements
    // ==============================================================
    $("body").trigger("resize");
    // ==============================================================
    // Collapsable cards
    // ==============================================================
    $('a[data-action="collapse"]').on('click', function (e) {
        e.preventDefault();
        $(this).closest('.card').find('[data-action="collapse"] i').toggleClass('fas fa-minus fas fa-plus');
        $(this).closest('.card').children('.card-body').collapse('toggle');
    });
    // Toggle fullscreen
    $('a[data-action="expand"]').on('click', function (e) {
        e.preventDefault();
        $(this).closest('.card').find('[data-action="expand"] i').toggleClass('mdi-arrow-expand mdi-arrow-compress');
        $(this).closest('.card').toggleClass('card-fullscreen');
    });
    // Close Card
    $('a[data-action="close"]').on('click', function () {
        $(this).closest('.card').removeClass().slideUp('fast');
    });

    // For Custom File Input
    $('.custom-file-input').on('change',function(){
        //get the file name
        var fileName = $(this).val();
        //replace the "Choose a file" label
        $(this).next('.custom-file-label').html(fileName);
    })


    // ==============================================================
    // MIS OPCIONES CUSTOM
    // ==============================================================
    
    /* 
    |---------------------------------------------------------------
    | Inicializar select2
    |---------------------------------------------------------------
    */
    $(".select2").select2();

    /* 
    |---------------------------------------------------------------
    | Input Mask
    |---------------------------------------------------------------
    */
   $(".phone-inputmask").inputmask("(999) 999-9999");
    
    $(".email-inputmask").inputmask({
       mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[*{2,6}][*{1,2}].*{1,}[.*{2,6}][.*{1,2}]"
       , greedy: !1
       , onBeforePaste: function (n, a) {
           return (e = e.toLowerCase()).replace("mailto:", "")
       }
       , definitions: {
           "*": {
               validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~/-]"
               , cardinality: 1
               , casing: "lower"
           }
       }
    });  

    $(".fecha-mask").inputmask();
   
    /* 
    |---------------------------------------------------------------
    | Dropify (Translated)
    |---------------------------------------------------------------
    */
    $('.dropify').dropify({
        messages: {
            default:'Haz clic aquí para subir un archivo.',
            replace:'Haz clic aquí para remplazar.',
            remove:'Remover archivo.',
            error: 'Ups, algo salió mal.'
        },
        error: {
            fileSize: 'El tamaño del archivo es demasiado grande ({{ value }} max).',
            minWidth: 'El ancho de la imagen es demasiado pequeño ({{ value }}px min).',
            maxWidth: 'El ancho de la imagen es demasiado grande ({{ value }}px max).',
            minHeight: 'La altura de la imagen es demasiado pequeña ({{ value }}px min).',
            maxHeight: 'La altura de la imagen es demasiado grande ({{ value }}px max).',
            imageFormat: 'El formato de imagen no está permitido, solo formatos ({{ value }}).',
            fileExtension: 'El archivo no está permitido, solo formatos ({{ value }}).'
        }
    });


    /* 
    |---------------------------------------------------------------
    | Moment Js locale
    |---------------------------------------------------------------
    */
    moment.updateLocale('es', {
        months: 'Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre'.split('_'),
        monthsShort: 'Ene_Feb_Mar_Abr_May_Jun_Jul_Ago_Sep_Oct_Nov_Dic'.split('_'),
        weekdays: 'Domingo_Lunes_Martes_Miercoles_Jueves_Viernes_Sabado'.split('_'),
        weekdaysShort: 'Dom._Lun._Mar._Mier._Jue._Vier._Sab.'.split('_'),
        weekdaysMin: 'Do_Lu_Ma_Mi_Ju_Vi_Sa'.split('_')
        }
    );

    /* 
    |---------------------------------------------------------------
    | Inicializa datapicker
    |---------------------------------------------------------------
    */
    $('.date-picker').datepicker({
        changeMonth: true,
        changeYear: true,
        // yearRange: "c-70:c+0", // last hundred years and current years
        yearRange: "c-100:c+100", // last hundred years and future hundred years
    });

    $.timepicker.regional['es'] = {
		timeOnlyTitle: 'Elegir una hora',
		timeText: 'Hora',
		hourText: 'Horas',
		minuteText: 'Minutos',
		secondText: 'Segundos',
		millisecText: 'Milisegundos',
		microsecText: 'Microsegundos',
		timezoneText: 'Uso horario',
		currentText: 'Hoy',
		closeText: 'Cerrar',
		timeFormat: 'hh:mm tt',
		timeSuffix: '',
		amNames: ['a.m.', 'AM', 'A'],
		pmNames: ['p.m.', 'PM', 'P'],
		isRTL: false
	};
    $.timepicker.setDefaults($.timepicker.regional['es']);
    
    $('.date-picker-time').datetimepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "c-100:c+100", // last hundred years and future hundred years
        showButtonPanel: false,
        controlType: 'select',
        oneLine: true,
        timeFormat: 'hh:mm tt'
    });

    
    /* 
    |---------------------------------------------------------------
    | Funcion para que quite los espacios y ponga en mayusculas
    |---------------------------------------------------------------
    */
    $('.sinEspaciosMayusculas').keyup(function(){
        /* Obtengo el valor contenido dentro del input */
        var value = $(this).val();
    
        /* Convierto a mayusculas el valor y elimino todos los espacios en blanco que tenga la cadena delante y detrás */
        var new_value = value.toUpperCase().trim();
    
        /* Cambio el valor contenido por el valor sin espacios */
        $(this).val(new_value);
    });
   
});

//=======================================================================================
// FUERA DEL SCOPE DE JQUERY
//=======================================================================================

/* 
|---------------------------------------------------------------
| Variable para traduccion de las datatables
|---------------------------------------------------------------
*/
var idiomaDataTable = 
    {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        },
        "buttons": {
            "copy": "Copiar",
            "colvis": "Visibilidad"
        }
    }


/* 
|---------------------------------------------------------------
| Funcion para delimitar que meses quiero que se bloqueen
| en datapicker
|---------------------------------------------------------------
*/
var monthsToDisable = [2, 3, 4, 5 , 7, 9, 10, 11];

function disableSpecificWeekDays(date) {
    var month = date.getMonth();
    if ($.inArray(month, monthsToDisable) != -1) {
        return [false];
    }
    return [true];
}



/* 
|---------------------------------------------------------------
| Funcion para validar que solo sea ingresada hasta la longitud deseada
|---------------------------------------------------------------
*/
function validarLongitud(e, longitud) {
    if (e.value.length > (longitud- 1)){
        return false;
    }else{
        return true;  
    }
}


/* 
|---------------------------------------------------------------
| Funcion para convertir en mayusculas y evitar caracteres especiales
|---------------------------------------------------------------
*/
function mayusculasEspecial(e) {
    // Expresión regular para eliminar los caractesres especiales y espacios
    e.value = e.value.replace(/[^a-zA-Z0-9]/g, '').toUpperCase();
}



/* 
|---------------------------------------------------------------
| Funcion para que solo se puedan ingresar numeros
|---------------------------------------------------------------
*/
function soloNumeros(e) {
    var key = e.keyCode || e.which;
    var tecla = String.fromCharCode(key);
    var letras = " 1234567890";
    var especiales = "8-37-39-46";
  
    var tecla_especial = false
    for(var i in especiales){
      if(key == especiales[i]){
        tecla_especial = true;
        break;
      }
    }
  
    if(letras.indexOf(tecla) == -1 && !tecla_especial){
      return false;
    }
}


/* 
|---------------------------------------------------------------
| Funcion para que acepte numeros decimales
|---------------------------------------------------------------
*/
function soloNumerosAndDecimales(e) {
    var key = e.keyCode || e.which;
    var tecla = String.fromCharCode(key);
    var letras = " 1234567890.";
    var especiales = "8-37-39-46";
  
    var tecla_especial = false
    for(var i in especiales){
      if(key == especiales[i]){
        tecla_especial = true;
        break;
      }
    }
  
    if(letras.indexOf(tecla) == -1 && !tecla_especial){
      return false;
    }
}


/* 
|---------------------------------------------------------------
| Funcion para actualizar la visibilidad de perfect scrollbar
|---------------------------------------------------------------
*/
const container = document.querySelector('.scroll-sidebar');
const psu = new PerfectScrollbar(container);

function ps(){
    psu.update();
}


/* 
|---------------------------------------------------------------
| Funcion para limpiar los campos dada una clase css
|---------------------------------------------------------------
*/
function clear_form_elements(class_name) {
    $("." + class_name).find(':input').each(function() {
        switch(this.type) {
            case 'password':
            case 'text':
            case 'textarea':
            case 'file':
            case 'select-one':
            case 'select-multiple':
            case 'date':
            case 'number':
            case 'tel':
            case 'email':
                $(this).val('');
                break;
            case 'checkbox':
            case 'radio':
                this.checked = false;
                break;
        }
    });
}


/* 
|---------------------------------------------------------------
| Funcion para limpiar los campos file(dropify)
|---------------------------------------------------------------
*/
function clear_dropify(class_name) {
    $("." + class_name).find('input:file').each(function() {
        var drEvent = $('#' + this.id).dropify();
        drEvent = drEvent.data('dropify');
        drEvent.resetPreview();
        drEvent.clearElement();
        drEvent.destroy();
        drEvent.init();
    });
}


/* 
|---------------------------------------------------------------
| Funcion para evaluar si es empty
|---------------------------------------------------------------
*/
function isEmpty(value){
    return (value == null || value.length === 0);
}








