const mix = require('laravel-mix');

/*
|--------------------------------------------------------------------------
| Mix Asset Management
|--------------------------------------------------------------------------
|
| Mix provides a clean, fluent API for defining some Webpack build steps
| for your Laravel application. By default, we are compiling the Sass
| file for the application as well as bundling up all the JS files.
|
|
|
| INSTRUCCIONES
|
| Ejecutar el comando sin los asteriscos *npm run dev* para compilar 
| los archivos para desarrollo.
| 
|
*/


// Con esto combinamos todos los archivos en uno solo, asi evitamos que se hagan demasiadas peticiones
mix.styles([
   'resources/assets/css/style.css',
   'resources/assets/css/dataTables.bootstrap4.css',
   'resources/assets/css/responsive.dataTables.min.css',
   'resources/assets/css/dashboard1.css',
   'resources/assets/css/jquery-ui.css',
   'resources/assets/css/jquery-ui-timepicker-addon.min.css',
   'resources/assets/css/select2.min.css',
   'resources/assets/css/dropify.css',
   'resources/assets/css/icheck-material.css',
   'resources/assets/css/sweetalert-2.css',
   'resources/assets/css/perfect-scrollbar.css',
   'resources/assets/css/custom.css',
], 'public/css/all.css');


// Con esto copiamos los resources que necesitamos a la carpeta public
mix.copyDirectory('resources/assets/css/icons', 'public/css/icons');
mix.copyDirectory('resources/assets/css/fonts', 'public/css/fonts');
mix.copyDirectory('resources/assets/css/images', 'public/css/images');
mix.copyDirectory('resources/assets/images', 'public/images');

mix.copy('resources/assets/css/bootstrap_invoice.css', 'public/css/bootstrap_invoice.css');

// Con esto combinamos todos los archivos en uno solo asi evitamos que se hagan demasiadas peticiones
mix.combine([
   'resources/assets/js/axios.min.js',
   'resources/assets/js/jquery-3.2.1.min.js',
   'resources/assets/js/jquery-ui-custom.js',
   'resources/assets/js/jquery-ui-timepicker-addon.min.js',
   'resources/assets/js/popper.min.js',
   'resources/assets/js/bootstrap.js',
   'resources/assets/js/bootstrap-show-password.min.js',
   'resources/assets/js/perfect-scrollbar.js',
   'resources/assets/js/sidebarmenu.js',
   'resources/assets/js/waves.js',
   'resources/assets/js/jquery.dataTables.js',
   'resources/assets/js/dataTables.bootstrap4.js',
   'resources/assets/js/dataTables.responsive.min.js',
   'resources/assets/js/dataTables.rowGroup.js',
   'resources/assets/js/jquery.tabledit.js',
   'resources/assets/js/inputmask/dist/min/jquery.inputmask.bundle.min.js',
   'resources/assets/js/select2.full.js',
   'resources/assets/js/dropify.js',
   'resources/assets/js/jquery.validate.min.js',
   'resources/assets/js/jquery.maskMoney.min.js',
   'resources/assets/js/sweetalert-2.js',
   'resources/assets/js/moment-with-locales.min.js',
   'resources/assets/js/js-curp.js',
   'resources/assets/js/loadingoverlay.min.js',
   'resources/assets/js/mindmup-editabletable.js',
   'resources/assets/js/typeahead.bundle.js',
   'resources/assets/js/custom.js',
 ], 'public/js/all.js');

