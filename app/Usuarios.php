<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;

class Usuarios extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use SoftDeletes;
    
    public $timestamps = true;
    protected $table = 'usuarios';
    protected $hidden = ['password'];
    protected $fillable = [
        'nombre', 
        'apellido_paterno', 
        'apellido_materno', 
        'full_name', 
        'sexo_id', 
        'email', 
        'usuario', 
        'password', 
        'role_id'];





}

