<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('layouts.app', function ($view) {
            $data = DB::table('config')->where('id', 1)->first();
            
            $view->with('data', $data);
        });

        View::composer('layouts.app-alumnos', function ($view) {
            $data = DB::table('config')->where('id', 1)->first();
            
            $view->with('data', $data);
        });
    }
}
