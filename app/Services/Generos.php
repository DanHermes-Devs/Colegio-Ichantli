<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;

class Generos{

    public function get(){
        $generos = DB::table('cat_generos')->get(['id', 'nombre']);

        return $generos;
    }
}