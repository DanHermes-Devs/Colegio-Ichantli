<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;

class Municipios{

    public function get($estado_id){
        $municipios = DB::table('cat_municipios')->where('estado_id', $estado_id)->get(['id', 'nombre']);

        return $municipios;
    }
}