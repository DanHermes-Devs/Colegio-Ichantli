<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;

class Permisos{

    public function get(){
        $count = DB::table('permissions')->where('users', 1)->count();
        $skip = 1;
        $limit = $count - $skip;
        $permisos = DB::table('permissions')->where('users', 1)->skip(1)->take($limit)->select('id', 'long_name')->get();

        return $permisos;
    }
}