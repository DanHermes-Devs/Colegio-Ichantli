<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;

class Roles{

    public function get(){
        
        $count = DB::table('roles')->count();
        $skip = 1;
        $limit = $count - $skip;
        $roles = DB::table('roles')->skip(1)->take($limit)->get(['id', 'name']);
        
        return $roles;
    }
}