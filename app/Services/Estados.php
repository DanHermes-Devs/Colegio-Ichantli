<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;

class Estados{

    public function get(){
        $estados = DB::table('cat_estados')->get(['id', 'nombre']);

        return $estados;
    }
}