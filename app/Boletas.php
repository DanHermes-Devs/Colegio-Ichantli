<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Boletas extends Model
{
    use SoftDeletes;
    
    public $timestamps = true;
    protected $guarded = [];
    protected $table = 'boletas';
    protected $hidden = ['deleted_at', 'created_at','updated_at'];
    protected $dates = ['fecha_subida'];
    protected $fillable = [
        'alumno_id',
        'nivel_educativo_id',
        'grupo_id',
        'materia',
        'nombre',
        'descripcion',
        'archivo',
        'fecha_subida'
    ];


    public function getFechaSubidaAttribute($value){
        $date = Carbon::parse($value);
        return $date->format('d-m-Y');
    }

    
    // =============================================================================
    // RELACIONES
    // =============================================================================
    public function alumno(){
        return $this->belongsTo(Alumnos::class);
    }

    public function curso(){
        return $this->hasOne(CatNivelEducativo::class, 'id', 'nivel_educativo_id');
    }

    public function grupo(){
        return $this->hasOne(CatGrupos::class, 'id', 'grupo_id');
    }

}
