<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Multimedias extends Model
{
    use SoftDeletes;
    protected $table = 'multimedia';
    protected $fillable = [
        'id_alumno',
        'id_tipoArchivo',
        'nombre'
    ];

    public function alumno()
    {
        return $this->belongsTo(Alumnos::class, 'id_alumno', 'id');
    }

    public function archivo()
    {
        return $this->belongsTo(CatTipoArchivos::class, 'id_tipoArchivo', 'id');
    }
}
