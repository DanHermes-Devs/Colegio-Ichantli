<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Alumnos extends Authenticatable
{
    use SoftDeletes;

    public $timestamps = true;
    protected $table = 'alumnos';
    protected $hidden = ['password', 'deleted_at', 'created_at','updated_at'];
    protected $dates = ['fecha_nacimiento'];
    protected $fillable = [
        'plantel_id',
        'nivel_educativo_id',
        'sexo_id',
        'grupo_id',
        'matricula',
        'nombre',
        'apellido_paterno',
        'apellido_materno',
        'nombre_completo',
        'fecha_nacimiento',
        'curp',
        'email',
        'telefono',
        'domicilio',
        'foto',
        'nombre_completo_tutor',
        'email_tutor',
        'telefono_tutor',
        'observaciones',
        'password',
        'estatus',
        'monto'
    ];

    public function getNombreCompletoAttribute($value){
        return Str::title($value);
    }

    public function getMontoAttribute($value){
        return number_format($value, 2);
    }


    // =============================================================================
    // RELACIONES
    // =============================================================================
    public function genero(){
        return $this->belongsTo(CatGeneros::class, 'sexo_id', 'id');
    }

    public function curso(){
        return $this->belongsTo(CatNivelEducativo::class, 'nivel_educativo_id', 'id');
    }

    public function grupo(){
        return $this->belongsTo(CatGrupos::class);
    }

    public function boletas(){
        return $this->hasMany(Boletas::class, 'alumno_id', 'id');
    }

    public function plantel(){
        return $this->belongsTo(CatPlanteles::class);
    }

    public function multimedias()
    {
        return $this->hasMany(Multimedias::class, 'id_alumno', 'id');
    }
}
