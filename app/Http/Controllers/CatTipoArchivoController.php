<?php

namespace App\Http\Controllers;

use App\CatTipoArchivos;
use Illuminate\Http\Request;

class CatTipoArchivoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CatTipoArchivos  $catTipoArchivo
     * @return \Illuminate\Http\Response
     */
    public function show(CatTipoArchivos $catTipoArchivo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CatTipoArchivos  $catTipoArchivo
     * @return \Illuminate\Http\Response
     */
    public function edit(CatTipoArchivos $catTipoArchivo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CatTipoArchivos  $catTipoArchivo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CatTipoArchivos $catTipoArchivo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CatTipoArchivos  $catTipoArchivo
     * @return \Illuminate\Http\Response
     */
    public function destroy(CatTipoArchivos $catTipoArchivo)
    {
        //
    }
}
