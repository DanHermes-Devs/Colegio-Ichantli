<?php

namespace App\Http\Controllers;

use App\Roles;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Validator;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $user = Auth::user();

        // Total de registros
        $count_roles = Roles::all()->count();
        
        if(request()->ajax()) {
            if($user->id == 1){
                $roles = Roles::get(['id', 'name']); 
            }else{
                $roles = Roles::offset(1)->limit($count_roles)->get(['id', 'name']);
            }

            return datatables()
            ->of($roles)
            ->addColumn('action', 'gestion_usuarios.roles.actions')
            ->rawColumns(['action'])
            ->toJson();
        }

        return view('gestion_usuarios.roles.index');       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rol = Roles::findOrFail($id, ['id', 'name']);

        $permisos = DB::table('role_has_permissions')
        ->join('permissions', 'permissions.id', '=', 'role_has_permissions.permission_id')
        ->select('permissions.id', 'permissions.name')
        ->where('role_has_permissions.role_id', '=', $id)
        ->get();

        // Se crea un array de respuesta con el status, codigo y la respuesta
        $response = array(
            'rol' => $rol,
            'permisos' => $permisos
        );
        
        return response()->json($response);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id){   
        $rol = Roles::findOrFail($id, ['id', 'name']);

        $permisos = DB::table('role_has_permissions')
        ->join('permissions', 'permissions.id', '=', 'role_has_permissions.permission_id')
        ->select('permissions.id', 'permissions.name')
        ->where('role_has_permissions.role_id', '=', $id)
        ->get();

        // Se crea un array de respuesta con el status, codigo y la respuesta
        $response = array(
            'rol' => $rol,
            'permisos' => $permisos
        );
        
        return response()->json($response);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        //app('debugbar')->info($permisos);

        // Se eliminan los permisos para poder actualizar los nuevos permisos que se envien a traves de request
        $permisos_eliminar = DB::table('role_has_permissions')->where('role_id', '=', $request->rol_id);
        $permisos_eliminar->delete();

        // Se crea un nuevo rol
        $rol = Role::updateOrCreate(['id' => $request->rol_id], ['name' => $request->nombre_rol]);    
        
        // Se obtienen los permisos del request
        $permisos = $request->input('permisos');

        // Se asigna siempre por default el permiso de inicio
        $rol->givePermissionTo('inicio');

        //Se asignan los permisos al rol
        foreach($permisos as $permiso){            
            $permiso_bd = Permission::find($permiso);
            $rol->givePermissionTo($permiso_bd);
        }
               
        return response()->json(['status'=> true]);
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        // Se eliminan los permisos para poder actualizar los nuevos permisos que se envien a traves de request
        $permisos_eliminar = DB::table('role_has_permissions')->where('role_id', '=', $id);
        $permisos_eliminar->delete();
        
        $rol = Roles::findOrFail($id);
        $rol->delete();
    }
}
