<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->middleware('guest:web')->except('logout');
        $this->middleware('guest:alumnos')->except('logout');
    }

    public function index()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);  

        $request->session()->regenerate();

        if (Auth::guard('web')->attempt(['usuario' => $request->usuario, 'password' => $request->password])) {
            return redirect()->route('home');
        }
        
        if (Auth::guard('alumnos')->attempt(['matricula' => $request->usuario, 'password' => $request->password])) {
            return redirect()->route('alumno.home');
        }

        return back()->withErrors(['usuario' => 'El usuario o contraseña son incorrectos'])->withInput(request(['usuario']));
    }

    protected function validateLogin(Request $request){
        $this->validate($request,        
        $rules = [
            'usuario' => 'required',
            'password' => 'required'
        ],
        $messages = [
            'usuario.required' => 'El usuario es requerido',
            'password.required' => 'La contraseña es requerida'
        ]);

    }

    public function logout(Request $request) {
        //dd(Auth::guard());
        //Auth::logout();
        $this->guard()->logout();

        $request->session()->invalidate();
        return redirect('/');
    }
}
