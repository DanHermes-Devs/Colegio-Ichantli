<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ShowMunicipios extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        $estado_id = trim(\request('estado_id'));
        $results = DB::table('cat_municipios')->select('id', 'nombre')->where('estado_id','=', $estado_id)->get();
        // Ciudad::where('nombre', 'LIKE', '%' . $q . '%')->take(15)->get();

        return $results->toJson();
    }
}
