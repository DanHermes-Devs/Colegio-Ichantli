<?php

namespace App\Http\Controllers;

use App\Alumnos;
use App\Boletas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class HomeAlumnoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $alumno = Auth::user();

        $data = DB::table('boletas')->where('alumno_id', $alumno->id)->where('deleted_at', null)->select('*', DB::raw("DATE_FORMAT(fecha_subida, '%d-%m-%Y') as fecha_subida"))->get();

        if(request()->ajax()) {
            return datatables()
            ->of($data)
            ->addColumn('action', 'home.alumno.actions')
            ->rawColumns(['action'])
            ->toJson();
        }
        return view('home.alumno.inicio', compact('alumno'));
    }

    
    /**
    * Funcion para listar los circulares
    */
    public function circulares(){
        $data = DB::table('circulares')->select('*', DB::raw("DATE_FORMAT(fecha_subida, '%d-%m-%Y') as fecha_subida"))->get();

        if(request()->ajax()){
            return datatables()
            ->of($data)
            ->toJson();
        }
    }


    /**
    * Funcion para descargar un archivo
    */
    public function downloadBoleta($id){
        $data = DB::table('boletas')->where('id', $id)->first();

        $ruta = public_path('store/alumnos/boletas/').$data->archivo;
        $name = $data->archivo;
        $headers = ['Content-Type' => 'application/pdf'];

        return response()->download($ruta, $name, $headers);
    }


    /**
    * Funcion para descargar un archivo
    */
    public function downloadCircular($id){
        $data = DB::table('circulares')->where('id', $id)->first();

        $ruta = public_path('store/alumnos/circulares/').$data->archivo;
        $name = $data->archivo;
        $headers = ['Content-Type' => 'application/pdf'];

        return response()->download($ruta, $name, $headers);
    }
}
