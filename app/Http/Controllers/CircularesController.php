<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Circulares;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CircularesController extends Controller
{
    /**
    * Funcion para listar las circulares
    */
    public function index(){
        $data = Circulares::all();

        if(request()->ajax()) {
            return datatables()
            ->of($data)
            ->addColumn('action', 'circulares.actions')
            ->rawColumns(['action'])
            ->toJson();
        }

        return view('circulares.index');
    }


    /**
    * Funcion para guardar circular
    */
    public function store(Request $request){
        $input = $request->all();

        if($request->file('archivo')){

            $validator = Validator::make($request->all(), [
                'nombre' => 'required',
                'descripcion' => 'required',
                'archivo' => 'required|mimes:pdf|max:3072',
            ]);

            if(!$validator->fails()){
                $ubicacion = public_path('/store/alumnos/circulares');
                if (!file_exists($ubicacion)) {
                    mkdir($ubicacion, 0777, true);
                }

                $date = Carbon::now();
                $fecha_actual = $date->format('Y-m-d');

                if($request->file('archivo')){
                    $archivo = $request->file('archivo');
                    $nombre_archivo = strtr($input['nombre'], ' ', '_').'_'.time() . "." . $request->file('archivo')->extension();
                    $archivo->move($ubicacion, $nombre_archivo);
                }

                $circular = new Circulares;

                $circular->nombre = $input['nombre'];
                $circular->descripcion = $input['descripcion'];
                $circular->archivo = $nombre_archivo;
                $circular->fecha_subida = $fecha_actual;
                $circular->save();

                return response()->json(['code' => 200, 'data' => $circular, 'message' => 'La circular se ha almacanado con éxito.']);
            }else{
                return response()->json(['code' => 500, 'data' => $validator->errors()->all(), 'message' => 'No se pudo guardar la circular por que excede el limite de 3MB.']);
            }
        }else{
            return response()->json(['code' => 500, 'data' => [], 'message' => 'Error no se pudo guardar la circular.']);
        } 
    }


    /**
    * Funcion para descargar un archivo
    */
    public function download($id){
        $data = Circulares::findOrFail($id);

        $ruta = public_path('store/alumnos/circulares/').$data->archivo;
        $name = $data->archivo;
        $headers = ['Content-Type' => 'application/pdf'];

        return response()->download($ruta, $name, $headers);
    }


    /**
    * Funcion para eliminar logicamente una circular
    */
    public function destroy($id){
        $data = Circulares::findOrFail($id);
        $data->delete();
    }
}
