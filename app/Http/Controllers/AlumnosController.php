<?php

namespace App\Http\Controllers;

use App\Alumnos;
use App\CatGrupos;
use App\CatTipoArchivos;
use App\Multimedias;
use Carbon\Carbon;
use App\CatGeneros;
use App\CatPlanteles;
use App\CatNivelEducativo;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Exports\UsersExport;
use Illuminate\Http\Request;
use App\Exports\UserExportFilter;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;

class AlumnosController extends Controller
{

    public function export()
    {
        return Excel::download(new UsersExport, "Alumnos-".date("Y-m-d H:i:s").".xlsx");

    }

    public function exportfilter($id1, $id2)
    {
        $alumnos = Alumnos::where('nivel_educativo_id', '=', $id1)->where('grupo_id', '=', $id2)->get();
        return (new UserExportFilter($alumnos))->download("Alumnos-".date("Y-m-d H:i:s").".xlsx");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Alumnos::with('curso', 'grupo', 'plantel')->get();

        if (request()->ajax()) {
            return datatables()
                ->of($data)
                ->addColumn('action', 'catalogos.alumnos.actions')
                ->rawColumns(['action'])
                ->toJson();
        }

        $niveleducativo = CatNivelEducativo::all();

        return view('catalogos.alumnos.index', compact('niveleducativo'));
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cursos = CatNivelEducativo::all();
        $generos = CatGeneros::all();
        $planteles = CatPlanteles::all();

        $date = Carbon::now()->format('y');

        return view('catalogos.alumnos.create', compact('cursos', 'generos', 'planteles', 'date'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        // Verifico si hay una fotografia
        if ($request->file('fotografia')) {
            $validator = Validator::make($request->all(), [
                'fotografia' => 'required|image|mimes:png,jpg,jpeg|max:3072',
            ]);

            if (!$validator->fails()) {
                $path = public_path('store/alumnos/fotografias');
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }

                // if($alumno->fotografia_infantil){
                //     if (!unlink($path.'/'.$alumno->fotografia_infantil)) {
                //         return response()->json(['code' => 500, 'data' => [], 'message' => 'No se pudo almacenar la nueva imagen.']);
                //     }
                // }

                $input['fotografia'] = time() . '.' . $request->fotografia->extension();
                $request->fotografia->move(public_path('store/alumnos/fotografias'), $input['fotografia']);

                $alumno = new Alumnos;

                $alumno->plantel_id = $input['plantel_id'];
                $alumno->nivel_educativo_id = $input['curso_id'];
                $alumno->sexo_id = $input['sexo_id'];
                $alumno->grupo_id = $input['grupo_id'];
                $alumno->matricula = $input['matricula2'];
                $alumno->nombre = ucwords($input['nombre']);
                $alumno->apellido_paterno = ucwords($input['apellido_paterno']);
                $alumno->apellido_materno = ucwords($input['apellido_materno']);
                $alumno->nombre_completo = Str::lower($input['nombre'] . ' ' . $input['apellido_paterno'] . ' ' . $input['apellido_materno']);
                $alumno->fecha_nacimiento = Carbon::createFromFormat('d/m/Y', $input['fecha_nacimiento'])->format('Y-m-d');
                $alumno->curp = $input['curp'];
                $alumno->domicilio = $input["domicilio"];
                $alumno->nombre_completo_tutor = Str::lower($input['nombre_tutor']);
                $alumno->email_tutor = $input['correo_electronico_tutor'];
                $alumno->telefono_tutor = $input['telefono_celular_tutor'];
                $alumno->observaciones = $input['observaciones'];
                $alumno->password = Hash::make($input['curp']);
                $alumno->activado = 1;

                // Guardar nombre del archivo en la base de datos
                $alumno->foto = $input['fotografia'];
                $alumno->save();

                $data = [
                    'fotografia' => $alumno->fotografia_infantil,
                    'alumno' => $alumno
                ];

                return response()->json(['code' => 200, 'data' => $data, 'message' => 'Fotografía infantil y datos guardados con éxito.']);
            } else {
                return response()->json(['code' => 500, 'data' => $validator->errors()->all(), 'message' => 'No se pudo guardar la fotografía.']);
            }
        } else {

            $alumno = new Alumnos;
            $alumno->plantel_id = $input['plantel_id'];
            $alumno->nivel_educativo_id = $input['curso_id'];
            $alumno->sexo_id = $input['sexo_id'];
            $alumno->grupo_id = $input['grupo_id'];
            $alumno->matricula = $input['matricula2'];
            $alumno->nombre = ucwords($input['nombre']);
            $alumno->apellido_paterno = ucwords($input['apellido_paterno']);
            $alumno->apellido_materno = ucwords($input['apellido_materno']);
            $alumno->nombre_completo = Str::lower($input['nombre'] . ' ' . $input['apellido_paterno'] . ' ' . $input['apellido_materno']);
            $alumno->fecha_nacimiento = Carbon::createFromFormat('d/m/Y', $input['fecha_nacimiento'])->format('Y-m-d');
            $alumno->curp = $input['curp'];
            $alumno->domicilio = $input["domicilio"];
            $alumno->nombre_completo_tutor = Str::lower($input['nombre_tutor']);
            $alumno->email_tutor = $input['correo_electronico_tutor'];
            $alumno->telefono_tutor = $input['telefono_celular_tutor'];
            $alumno->observaciones = $input['observaciones'];
            $alumno->password = Hash::make($input['curp']);
            $alumno->activado = 1;

            $alumno->save();

            return response()->json(['code' => 200, 'data' => [], 'message' => 'Fotografía infantil y datos guardados con éxito.']);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Alumnos::with('curso', 'grupo', 'genero', 'plantel')->find($id);

        return view('catalogos.alumnos.show', compact('data'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cursos    = CatNivelEducativo::all();
        $generos   = CatGeneros::all();
        $grupos    = CatGrupos::all();
        $planteles = CatPlanteles::all();
        $tipoArchivos = DB::table('cat_tipo_archivos')->get()->pluck('nombre', 'id');

        $data = Alumnos::with('curso', 'grupo', 'genero', 'plantel')->find($id);

        $data2 = Multimedias::with('alumno', 'archivo')->where('id_alumno', '=', $id)->get();


        if (request()->ajax()) {
            return datatables()
                ->of($data2)
                ->addColumn('action', 'multimedia.actions')
                ->rawColumns(['action'])
                ->toJson();
        }

        return view('catalogos.alumnos.edit', compact('data', 'cursos', 'generos', 'grupos', 'planteles', 'tipoArchivos'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $input = $request->all();
        $alumno = Alumnos::find($input['alumno_id']);

        // Verifico si hay una fotografia
        if ($request->file('fotografia')) {
            $validator = Validator::make($request->all(), [
                'fotografia' => 'required|image|mimes:png,jpg,jpeg|max:3072',
            ]);

            if (!$validator->fails()) {
                $path = public_path('store/alumnos/fotografias');
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }

                if ($alumno->foto) {
                    if (!unlink($path . '/' . $alumno->foto)) {
                        return response()->json(['code' => 500, 'data' => [], 'message' => 'No se pudo almacenar la nueva imagen.']);
                    }
                }

                $input['fotografia'] = time() . '.' . $request->fotografia->extension();
                $request->fotografia->move(public_path('store/alumnos/fotografias'), $input['fotografia']);

                $alumno->plantel_id = $input['plantel_id'];
                $alumno->nivel_educativo_id = $input['curso_id'];
                $alumno->sexo_id = $input['sexo_id'];
                $alumno->grupo_id = $input['grupo_id'];
                $alumno->matricula = $input['matricula2'];
                $alumno->nombre = ucwords($input['nombre']);
                $alumno->apellido_paterno = ucwords($input['apellido_paterno']);
                $alumno->apellido_materno = ucwords($input['apellido_materno']);
                $alumno->nombre_completo = Str::lower($input['nombre'] . ' ' . $input['apellido_paterno'] . ' ' . $input['apellido_materno']);
                $alumno->fecha_nacimiento = Carbon::createFromFormat('d/m/Y', $input['fecha_nacimiento'])->format('Y-m-d');
                $alumno->curp = $input['curp'];
                $alumno->domicilio = $input["domicilio"];
                $alumno->nombre_completo_tutor = Str::lower($input['nombre_tutor']);
                $alumno->email_tutor = $input['correo_electronico_tutor'];
                $alumno->telefono_tutor = $input['telefono_celular_tutor'];
                $alumno->observaciones = $input['observaciones'];
                $alumno->password = Hash::make($input['curp']);
                $alumno->activado = 1;

                // Guardar nombre del archivo en la base de datos
                $alumno->foto = $input['fotografia'];
                $alumno->save();

                $data = [
                    'fotografia' => $alumno->fotografia_infantil,
                    'alumno' => $alumno
                ];

                return response()->json(['code' => 200, 'data' => $data, 'message' => 'Fotografía infantil y datos guardados con éxito.']);
            } else {
                return response()->json(['code' => 500, 'data' => $validator->errors()->all(), 'message' => 'No se pudo guardar la fotografía.']);
            }
        } else {

            $alumno->plantel_id = $input['plantel_id'];
            $alumno->nivel_educativo_id = $input['curso_id'];
            $alumno->sexo_id = $input['sexo_id'];
            $alumno->grupo_id = $input['grupo_id'];
            $alumno->matricula = $input['matricula2'];
            $alumno->nombre = ucwords($input['nombre']);
            $alumno->apellido_paterno = ucwords($input['apellido_paterno']);
            $alumno->apellido_materno = ucwords($input['apellido_materno']);
            $alumno->nombre_completo = Str::lower($input['nombre'] . ' ' . $input['apellido_paterno'] . ' ' . $input['apellido_materno']);
            $alumno->fecha_nacimiento = Carbon::createFromFormat('d/m/Y', $input['fecha_nacimiento'])->format('Y-m-d');
            $alumno->curp = $input['curp'];
            $alumno->domicilio = $input["domicilio"];
            $alumno->nombre_completo_tutor = Str::lower($input['nombre_tutor']);
            $alumno->email_tutor = $input['correo_electronico_tutor'];
            $alumno->telefono_tutor = $input['telefono_celular_tutor'];
            $alumno->observaciones = $input['observaciones'];
            $alumno->password = Hash::make($input['curp']);
            $alumno->activado = 1;

            $alumno->save();

            return response()->json(['code' => 200, 'data' => [], 'message' => 'Fotografía infantil y datos guardados con éxito.']);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $alumno = Alumnos::findOrFail($id);
        $alumno->activado = 0;
        $alumno->save();
        $alumno->delete();
    }


    /**
     * Search the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function find(Request $request)
    {
        $alumno = Alumnos::where('matricula', $request->matricula)->first();

        if ($alumno) {
            return response()->json(['status' => true, 'data' => $alumno]);
        } else {
            return response()->json(['status' => false, 'data' => null]);
        }
    }


    // Funcion para activar un alumno
    public function activar(Request $request)
    {
        $alumno = Alumnos::findOrFail($request->alumno_id);

        if ($alumno) {
            $alumno->monto = null;
            $alumno->activado = 1;
            $alumno->save();

            return response()->json(['status' => true, 'data' => $alumno]);
        } else {
            return response()->json(['status' => false, 'data' => null]);
        }
    }


    // Funcion para desactivar un alumno
    public function desactivar(Request $request)
    {
        $alumno = Alumnos::findOrFail($request->alumno_id);

        if ($alumno) {
            $alumno->monto = $request->monto;
            $alumno->activado = 0;
            $alumno->save();

            return response()->json(['status' => true, 'data' => $alumno]);
        } else {
            return response()->json(['status' => false, 'data' => null]);
        }
    }



    public function archivosIndex($id)
    {

        $tipoArchivos = DB::table('cat_tipo_archivos')->get()->pluck('nombre', 'id');

        $data = Alumnos::with('curso', 'grupo', 'genero', 'plantel')->find($id);

        $data2 = Multimedias::with('alumno', 'archivo')->where('id_alumno', '=', $id)->get();


        if (request()->ajax()) {
            return datatables()
                ->of($data2)
                ->addColumn('action', 'multimedia.actions')
                ->rawColumns(['action'])
                ->toJson();
        }

        return view('catalogos.alumnos.files', compact('data', 'tipoArchivos'));
    }
}
