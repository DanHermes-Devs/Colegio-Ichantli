<?php

namespace App\Http\Controllers;

use App\Usuarios;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $user = Auth::user();

        // Total de registros
        $count_usuarios = Usuarios::all()->count();

        if(request()->ajax()) {
            if($user->id == 1){
                $usuarios = Usuarios::with('roles')->get(); 
            }else{
                $usuarios = Usuarios::with('roles')->offset(1)->limit($count_usuarios)->get();
            }

            return datatables()
            ->of($usuarios)
            ->addColumn('action', 'gestion_usuarios.usuarios.actions')
            ->rawColumns(['action'])
            ->toJson();
        }

        return view('gestion_usuarios.usuarios.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usuario = Usuarios::with('roles')->findOrFail($id);
        return response()->json($usuario);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        // Si es usuario ya registrado
        if($request->id){
            $user = Usuarios::where([['usuario', $request->usuario]])->first();

            if($user->id == $request->id){
                if(empty($request->password)){
                    // Si el password esta vacio no lo inlcuimos 
                    $usuario = Usuarios::updateOrCreate(['id' => $request->id],[
                        'nombre'           => Str::title($request->nombre),
                        'apellido_paterno' => Str::title($request->apellido_paterno),
                        'apellido_materno' => Str::title($request->apellido_materno),
                        'full_name'        => Str::title($request->nombre.' '.$request->apellido_paterno.' '.$request->apellido_materno),
                        'sexo_id'          => $request->sexo_id,
                        'email'            => $request->email,
                        'usuario'          => $request->usuario,
                        'role_id'          => $request->role_id
                    ]);
                }else{
                    $usuario = Usuarios::updateOrCreate(['id' => $request->id],[
                        'nombre'           => Str::title($request->nombre),
                        'apellido_paterno' => Str::title($request->apellido_paterno),
                        'apellido_materno' => Str::title($request->apellido_materno),
                        'full_name'        => Str::title($request->nombre.' '.$request->apellido_paterno.' '.$request->apellido_materno),
                        'sexo_id'          => $request->sexo_id,
                        'email'            => $request->email,
                        'usuario'          => $request->usuario,
                        'password'         => Hash::make($request->password),
                        'role_id'          => $request->role_id
                    ]);
                }

                $user = Usuarios::find($usuario->id);

                // Asignamos el rol al usuario
                $user->syncRoles($request->role_id);
                
                return response()->json(['status'=> true]);
            }else{
                return response()->json(['status'=> 'user_exist']);
            }
        }else{
            $usuario = Usuarios::updateOrCreate(['id' => $request->id],[
                'nombre'           => Str::title($request->nombre),
                'apellido_paterno' => Str::title($request->apellido_paterno),
                'apellido_materno' => Str::title($request->apellido_materno),
                'full_name'        => Str::title($request->nombre.' '.$request->apellido_paterno.' '.$request->apellido_materno),
                'sexo_id'          => $request->sexo_id,
                'email'            => $request->email,
                'usuario'          => $request->usuario,
                'password'         => Hash::make($request->password),
                'role_id'          => $request->role_id
            ]);

            $user = Usuarios::find($usuario->id);

            // Asignamos el rol al usuario
            $user->syncRoles($usuario->role_id);
            
            return response()->json(['status'=> true]);
        }        
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = Usuarios::with('roles')->findOrFail($id);

        return response()->json($usuario);
    }

    public function updatePassword(Request $request)
    {
        $usuario = Usuarios::findOrFail($request->id);

        $usuario->password = Hash::make($request->password);
        $usuario->save();

        return response()->json(['status'=> true]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = Usuarios::findOrFail($id);
        // Remover roles del usuario
        $usuario->syncRoles([]);
        $usuario->delete();
    }


    // Validar si existe un usuario o no
    public function validateUsuario(Request $request) {
        $flag = false;
        $usuario = Usuarios::all()->where('usuario', '=', $request->nombre_de_usuario)->first();
        
        if ($usuario) {
            $flag = false;
        }else {
            $flag = true;
        }

        return response()->json($flag);
    }
}
