<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ConfigController extends Controller
{
    public function index(){
        $data = DB::table('config')->where('id', 1)->first();

        return view('configuracion.index', compact('data'));
    }

    public function store(Request $request){
        $data = DB::table('config')
            ->where('id', 1)
            ->update([
                'nombre_empresa' => $request->nombre_empresa,
                'descripcion' => $request->descripcion_empresa,
                'url_empresa' => $request->url_empresa,
                'url_activo' => $request->url_activo
            ]);

        if($data){
            return response()->json(['code' => 200, 'data' => $data, 'message' => 'Configuración guardada con éxito.']);  
        }else{
            return response()->json(['code' => 500, 'data' => [], 'message' => 'Error al guardar configuración.']);  

        } 
    }
}
