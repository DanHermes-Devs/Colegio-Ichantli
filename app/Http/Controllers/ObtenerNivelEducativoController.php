<?php

namespace App\Http\Controllers;

use App\CatNivelEducativo;
use Illuminate\Http\Request;

class ObtenerNivelEducativoController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {

        $results = CatNivelEducativo::all();

        return response()->json(['results' => $results]);
    }
}
