<?php

namespace App\Http\Controllers;

use App\Alumnos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $alumnos = DB::table('alumnos')->whereNull('deleted_at')->count();
        $circulares = DB::table('circulares')->count();
        $alumnos_adeudo = DB::table('alumnos')->where('activado', 0)->whereNull('deleted_at')->count();

        return view('home.inicio', compact(
            'alumnos',
            'alumnos_adeudo',
            'circulares',
        ));
    }
}
