<?php

namespace App\Http\Controllers;

use App\Alumnos;
use App\CatTipoArchivos;
use App\Multimedias;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class MultimediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nombre_de_archivo = CatTipoArchivos::with('multimedias')->where('id', '=', $request->input('id_tipoArchivo'))->first('nombre');
        $alumno = Alumnos::find($request->input('id_alumno'));
        $tipoArchivoId = $request->input('id_tipoArchivo');

        // Almacenar archivo
        if($request->file('nombreArchivo'))
        {

            $validator = Validator::make($request->all(), [
                'nombreArchivo' => 'required'
            ]);


            if($validator->fails())
            {
                return response()->json([
                    'status' => 400,
                    'errors' => $validator->messages()
                ]);

            } else {
                $path = public_path('store/alumnos/archivos');
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }



                $archivo = $request->file('nombreArchivo');
                $nombreArchivo = strtr(Str::lower($nombre_de_archivo->nombre), " " , "_") . "_" . $alumno->curp . "." . $request->file('nombreArchivo')->extension();
                $ubicacion = public_path('store/alumnos/archivos');
                $archivo->move($ubicacion, $nombreArchivo);


                $multimedia = new Multimedias;
                $multimedia->id_alumno = $request->input('id_alumno');
                $multimedia->id_tipoArchivo = $request->input('id_tipoArchivo');
                $multimedia->nombreArchivo = $nombreArchivo;
                $multimedia->save();

                return response()->json([
                    'status' => 200,
                    'message' => 'Añadido Correctamente'
                ]);
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Multimedias  $multimedia
     * @return \Illuminate\Http\Response
     */
    public function show(Multimedias $multimedia)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Multimedias  $multimedia
     * @return \Illuminate\Http\Response
     */
    public function edit(Multimedias $multimedia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Multimedias  $multimedia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Multimedias $multimedia)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Multimedias  $multimedia
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Multimedias::findOrFail($id);
        $path = public_path('store/alumnos/archivos');

        if($data->nombreArchivo){
            if (!unlink($path.'/'.$data->nombreArchivo)) {
                return response()->json(['code' => 500, 'data' => [], 'message' => 'No se pudo borrar el registro de la boleta.']);
            }else{
                $data->delete();
                return response()->json(['code' => 200, 'data' => [], 'message' => 'Éxito el registro de boleta se ha borrado.']);
            }
        }else{
            return response()->json(['code' => 500, 'data' => [], 'message' => 'No se pudo borrar el registro de la boleta.']);
        }
    }

    /**
     * Funcion para descargar un archivo
     */
    public function download($id){
        $data = Multimedias::findOrFail($id);

        $ruta = public_path('store/alumnos/archivos/').$data->nombreArchivo;
        $name = $data->nombreArchivo;
        $headers = ['Content-Type' => 'application/pdf'];

        return response()->download($ruta, $name, $headers);
    }
}
