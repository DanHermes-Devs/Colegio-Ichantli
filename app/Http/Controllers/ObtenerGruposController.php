<?php

namespace App\Http\Controllers;

use App\CatGrupos;
use Illuminate\Http\Request;

class ObtenerGruposController extends Controller
{
    public function __invoke(Request $request)
    {

        if($request->curso_id){
            $results = CatGrupos::all()->where('nivel_educativo_id', $request->curso_id);
        }

        return $results->toJson();
    }
}
