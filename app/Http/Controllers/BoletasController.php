<?php

namespace App\Http\Controllers;

use App\Alumnos;
use App\Boletas;
use App\Imports\BoletasImport;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;

class BoletasController extends Controller
{
    /**
    * Funcion para mostrar el listado de alumnos
    */
    public function index(){
    
        if(request()->ajax()) {
            // dd(request());
            $data = Alumnos::with('curso', 'grupo', 'plantel')
            ->where('nivel_educativo_id', request()->curso_id)
            ->orderBy('grupo_id', 'asc')
            ->get();

            return datatables()
            ->of($data)
            ->addColumn('action', 'boletas.actions')
            ->rawColumns(['action'])
            ->toJson();
        }

        return view('boletas.index');
    }


    /**
     * Funcion para guardar boletas a un alumno
    */
    public function store(Request $request){
        $input = $request->all();
        $alumno = Alumnos::with('curso', 'grupo')->findOrFail($request->alumno_id_2);

        // Verifico si hay un archivo
        if($request->file('archivo')){
            $validator = Validator::make($request->all(), [
                'archivo' => 'required|mimes:pdf|max:3072',
            ]);

            if(!$validator->fails()){
                $path = public_path('store/alumnos/boletas');
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }

                if($input['materia_id'] == 1){
                    $materia = 'ESPAÑOL';
                    $materia2 = 'ESP';
                }else if($input['materia_id'] == 2){
                    $materia = 'INGLÉS';
                    $materia2 = 'ING';
                }else if($input['materia_id'] == 3){
                    $materia = 'FRANCÉS';
                    $materia2 = 'FRA';
                }

                // Obtener fecha actual formato d-m-y
                $date = Carbon::now();
                $fecha_actual = $date->format('d-m-Y');

                $input['archivo'] = $alumno->matricula.'_BOLETA_DE_'.$materia2.'_'.time().'.'.$request->archivo->extension();
                $request->archivo->move(public_path('store/alumnos/boletas'), $input['archivo']);
    
                $boleta = new Boletas;
                
                $boleta->alumno_id = $alumno->id;
                $boleta->nivel_educativo_id = $alumno->nivel_educativo_id;
                $boleta->grupo_id = $alumno->grupo->id;
                $boleta->materia = $input['materia_id'];
                $boleta->nombre = 'BOLETA DE '.$materia;
                $boleta->descripcion = null;
                $boleta->archivo = $input['archivo'];
                $boleta->fecha_subida = $fecha_actual;

                $boleta->save();

                $data = [
                    'archivo' => $boleta->archivo,
                    'alumno' => $alumno
                ];
                
                return response()->json(['code' => 200, 'data' => $data, 'message' => 'Boleta del alumno guardado con éxito.']);
            }else{
                return response()->json(['code' => 500, 'data' => $validator->errors()->all(), 'message' => 'No se pudo guardar la boleta por que excede el limite de 3MB.']);
            }
        }else{
            return response()->json(['code' => 500, 'data' => [], 'message' => 'Error no se pudo guardar la boleta.']);
        } 
    }


    /**
    * Funcion para traer todas las boletas asociadas a un alumno.
    */
    public function show($id){
        $data = Boletas::with('alumno', 'curso', 'grupo')->where('alumno_id', $id)->get();

        if(request()->ajax()) {
            return datatables()
            ->of($data)
            ->toJson();
        }
    }


    /**
    * Funcion para descargar un archivo
    */
    public function download(Request $request){
        $data = Boletas::findOrFail($request->id);

        $ruta = public_path('store/alumnos/boletas/').$data->archivo;
        $name = $data->archivo;
        $headers = ['Content-Type' => 'application/pdf'];

        return response()->download($ruta, $name, $headers);
    }


    /**
    * Función para eliminar el registro de boleta de un alumno.
    */
    public function destroy($id){
        $data = Boletas::findOrFail($id);
        $path = public_path('store/alumnos/boletas');

        if($data->archivo){
            if (!unlink($path.'/'.$data->archivo)) {  
                return response()->json(['code' => 500, 'data' => [], 'message' => 'No se pudo borrar el registro de la boleta.']);
            }else{
                $data->delete();
                return response()->json(['code' => 200, 'data' => [], 'message' => 'Éxito el registro de boleta se ha borrado.']);
            }
        }else{
            return response()->json(['code' => 500, 'data' => [], 'message' => 'No se pudo borrar el registro de la boleta.']);
        }
    }


    /**
    * Función para cargar la vista de subir masivamente.
    */
    public function masivamente(){
        return view('boletas.masivamente');
    }

    /**
    * Función para importar datos de un excel.
    */
    public function import(Request $request){
        // Verifico si hay un archivo

        if($request->file('archivo')){
            try {
                // Este para cuando se tiene un archivo guardado
                // $excel = Excel::import(new BoletasImport, 'store/alumnos.xlsx', 'real_public');
                Excel::import(new BoletasImport, $request->file('archivo'));

                return response()->json(['code' => 200, 'data' => [], 'message' => 'Importación exitosa.']);

            }catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
                $failures = $e->failures();
                return response()->json(['code' => 500, 'data' => $failures, 'message' => 'Error no sa cargado el archivo de excel.']);
            }
        }else{
            return response()->json(['code' => 500, 'data' => [], 'message' => 'Error no sa cargado el archivo de excel.']);
        }   
    }

    
    /**
    * Función para guardar archivos masivamente.
    */
    public function storeFilesMasivamente(Request $request){
        $result = [];
        foreach ($request->file('file') as $file) {
            $filename = $file->getClientOriginalName();
            $file->move(public_path('store/alumnos/boletas/'), $filename);

            if($file) {
                $result[] = [
                    'success' => true,
                    'message' => '',
                ];
            }else {
                $result[] = [
                    'success' => false,
                    'message' => '',
                ];
            }
        }

        return response()->json(['data' => $result]);
    }


    /**
    * Función para eliminar archivos masivamente.
    */
    public function destroyFilesMasivamente(Request $request){
        $filename = $request->get('filename');
        $path = public_path('store/alumnos/boletas/').$filename;

        if(file_exists($path)) {
            unlink($path);
        }

        return $filename;  
    }
}
