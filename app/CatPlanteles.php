<?php

namespace App;

use App\Alumnos;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CatPlanteles extends Model
{
    use SoftDeletes;

    public $timestamps = true;
    protected $table = 'cat_planteles';
    protected $fillable = [
        'clave', 
        'nombre', 
        'descripcion', 
        'direccion', 
        'contacto', 
        'activo'
    ];

    public function alumno(){
        return $this->hasMany(Alumnos::class);
    }
    
}
