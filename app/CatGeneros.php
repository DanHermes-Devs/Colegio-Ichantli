<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CatGeneros extends Model
{
    use SoftDeletes;

    public $timestamps = true;
    protected $table = 'cat_generos';
    protected $hidden = ['deleted_at', 'created_at','updated_at'];
    protected $fillable = ['nombre'];


    // =============================================================================
    // RELACIONES
    // =============================================================================
    public function alumnos(){
        return $this->hasMany(Alumnos::class);
    }    
}
