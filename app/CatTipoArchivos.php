<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CatTipoArchivos extends Model
{
    use SoftDeletes;
    protected $table = 'cat_tipo_archivos';
    protected $fillable = [
        'nombre'
    ];

    public function multimedias()
    {
        return $this->hasMany(Multimedias::class, 'id_tipoArchivo', 'id');
    }
}
