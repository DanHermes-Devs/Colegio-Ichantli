<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Circulares extends Model
{

    use SoftDeletes;

    public $timestamps = true;
    protected $table = 'circulares';
    protected $fillable = [
        'nombre', 
        'descripcion', 
        'archivo', 
        'fecha_subida', 
        'activo'
    ];


    public function getFechaSubidaAttribute($value){
        $date = Carbon::parse($value);
        return $date->format('d-m-Y');
    }
}
