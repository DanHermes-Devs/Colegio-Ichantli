<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatGrupos extends Model
{
    public $timestamps = true;
    protected $table = 'cat_grupos';
    protected $hidden = ['deleted_at', 'created_at','updated_at'];
    protected $fillable = [
        'nivel_educativo_id',
        'grado',
        'letra'
    ];


    // =============================================================================
    // RELACIONES
    // =============================================================================
    public function alumnos(){
        return $this->hasMany(Alumnos::class);
    } 

    public function curso(){
        return $this->belongsTo(CatNivelEducativo::class);
    } 

    public function boletas(){
        return $this->belongsTo(Boletas::class);
    }
}
