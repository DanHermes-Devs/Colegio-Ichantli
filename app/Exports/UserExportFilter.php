<?php

namespace App\Exports;

use App\Alumnos;
use App\CatGrupos;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithTitle;

class UserExportFilter implements FromView
{
    use Exportable;

    protected $id1;
    protected $id2;

    public function __construct($id1 = null, $id2 = null)
    {
        $this->id1 = $id1;
        $this->id2 = $id2;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    // public function collection()
    // {
    //     // dd(id1);
    //     return $this->id1 ?: Alumnos::all();
    // }

    public function view(): View
    {
        return view('exports.alumnos', [
            'cursos' => $this->id1 ?: Alumnos::all(),
            'grupos' => $this->id2 ?: CatGrupos::all()
        ]);
    }

    public function title(): string
    {
        return 'Lista de Alumnos';
    }
}
