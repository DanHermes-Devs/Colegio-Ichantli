<?php

namespace App\Exports;

use App\Alumnos;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class UsersExport implements FromView, WithTitle
{
    public function view(): View
    {
        return view('exports.alumnosTotales', [
            'alumnos' => Alumnos::all()
        ]);
    }

    public function title(): string
    {
        return 'Lista de Alumnos';
    }
}

