<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatMunicipios extends Model
{
    public $timestamps = true;
    protected $table = 'cat_municipcios';
    protected $fillable = ['estado_id', 'clave', 'nombre', 'activo'];


    // Esta preinscripción pertenece a un estado
    public function estados(){
        return $this->belongsTo(CatEstados::class);
    }

    // Municipios tiene una o muchas preinscripciones
    public function preinscripcion(){
        return $this->hasMany(Preinscripcion::class);
    } 


    // Municipios tiene una o muchos alumnos
    public function Alumnos(){
        return $this->hasMany(Alumnos::class);
    }       
}
