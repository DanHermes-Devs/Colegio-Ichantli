<?php

namespace App\Imports;

use App\Alumnos;
use App\Boletas;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class BoletasImport implements ToCollection, WithHeadingRow
{
    public function collection(Collection $rows){
        foreach ($rows as $row) {

            $data = Alumnos::whereMatricula($row['matricula'])->first();

            if($row['materia'] == 'ESP'){
                $materia_id = 1;
                $materia_nombre = 'ESPAÑOL';
            }else if($row['materia'] == 'ING'){
                $materia_id = 2;
                $materia_nombre = 'INGLÉS';
            }else if($row['materia'] == 'FRA'){
                $materia_id = 3;
                $materia_nombre = 'FRANCÉS';
            }

            // Obtener fecha actual formato d-m-y
            $date = Carbon::now();
            $fecha_actual = $date->format('d-m-Y');

            Boletas::create([
                'alumno_id' => $data->id,
                'nivel_educativo_id' => $data->nivel_educativo_id,
                'grupo_id' => $data->grupo_id,
                'materia' => $materia_id,
                'nombre' => 'BOLETA DE '.$materia_nombre,
                'descripcion' => null,
                'archivo' => $row['archivo'],
                'fecha_subida' => $fecha_actual
            ]);
        }
    }
}