<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Roles extends Model
{
    use SoftDeletes;
    
    public $timestamps = true;
    protected $table = 'roles';
    protected $fillable = ['name'];

}
