<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class CatEstados extends Model
{
    use SearchableTrait;
    
    public $timestamps = true;
    protected $table = 'cat_estados';
    protected $fillable = ['clave','nombre','abrev', 'activo'];
    protected $hidden = ['deleted_at', 'created_at','updated_at'];

    protected $searchable = [
        'columns' => [
            'id' => 0,
            'nombre' => 10,
            'clave' => 10
        ]
    ];


    // Escuelas tiene una o muchos municipios
    public function municipios(){
        return $this->hasMany(CatMunicipios::class);
    }

    // Estados tiene una o muchas preinscripciones
    public function preinscripcion(){
        return $this->hasMany(Preinscripcion::class);
    } 
    
    // Estados tiene una o muchos alumnos
    public function alumnos(){
        return $this->hasMany(Alumnos::class);
    }     
}
