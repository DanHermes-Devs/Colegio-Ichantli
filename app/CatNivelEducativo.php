<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatNivelEducativo extends Model
{
    public $timestamps = true;
    protected $table = 'cat_nivel_educativo';
    protected $hidden = ['deleted_at', 'created_at','updated_at'];
    protected $fillable = [
        'nombre',
        'descripcion',
        'centro_trabajo',
        'years_per_curse'
    ];

    // =============================================================================
    // RELACIONES
    // =============================================================================
    public function alumnos(){
        return $this->hasMany(Alumnos::class);
    } 

    public function grupos(){
        return $this->hasMany(CatGrupos::class);
    } 

    public function boletas(){
        return $this->belongsTo(Boletas::class);
    } 
}
