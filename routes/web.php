<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;


/*
|--------------------------------------------------------------------------
| Rutas para el acceso al sistema
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes();
Route::get('/', 'Auth\LoginController@index')->name('/');
//Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Route::post('/login', 'Auth\LoginController@login')->name('login');

Route::middleware(['auth:web'])->group(function () {
    /*
    |---------------------------------------------------------------
    | Rutas para el home del dashboard
    |---------------------------------------------------------------
    */
    Route::get('/home', 'HomeController@index')->name('home')->middleware('permission:inicio');
    Route::get('/obtener/grupos', 'ObtenerGruposController')->name('obtener.grupos')->middleware('permission:inicio');

    /*
    |---------------------------------------------------------------
    | Rutas para las [boletas]
    |---------------------------------------------------------------
    */
    Route::get('/boleta', 'BoletasController@index')->name('boleta.index')->middleware('permission:boletas');
    Route::post('/boleta/store', 'BoletasController@store')->name('boleta.store')->middleware('permission:boletas');
    Route::get('/boleta/show/{id}', 'BoletasController@show')->name('boleta.show')->middleware('permission:boletas');
    Route::get('/boleta/download', 'BoletasController@download')->name('boleta.download')->middleware('permission:boletas');
    Route::delete('/boleta/destroy/{id}', 'BoletasController@destroy')->name('boleta.destroy')->middleware('permission:boletas');
    Route::get('/boleta/masivamente', 'BoletasController@masivamente')->name('boleta.index_masivamente')->middleware('permission:boletas');
    Route::post('/boleta/masivamente/store', 'BoletasController@storeFilesMasivamente')->name('boleta.store_masivamente')->middleware('permission:boletas');
    Route::post('/boleta/masivamente/destroy', 'BoletasController@destroyFilesMasivamente')->name('boleta.destroy_masivamente')->middleware('permission:boletas');
    Route::post('/boleta/import', 'BoletasController@import')->name('boleta.import')->middleware('permission:boletas');

    /*
    |---------------------------------------------------------------
    | Rutas para las [multimedias]
    |---------------------------------------------------------------
    */
    Route::get('/multimedia', 'MultimediaController@index')->name('multimedia.index')->middleware('permission:catalogos');
    Route::post('/multimedia/store', 'MultimediaController@store')->name('multimedia.store')->middleware('permission:catalogos');
    Route::delete('/multimedia/destroy/{id}', 'MultimediaController@destroy')->name('multimedia.destroy')->middleware('permission:catalogos');
    Route::get('/multimedia/download/{id}', 'MultimediaController@download')->name('multimedia.download')->middleware('permission:catalogos');

    /*
    |---------------------------------------------------------------
    | Rutas para las [Circulares]
    |---------------------------------------------------------------
    */
    Route::get('/circulares', 'CircularesController@index')->name('circulares.index')->middleware('permission:circulares');
    Route::post('/circulares/store', 'CircularesController@store')->name('circulares.store')->middleware('permission:circulares');
    Route::get('/circulares/download/{id}', 'CircularesController@download')->name('circulares.download')->middleware('permission:circulares');
    Route::delete('/circulares/destroy/{id}', 'CircularesController@destroy')->name('circulares.destroy')->middleware('permission:circulares');


    /*
    |---------------------------------------------------------------
    | Rutas para la gestion del catálogo de [alumnos]
    |---------------------------------------------------------------
    */
    Route::get('/catalogos/alumnos', 'AlumnosController@index')->name('alumno.index')->middleware('permission:catalogos');
    Route::get('/catalogos/alumno/create', 'AlumnosController@create')->name('alumno.create')->middleware('permission:catalogos');
    Route::post('/catalogos/alumno/store', 'AlumnosController@store')->name('alumno.store')->middleware('permission:catalogos');
    Route::get('/catalogos/alumno/show/{id}', 'AlumnosController@show')->name('alumno.show')->middleware('permission:catalogos');
    Route::get('/catalogos/alumno/edit/{id}', 'AlumnosController@edit')->name('alumno.edit')->middleware('permission:catalogos');
    Route::post('/catalogos/alumno/update', 'AlumnosController@update')->name('alumno.update')->middleware('permission:catalogos');
    Route::delete('/catalogos/alumno/destroy/{id}', 'AlumnosController@destroy')->name('alumno.destroy')->middleware('permission:catalogos');
    Route::get('/catalogos/alumno/find', 'AlumnosController@find')->name('alumno.find')->middleware('permission:catalogos');
    Route::post('/catalogos/alumno/activar', 'AlumnosController@activar')->name('alumno.activar')->middleware('permission:catalogos');
    Route::post('/catalogos/alumno/desactivar', 'AlumnosController@desactivar')->name('alumno.desactivar')->middleware('permission:catalogos');
    Route::get('/catalogos/alumnos/export', 'AlumnosController@export')->name('alumno.export')->middleware('permission:catalogos');
    Route::get('/filtro/{idn}/{id}', 'AlumnosController@exportfilter')->name('alumno.filter')->middleware('permission:catalogos');
    Route::get('/catalogos/alumno/archivos/{id}', 'AlumnosController@archivosIndex')->name('alumno.archivos')->middleware('permission:catalogos');


    /*
    |---------------------------------------------------------------
    | Rutas para la gestion de usuarios [usuarios]
    |---------------------------------------------------------------
    */
    Route::get('/usuario', 'UsuariosController@index')->name('usuario.index')->middleware('permission:gestion_de_usuarios');
    Route::post('/usuario/store', 'UsuariosController@store')->name('usuario.store')->middleware('permission:gestion_de_usuarios');
    Route::get('/usuario/show/{id}', 'UsuariosController@show')->name('usuario.show')->middleware('permission:gestion_de_usuarios');
    Route::get('/usuario/edit/{id}', 'UsuariosController@edit')->name('usuario.edit')->middleware('permission:gestion_de_usuarios');
    Route::post('/usuario/updatePassword', 'UsuariosController@updatePassword')->name('usuario.updatePassword')->middleware('permission:gestion_de_usuarios');
    Route::delete('/usuario/destroy/{id}', 'UsuariosController@destroy')->name('usuario.destroy')->middleware('permission:gestion_de_usuarios');
    Route::post('/usuario/validateUsuario', 'UsuariosController@validateUsuario')->name('usuario.validateUsuario')->middleware('permission:gestion_de_usuarios');
    Route::get('/configuracion', 'ConfigController@index')->name('config.index')->middleware('permission:gestion_de_usuarios');
    Route::post('/configuracion/store', 'ConfigController@store')->name('config.store')->middleware('permission:gestion_de_usuarios');


    /*
    |---------------------------------------------------------------
    | Rutas para la gestion de usuarios [roles]
    |---------------------------------------------------------------
    */
    Route::get('/rol', 'RolesController@index')->name('rol.index')->middleware('permission:gestion_de_usuarios');
    Route::post('/rol/store', 'RolesController@store')->name('rol.store')->middleware('permission:gestion_de_usuarios');
    Route::get('/rol/show/{id}', 'RolesController@show')->name('rol.show')->middleware('permission:gestion_de_usuarios');
    Route::get('/rol/edit/{id}', 'RolesController@edit')->name('rol.edit')->middleware('permission:gestion_de_usuarios');
    Route::delete('/rol/destroy/{id}', 'RolesController@destroy')->name('rol.destroy')->middleware('permission:gestion_de_usuarios');

});



/*
|---------------------------------------------------------------
| Rutas para el usuario alumno
|---------------------------------------------------------------
*/
Route::middleware(['auth:alumnos'])->group(function () {

    Route::get('/alumno/home', 'HomeAlumnoController@index')->name('alumno.home');
    Route::get('/alumno/circulares', 'HomeAlumnoController@circulares')->name('alumno_panel.circulares');
    Route::get('/alumno/download/boleta/{id}', 'HomeAlumnoController@downloadBoleta')->name('alumno_panel.download_boleta');
    Route::get('/alumno/download/circular/{id}', 'HomeAlumnoController@downloadCircular')->name('alumno_panel.download_circular');

});
