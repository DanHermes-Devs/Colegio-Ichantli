<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatEstadosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('cat_estados')->delete();
        
        DB::table('cat_estados')->insert(array (
            0 => 
            array (
                'clave' => '01',
                'nombre' => 'Aguascalientes',
                'abrev' => 'Ags.',
                'activo' => 1,
            ),
            1 => 
            array (
                'clave' => '02',
                'nombre' => 'Baja California',
                'abrev' => 'BC',
                'activo' => 1,
            ),
            2 => 
            array (
                'clave' => '03',
                'nombre' => 'Baja California Sur',
                'abrev' => 'BCS',
                'activo' => 1,
            ),
            3 => 
            array (
                'clave' => '04',
                'nombre' => 'Campeche',
                'abrev' => 'Camp.',
                'activo' => 1,
            ),
            4 => 
            array (
                'clave' => '05',
                'nombre' => 'Coahuila de Zaragoza',
                'abrev' => 'Coah.',
                'activo' => 1,
            ),
            5 => 
            array (
                'clave' => '06',
                'nombre' => 'Colima',
                'abrev' => 'Col.',
                'activo' => 1,
            ),
            6 => 
            array (
                'clave' => '07',
                'nombre' => 'Chiapas',
                'abrev' => 'Chis.',
                'activo' => 1,
            ),
            7 => 
            array (
                'clave' => '08',
                'nombre' => 'Chihuahua',
                'abrev' => 'Chih.',
                'activo' => 1,
            ),
            8 => 
            array (
                'clave' => '09',
                'nombre' => 'Ciudad de México',
                'abrev' => 'CDMX',
                'activo' => 1,
            ),
            9 => 
            array (
                'clave' => '10',
                'nombre' => 'Durango',
                'abrev' => 'Dgo.',
                'activo' => 1,
            ),
            10 => 
            array (
                'clave' => '11',
                'nombre' => 'Guanajuato',
                'abrev' => 'Gto.',
                'activo' => 1,
            ),
            11 => 
            array (
                'clave' => '12',
                'nombre' => 'Guerrero',
                'abrev' => 'Gro.',
                'activo' => 1,
            ),
            12 => 
            array (
                'clave' => '13',
                'nombre' => 'Hidalgo',
                'abrev' => 'Hgo.',
                'activo' => 1,
            ),
            13 => 
            array (
                'clave' => '14',
                'nombre' => 'Jalisco',
                'abrev' => 'Jal.',
                'activo' => 1,
            ),
            14 => 
            array (
                'clave' => '15',
                'nombre' => 'México',
                'abrev' => 'Mex.',
                'activo' => 1,
            ),
            15 => 
            array (
                'clave' => '16',
                'nombre' => 'Michoacán de Ocampo',
                'abrev' => 'Mich.',
                'activo' => 1,
            ),
            16 => 
            array (
                'clave' => '17',
                'nombre' => 'Morelos',
                'abrev' => 'Mor.',
                'activo' => 1,
            ),
            17 => 
            array (
                'clave' => '18',
                'nombre' => 'Nayarit',
                'abrev' => 'Nay.',
                'activo' => 1,
            ),
            18 => 
            array (
                'clave' => '19',
                'nombre' => 'Nuevo León',
                'abrev' => 'NL',
                'activo' => 1,
            ),
            19 => 
            array (
                'clave' => '20',
                'nombre' => 'Oaxaca',
                'abrev' => 'Oax.',
                'activo' => 1,
            ),
            20 => 
            array (
                'clave' => '21',
                'nombre' => 'Puebla',
                'abrev' => 'Pue.',
                'activo' => 1,
            ),
            21 => 
            array (
                'clave' => '22',
                'nombre' => 'Querétaro',
                'abrev' => 'Qro.',
                'activo' => 1,
            ),
            22 => 
            array (
                'clave' => '23',
                'nombre' => 'Quintana Roo',
                'abrev' => 'Q. Roo',
                'activo' => 1,
            ),
            23 => 
            array (
                'clave' => '24',
                'nombre' => 'San Luis Potosí',
                'abrev' => 'SLP',
                'activo' => 1,
            ),
            24 => 
            array (
                'clave' => '25',
                'nombre' => 'Sinaloa',
                'abrev' => 'Sin.',
                'activo' => 1,
            ),
            25 => 
            array (
                'clave' => '26',
                'nombre' => 'Sonora',
                'abrev' => 'Son.',
                'activo' => 1,
            ),
            26 => 
            array (
                'clave' => '27',
                'nombre' => 'Tabasco',
                'abrev' => 'Tab.',
                'activo' => 1,
            ),
            27 => 
            array (
                'clave' => '28',
                'nombre' => 'Tamaulipas',
                'abrev' => 'Tamps.',
                'activo' => 1,
            ),
            28 => 
            array (
                'clave' => '29',
                'nombre' => 'Tlaxcala',
                'abrev' => 'Tlax.',
                'activo' => 1,
            ),
            29 => 
            array (
                'clave' => '30',
                'nombre' => 'Veracruz de Ignacio de la Llave',
                'abrev' => 'Ver.',
                'activo' => 1,
            ),
            30 => 
            array (
                'clave' => '31',
                'nombre' => 'Yucatán',
                'abrev' => 'Yuc.',
                'activo' => 1,
            ),
            31 => 
            array (
                'clave' => '32',
                'nombre' => 'Zacatecas',
                'abrev' => 'Zac.',
                'activo' => 1,
            ),
        ));
        
        
    }
}