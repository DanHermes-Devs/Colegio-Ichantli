<?php

use App\CatCicloEscolar;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        // Catalogos
        $this->call(CatGenerosTableSeeder::class);
        $this->call(CatEstadosTableSeeder::class);
        $this->call(CatMunicipiosTableSeeder::class);

        // Usuarios
        $this->call(UsuariosTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);

        $this->call(CatNivelEducativoTableSeeder::class);
        $this->call(CatGruposTableSeeder::class);
        $this->call(CatPlantelesTableSeeder::class);
        $this->call(ConfigTableSeeder::class);
        $this->call(CatTipoArchivoSeeder::class);
    }
}
