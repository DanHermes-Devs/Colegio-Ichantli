<?php

use App\Usuarios;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Lista de roles
        $root = Role::create(['name' => 'Administrador_ghsoft']);
        $usuario = Role::create(['name' => 'Administrador']);

        // Lista de permisos
        Permission::create(['name' => 'inicio', 'long_name' => 'Inicio', 'users' => 1]);
        Permission::create(['name' => 'boletas', 'long_name' => 'Boletas', 'users' => 1]);
        Permission::create(['name' => 'circulares', 'long_name' => 'Circulares', 'users' => 1]);
        Permission::create(['name' => 'catalogos', 'long_name' => 'Catálogos', 'users' => 1]);
        Permission::create(['name' => 'gestion_de_usuarios', 'long_name' => 'Gestión de usuarios', 'users' => 1]);
        Permission::create(['name' => 'configuracion', 'long_name' => 'Configuración', 'users' => 0]);
        
        $root->givePermissionTo(Permission::All());
        $usuario->givePermissionTo(['inicio', 'boletas', 'circulares', 'catalogos', 'gestion_de_usuarios']);

        // Asignamos el rol adminsitrador_ghsoft (id=1) al usuario 1
        $user = Usuarios::find(1);
        $user->assignRole(1);

        // Asignamos el rol adminsitrador (id=2) al usuario 2
        $user2 = Usuarios::find(2);
        $user2->assignRole(2);

    }
}
