<?php

use Illuminate\Database\Seeder;

class CatGruposTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('cat_grupos')->delete();
        
        \DB::table('cat_grupos')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nivel_educativo_id' => 1,
                'grado' => 1,
                'letra' => 'A',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'nivel_educativo_id' => 1,
                'grado' => 1,
                'letra' => 'B',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'nivel_educativo_id' => 2,
                'grado' => 1,
                'letra' => 'A',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'nivel_educativo_id' => 2,
                'grado' => 1,
                'letra' => 'B',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'nivel_educativo_id' => 2,
                'grado' => 2,
                'letra' => 'A',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'nivel_educativo_id' => 2,
                'grado' => 2,
                'letra' => 'B',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'nivel_educativo_id' => 2,
                'grado' => 3,
                'letra' => 'A',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'nivel_educativo_id' => 2,
                'grado' => 3,
                'letra' => 'B',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'nivel_educativo_id' => 3,
                'grado' => 1,
                'letra' => 'A',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'nivel_educativo_id' => 3,
                'grado' => 1,
                'letra' => 'B',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'nivel_educativo_id' => 4,
                'grado' => 1,
                'letra' => 'A',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'nivel_educativo_id' => 4,
                'grado' => 1,
                'letra' => 'B',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'nivel_educativo_id' => 4,
                'grado' => 1,
                'letra' => 'C',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'nivel_educativo_id' => 4,
                'grado' => 2,
                'letra' => 'A',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'nivel_educativo_id' => 4,
                'grado' => 2,
                'letra' => 'B',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'nivel_educativo_id' => 4,
                'grado' => 2,
                'letra' => 'C',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'nivel_educativo_id' => 4,
                'grado' => 3,
                'letra' => 'A',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'nivel_educativo_id' => 4,
                'grado' => 3,
                'letra' => 'B',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'nivel_educativo_id' => 4,
                'grado' => 3,
                'letra' => 'C',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'nivel_educativo_id' => 4,
                'grado' => 4,
                'letra' => 'A',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'nivel_educativo_id' => 4,
                'grado' => 4,
                'letra' => 'B',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'nivel_educativo_id' => 4,
                'grado' => 4,
                'letra' => 'C',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'nivel_educativo_id' => 4,
                'grado' => 5,
                'letra' => 'A',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'nivel_educativo_id' => 4,
                'grado' => 5,
                'letra' => 'B',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'nivel_educativo_id' => 4,
                'grado' => 5,
                'letra' => 'C',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'nivel_educativo_id' => 4,
                'grado' => 6,
                'letra' => 'A',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'nivel_educativo_id' => 4,
                'grado' => 6,
                'letra' => 'B',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'nivel_educativo_id' => 4,
                'grado' => 6,
                'letra' => 'C',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'nivel_educativo_id' => 5,
                'grado' => 1,
                'letra' => 'A',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => 30,
                'nivel_educativo_id' => 5,
                'grado' => 1,
                'letra' => 'B',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'id' => 31,
                'nivel_educativo_id' => 5,
                'grado' => 1,
                'letra' => 'C',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => 32,
                'nivel_educativo_id' => 5,
                'grado' => 2,
                'letra' => 'A',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => 33,
                'nivel_educativo_id' => 5,
                'grado' => 2,
                'letra' => 'B',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => 34,
                'nivel_educativo_id' => 5,
                'grado' => 2,
                'letra' => 'C',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'id' => 35,
                'nivel_educativo_id' => 5,
                'grado' => 3,
                'letra' => 'A',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'id' => 36,
                'nivel_educativo_id' => 5,
                'grado' => 3,
                'letra' => 'B',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'id' => 37,
                'nivel_educativo_id' => 5,
                'grado' => 3,
                'letra' => 'C',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}