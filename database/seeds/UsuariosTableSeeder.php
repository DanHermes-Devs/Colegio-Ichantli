<?php

use Illuminate\Database\Seeder;

class UsuariosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('usuarios')->delete();

        \DB::table('usuarios')->insert(array (
            0 =>
            array (
                'id' => 1,
                'nombre' => 'G&H Software',
                'apellido_paterno' => '',
                'apellido_materno' => '',
                'full_name' => 'G&H Software',
                'sexo_id' => 1,
                'email' => '',
                'usuario' => 'gyhsoftware',
                'password' => '$2y$10$mPsSqYtV2PlSAixP5mXfHuV8FTocGmRg0FAUOS5GFeII.e13v.EGC', //95Rt@jQztfmT
                'role_id' => 1,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 =>
            array (
                'id' => 2,
                'nombre' => 'Admin',
                'apellido_paterno' => 'Admin',
                'apellido_materno' => 'Admin',
                'full_name' => 'Admin Admin Admin',
                'sexo_id' => 1,
                'email' => 'admin@admin.com',
                'usuario' => 'admin',
                'password' => '$2y$10$EpdPuF3ajIj2y7DLQA0rbeyoutTBL4XDWbQ.Vi/WHaeSr/ANNRJY.',
                'role_id' => 2,
                'deleted_at' => NULL,
                'created_at' => '2021-04-27 16:47:34',
                'updated_at' => '2021-04-27 16:47:34',
            ),
        ));


    }
}
