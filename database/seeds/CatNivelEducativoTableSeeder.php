<?php

use Illuminate\Database\Seeder;

class CatNivelEducativoTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('cat_nivel_educativo')->delete();
        
        \DB::table('cat_nivel_educativo')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Maternal',
                'descripcion' => 'Maternal del colegio ichantli',
                'centro_trabajo' => NULL,
                'years_per_curse' => 3,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Kinder',
                'descripcion' => 'Kinder del colegio ichantli',
                'centro_trabajo' => NULL,
                'years_per_curse' => 3,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'Preescolar',
                'descripcion' => 'Preescolar del colegio ichantli',
                'centro_trabajo' => NULL,
                'years_per_curse' => 1,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'Primaria',
                'descripcion' => 'Primaria del colegio ichantli',
                'centro_trabajo' => NULL,
                'years_per_curse' => 6,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'nombre' => 'Secundaria',
                'descripcion' => 'Secundaria del colegio ichantli',
                'centro_trabajo' => NULL,
                'years_per_curse' => 3,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}