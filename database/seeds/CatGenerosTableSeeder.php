<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatGenerosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cat_generos')->delete();
        
        DB::table('cat_generos')->insert(array (
            0 => 
            array (
                'nombre' => 'Hombre',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ),

            1 => 
            array (
                'nombre' => 'Mujer',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ),
        ));    
    }
}
