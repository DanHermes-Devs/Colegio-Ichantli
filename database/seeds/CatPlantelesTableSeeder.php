<?php

use Illuminate\Database\Seeder;

class CatPlantelesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('cat_planteles')->delete();
        
        \DB::table('cat_planteles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'clave' => NULL,
                'nombre' => 'CDMX',
                'descripcion' => 'Plantel CDMX',
                'direccion' => NULL,
                'contacto' => NULL,
                'activo' => 1,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'clave' => NULL,
                'nombre' => 'RM',
                'descripcion' => 'Plantel RM',
                'direccion' => NULL,
                'contacto' => NULL,
                'activo' => 1,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}