<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatGruposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_grupos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('nivel_educativo_id');
            $table->smallInteger('grado');
            $table->string('letra', 1);
            $table->softDeletes();
            $table->timestamps();   
            
            $table->foreign('nivel_educativo_id')->references('id')->on('cat_nivel_educativo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_grupos');
    }
}
