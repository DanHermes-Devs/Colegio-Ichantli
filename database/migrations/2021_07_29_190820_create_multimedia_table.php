<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMultimediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('multimedia', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_alumno')->unsigned();
            $table->bigInteger('id_tipoArchivo')->unsigned();
            $table->string('nombreArchivo');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('id_alumno')->references('id')->on('alumnos');
            $table->foreign('id_tipoArchivo')->references('id')->on('cat_tipo_archivos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('multimedia');
    }
}
