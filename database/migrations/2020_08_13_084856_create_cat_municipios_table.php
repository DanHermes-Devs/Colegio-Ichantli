<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatMunicipiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_municipios', function (Blueprint $table) {
            $table->id();
            $table->foreignId('estado_id')->references('id')->on('cat_estados');
            $table->string('clave');
            $table->string('nombre');
            $table->boolean('activo')->comment('Si es 1 quiere decir que el registro esta activo, y si es cero el registro esta desactivado');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_municipios');
    }
}
