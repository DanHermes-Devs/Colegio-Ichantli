<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoletasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boletas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('alumno_id')->nullable();
            $table->unsignedBigInteger('nivel_educativo_id')->nullable();
            $table->unsignedBigInteger('grupo_id')->nullable();
            $table->string('materia')->nullable();
            $table->string('nombre')->nullable();
            $table->text('descripcion')->nullable();
            $table->string('archivo')->nullable();
            $table->date('fecha_subida')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('alumno_id')->references('id')->on('alumnos');
            $table->foreign('nivel_educativo_id')->references('id')->on('cat_nivel_educativo');
            $table->foreign('grupo_id')->references('id')->on('cat_grupos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boletas');
    }
}
