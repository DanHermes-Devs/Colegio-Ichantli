<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlumnosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alumnos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('plantel_id')->nullable();
            $table->unsignedBigInteger('nivel_educativo_id')->nullable();
            $table->unsignedBigInteger('sexo_id')->nullable();
            $table->unsignedBigInteger('grupo_id')->nullable();
            $table->string('matricula')->nullable();
            $table->string('nombre')->nullable();
            $table->string('apellido_paterno')->nullable();
            $table->string('apellido_materno')->nullable();
            $table->string('nombre_completo')->nullable();
            $table->date('fecha_nacimiento')->nullable();
            $table->string('curp')->nullable();
            $table->string('email')->nullable();
            $table->string('telefono')->nullable();
            $table->text('domicilio')->nullable();
            $table->string('foto')->nullable()->comment('solo es el nombre del archivo');
            $table->text('nombre_completo_tutor')->nullable();
            $table->string('email_tutor')->nullable();
            $table->string('telefono_tutor')->nullable();
            $table->text('observaciones')->nullable();
            $table->string('password')->nullable();
            $table->boolean('activado')->nullable()->comment('0 es desactivado 1 activado lo cual puede ser dado de baja o activo');
            $table->float('monto')->nullable();            
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('plantel_id')->references('id')->on('cat_planteles');
            $table->foreign('nivel_educativo_id')->references('id')->on('cat_nivel_educativo');
            $table->foreign('sexo_id')->references('id')->on('cat_generos');
            $table->foreign('grupo_id')->references('id')->on('cat_grupos');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alumnos');
    }
}
