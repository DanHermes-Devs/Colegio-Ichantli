## Configuración del proyecto

Para configurar el proyecto es necesario hacer lo siguiente:

1. Base de datos
~~~
Crear la base de datos llamada db_colegio_ichantli en el equipo local.
~~~

2. Instalar las dependencias de composer
~~~
composer install
~~~

3. Instalar las dependencias de npm
~~~
npm install
~~~

4. OJO No es necesario generar la llave de cifrado ya que esta ya viene incluida en el archivo .env
pero en caso de ocuparlo ejecutar el siguiente comando.
~~~
php artisan key:generate
~~~

5. Ejecutar las migraciones y los seeds con los comandos siguientes:
~~~
php artisan migrate
php artisan db:seed
~~~

Para un trabajo colaborativo funcional debemos estar constantemente en comunicación sobre que estamos haciendo y a que parte le estamos moviendo esto con el fin de evitar conflictos.